#!/bin/bash

###########################################################################################
# This script is used to prepare input parameters for subsequent analysis.                #
# Please, notice that all your files' path names MUST NOT CONTAIN ANY BLANK SPACE.        #
#                                                                                         #
# USAGE:                                                                                  #
# ./parameters_setup.bash <n_rept> <k-list> <theta-list> <datasets_dir> <collection> \    #
#     <ram> <n_cpus>                                                                      #
#                                                                                         #
# Parameters:                                                                             #
#    <n_rept>:   Number of execution of any randomized algorithm.                         #
#    <k-list>:   List of k-mer length comma-separated. (eg: 21,26,31)                     #
#    <theta-list>:    Comma-separated list of frequent k-mers cut-off, see SPRISS' theta. #
#    <datasets_dir>:  Path/to/directory where the datasets of the collection are stored.  #
#    <collection>:    Path/to/file.txt where for each dataset a column headed "label"     #
#                     contains its label, and one named "ds_name" contains its filename   #
#                     without extension.                                                  #
#    <ram>:     Ammount of RAM memory available for execution, expressed in GigaBytes.    #
#    <n_cpus>:  Number of computing cores available for execution.                        #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ###  Author: Giorgio Gallina                                                       #### #
# ###  Update: February 2023                                                         #### #
###########################################################################################




# #########################   PARAMETERS   ######################### #

# --------------------  Get project's location  -------------------- #
file_link=$( readlink -f -- "$0"; )       # $PROJECT_DIR/BashScripts/parameter_preparation.bash
BASH_DIR=$( dirname -- "$file_link"; )    # $PROJECT_DIR/BashScripts
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )  # $PROJECT_DIR


# --------------------  Load useful functions  --------------------- #
source ${BASH_DIR}/utils.bash


# --------------------  Saving input arguments  -------------------- #
n_rept=$1
k_str=$2
th_str=$3
ds_dir=$4
collection_rawlist=$5  # datasets collection
ramg=$6
n_cpus=$7

k_list=($( split $k_str "," ))
th_list=($( split $th_str "," ))
ramm=$(($ramg * 900))	#RAM memory in MB - lower due to simka using more memory than allowed


# ---------------------  Constant parameters  ---------------------- #
# project folders
PARAM_DIR="$PROJECT_DIR/.parameters"
RES_DIR="$PROJECT_DIR/Results"
WORKDIR="$PROJECT_DIR/.workdir"
DB_DIR="$PROJECT_DIR/Databases"
RSCRIPTS_DIR="${PROJECT_DIR}/Rscripts"

# dataset folders
collection_fname=$( basename -- ${collection_rawlist} )  # name.txt
collection_name=${collection_fname%%.*}          # name
collection_dir="$ds_dir/$collection_name"        # $ds_dir/name
lists_dir="$collection_dir/lists"
ds_gStats="$collection_dir/statistics_reads.txt"
ds_kStats_dir="$collection_dir"
ds_kStats_fname='statistics_${k}mers.txt'
ds_kStats="${ds_kStats_dir}/${ds_kStats_fname}"

# update $RES_DIR
RES_DIR="$RES_DIR/$collection_name"

# parameters filenames
bracken="${PARAM_DIR}/param_bracken"
sprissimka="${PARAM_DIR}/param_sprissimka"
simkamin="${PARAM_DIR}/param_simkamin"

# datasets file extension
ds_list=($( ls $ds_dir/*.f*))  # get list of all files with an extension starting with ".f"
ds1=${ds_list[0]}              # get the first such file-name
extens=".${ds1##*.}"           # get file-extension
extens=$( basename -- "$extens" ; )  # delete eventual "second extension" (e.g. ".fa.bracken"). THIS SHOULD NEVER HAPPEN!

# get timestamp
time=$(date +%y%m%d_%H%M%S)

# bracken reference database and indices
dbref="$DB_DIR/std"
# bracken pre-compiled reference database and indices
dbfixed="$DB_DIR/fixed"
DB_FIX_ONLINE="k2_standard_20221209.tar.gz"






# ######################  Saving parameters  ####################### #

msg
msg "Writing parameters for analysis scripts (BashScripts/run_*.bash)."

# make directories
mkdir -p $PARAM_DIR $RES_DIR $WORKDIR $DB_DIR $lists_dir

# ----------------------  Bracken parameters  ---------------------- #
if [ -e $bracken ] ; then
	msg "WARNING: Overwriting file $bracken"
fi
bracken_workdir="${WORKDIR}/Bracken"
#echo -n "-n $n_cpus " > $bracken
#echo -n "-c $collection_rawlist " >> $bracken
#echo -n "-p $lists_dir/label_path.txt " >> $bracken
#echo -n "-s $ds_gStats " >> $bracken
#echo -n "-o $RES_DIR/Bracken " >> $bracken
#echo -n "-w ${bracken_workdir} " >> $bracken
#echo -n "-r $dbref " >> $bracken
##echo -n "-t S " >> $bracken #TODO: manage taxonomy level
echo '#!/bin/bash' > ${bracken}
echo NCPU=${n_cpus} >> ${bracken}
echo collection_fname='"'${collection_rawlist}'"' >> ${bracken}
echo DS_PATH_LIST=${lists_dir}/label_path.txt >> ${bracken}
echo DS_STATISTICS=${ds_gStats} >> ${bracken}
echo RESDIR=${RES_DIR}/Bracken >> ${bracken}
echo WORKDIR=${bracken_workdir} >> ${bracken}
echo DATABASE=${dbref} >> ${bracken}

# -------------------  Spriss + Simka parameters  ------------------ #
if [ -e $sprissimka ] ; then
	msg "WARNING: Overwriting file ${sprissimka}" 
fi
#echo $n_rept > ${sprissimka}_in
#echo $k_str >> ${sprissimka}_in
#echo $th_str >> ${sprissimka}_in #TODO!
#echo $ds_dir >> ${sprissimka}_in
#echo ${collection_rawlist} >> ${sprissimka}_in #TODO!
#echo $extens >> ${sprissimka}_in
#echo $ramg >> ${sprissimka}_in
#echo $n_cpus >> ${sprissimka}_in

echo '#!/bin/bash' > ${sprissimka}
echo RESDIR='"'${RES_DIR}/Spriss'"' >> ${sprissimka}
echo WORKDIR='"'${workir}'"' >> ${sprissimka}
echo STATS_DIR='"'${ds_kStats_dir}'"' >> ${sprissimka}
echo STATS_BASENAME='"'statistics_'"' >> ${sprissimka}
echo STATS_SUFFIX='"'"mers.txt"'"' >> ${sprissimka}
echo COLLECTION_NAME='"'"${collection_name}"'"' >> ${sprissimka}
echo collection_fname='"'${collection_rawlist}'"' >> ${sprissimka}
echo DS_DIR='"'${ds_dir}'"' >> ${sprissimka}
echo EXTENSION='"'${extens}'"' >> ${sprissimka}
echo NCPU=${n_cpus} >> ${sprissimka}
echo MEMG=${ramg} >> ${sprissimka}
echo NREPT=${n_rept} >> ${sprissimka}
echo K_LIST='('${k_list[@]}')' >> ${sprissimka}
echo THETA_LIST='('${th_list[@]}')' >> ${sprissimka}
#echo THETA_LIST='('0.00000001 0.00000002 0.00000005 0.0000001 0.00000015 0.0000002')' >> ${sprissimka}
echo THETA_METHOD='"'abs'"' >> ${sprissimka}
echo THETA_NORM_DEF=1 >> ${sprissimka}



# ---------------------  SimkaMin parameters  ---------------------- #
if [ -e $simkamin ] ; then
	msg "WARNING: Overwriting file $simkamin"
fi
#echo -n "-i ${lists_dir}/simka.txt " > ${simkamin}_in
#echo -n "-k ${k_str} " >> ${simkamin}_in
#echo -n "-s ${ds_gStats} " >> ${simkamin}_in
#echo -n "-r ${n_rept} " >> ${simkamin}_in
#echo -n "-m ${ramg} " >> ${simkamin}_in
#echo -n "-c ${n_cpus} " >> ${simkamin}_in
#echo -n "-o ${RES_DIR}/Simkamin " >> ${simkamin}_in

echo '#!/bin/bash' > ${simkamin}
# parameters the user can set
echo in_list="${lists_dir}/simka.txt" >> ${simkamin}
echo RESDIR="${RES_DIR}/Simkamin" >> ${simkamin}
echo COLLECTION_NAME="${collection_name}" >> ${simkamin}
echo collection_fname="${collection_rawlist}" >> ${simkamin}
echo NCPU=${n_cpus} >> ${simkamin}
echo MEMG=${ramg} >> ${simkamin}
echo NREPT=${n_rept} >> ${simkamin}
#echo sampling_rates=(1 5 10 20 30 40 50 60 70 80 90) >> ${simkamin}
#echo sampling_rates_scale=100 >> ${simkamin}
echo smp_ref="min" >> ${simkamin}
echo rand_seed=1 >> ${simkamin} 
echo read_rarefy=1 >> ${simkamin}
echo k_str=${k_str} >> ${simkamin}
echo k_list='('${k_list[@]}')' >> ${simkamin}
echo ds_statistics=${ds_gStats} >> ${simkamin}


# ----------------  Programs path for the protocol  ---------------- #
# Parameters already set by ../setup.sh if previously executed

msg
msg "Writing software default paths if not written yet."

PARAM_DIR="${PROJECT_DIR}/.parameters"
mkdir -p $PARAM_DIR


if ! [ -e ${PARAM_DIR}/prg_spriss ]; then
	echo "${spriss_dir}/scripts/spriss_uf.py" > "${PARAM_DIR}/prg_spriss"
fi

if ! [ -e ${PARAM_DIR}/prg_krakenTools ]; then
	echo "kraken2" > "${PARAM_DIR}/prg_krakenTools"
	echo "kraken2-build" >> "${PARAM_DIR}/prg_krakenTools"
	echo "bracken" >> "${PARAM_DIR}/prg_krakenTools"
	echo "bracken-build" >> "${PARAM_DIR}/prg_krakenTools"
	echo "beta_diversity.py" >> "${PARAM_DIR}/prg_krakenTools"
fi

if ! [ -e ${PARAM_DIR}/prg_simka ]; then
	echo "simka" > "${PARAM_DIR}/prg_simka"
	echo "simkaMin" >> "${PARAM_DIR}/prg_simka"
fi

## avoid problems with "which" command
#which which &> /dev/null
#if [ $? -ne 0 ]; then
#	alias which="/usr/bin/which"
#	msg "WARNING: which program not found. It is now being aliased to '/usr/bin/which', but issues are likely to rise."
#fi
#	
## Find dustmasker
#MASKER="dustmasker"
#if ! [ -e "${PARAM_DIR}/prg_dustmasker" ]; then
#	command -v ${MASKER} &> /dev/null
#	status=$?
#	if [ $status -ne 0 ]; then
#		# try reloading environment
#		source ~/.bashrc
#		command -v ${MASKER} &> /dev/null
#		status=$?
#		if [ $status -ne 0 ]; then
#		# try through conda
#			conda activate bracken
#			command -v ${MASKER} &> /dev/null
#			status=$?
#			if [ $status -ne 0 ]; then
#				msg "WARNING: dustmasker installation failed."
#			else
#				echo $(command -v "dustmasker") > "${PARAM_DIR}/prg_dustmasker"
#			fi
#			conda deactivate
#		else
#			echo $(command -v "dustmasker") > "${PARAM_DIR}/prg_dustmasker"
#		fi
#	else
#		echo $(command -v "dustmasker") > "${PARAM_DIR}/prg_dustmasker"
#	fi
#fi


# #####################  COMPILE CPP PROGRAMS  ##################### #


# Compile Dissimilarity Calculator
mkdir -p ${PROJECT_DIR}/bin
g++ -fopenmp -o ${PROJECT_DIR}/bin/get_dissimilarity_matrix ${PROJECT_DIR}/src/Cdissimilarities.cpp
g++ -fopenmp -o ${PROJECT_DIR}/bin/betaDiversity_spriss ${PROJECT_DIR}/src/betaDiversity_spriss.cpp
g++ -fopenmp -o ${PROJECT_DIR}/bin/betaDiversity_cami ${PROJECT_DIR}/src/betaDiversity_camitrue.cpp




# #################  Download fixed ref. database  ################# #
fixed_ctrl="${dbfixed}/.controls/.ready"
mkdir -p "${dbfixed}/.controls"
if ! [ -e $fixed_ctrl ] || [ $(cat $fixed_ctrl) -ne 0 ]; then
	msg
	msg "Downloading pre-compiled indices (standard, 2022-dec-09, ~100GB)"
	# wget -P $dbfixed https://genome-idx.s3.amazonaws.com/kraken/${DB_FIX_ONLINE}
	#TODO: decomment above line
	status=$?
	tar -xvzf ${DB_FIX_ONLINE}
	let status+=$?
	if [ $status -ne 0 ]; then
		msg "Download failed."
		echo $status > $fixed_ctrl
	else
		msg "Done."
		rm -rf ${DB_FIX_ONLINE}
		echo 0 > $fixed_ctrl
	fi
fi


# #################  Collection lists generation  ################## #
mkdir -p $lists_dir

msg
msg "Generating datasets collection lists."
msg

# Write the lists of datasets subsequently needed
# <raw_list_fname> <ds_dir> <lists_dir> <extension>
exec_cmd "Rscript $RSCRIPTS_DIR/input_lists_generator.R ${collection_rawlist} $ds_dir/ $lists_dir $extens"




# ################  Datasets statistics generation  ################ #
# -----  global (read) statistics  ----- #
msg
msg "Computing global statistics of the datasets (n. reads, read length...)"
# do not overwrite statistics
if [ -f $ds_gStats ] ; then
	msg "WARNING: Datasets global-statistics already stored for the collection."
	msg "If you want them to be computed from scratch, please remove the following file:"
	msg "$ds_gStats"
else
	# <input_file> <output_file>
	exec_cmd "Rscript ${RSCRIPTS_DIR}/datasets_global_statistics.R $lists_dir/label_path.txt $ds_gStats"
fi

# ------ local (kmer) statistics  ------ #
msg ""
msg "Computing kmer statistics of the datasets (n. kmers, kmers per read...)"

# <global_statistics_filename> <kmer_statistics_dir> <list of k values>
exec_cmd "Rscript ${RSCRIPTS_DIR}/datasets_kmer_statistics.R $ds_gStats $ds_kStats_dir ${k_list[*]}"

msg
msg "Parameter setup completed."


