#!/bin/bash

###############################################################################
# Use (properly) this script for running SimkaMin Analysis: check its         #
# behavior as sub-sampling rates vary.                                        #
#                                                                             #
# This script will try to load parameters stored by former execution of       #
# 'parameters_set.bash'                                                       #
#                                                                             #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                     #
# Update: February 2023                                                       #
###############################################################################




# ***************************  LOAD ENVIRONMENT  **************************** #

# command line parameters
INPUT_ARGS=($@)

# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $PROJECT_DIR/BashScripts/run_simkamin.bash
BASH_DIR=$( dirname -- "$file_link"; )		# $PROJECT_DIR/BashScripts
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )	# $PROJECT_DIR

# load general functions
source ${BASH_DIR}/utils.bash

TIME=$( date +%y%m%d_%H%M%S )




# ****************************  LOCAL FUNCTIONS  **************************** #


function display_usage {
	cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [OPTIONS]...

This script runs Simka for the desired k-mer lengths.

Options:
-h, --help         Print this message and exit.
-V, --version      Print version of this script and exit.
-c <fname>, --ds-collection=<fname>
                   Path to file where the collection's table is stored with at
                   least two colum named 'label', 'ds_name'.
-i <fname>, --input-simka=<fname>
                   Input list of datatsets for Simka.
-s <fname>, --ds-statistics=<fname>
                   Path to file to the table of global (reads) statistics of
                   the datasets.
-o <outdir>, --out-dir=<outdir>
                   Path to the directory where to store output results.
-w <WORKDIR>, --work-dir=<WORKDIR>
                   Path to the directory where to store temporary results.
-n <NCPUs>, --ncpus=<NCPUs>
                   Number of CPUs available for computation. [Default: 8]
-m <ram>, --memory=<ram>
                   Ammount of available RAM in Gigabytes. [Default: 8]
-k <k-list>, --kmer-list=<k-list>
                   Comma-separated list of k-mer lengths k. [Default: 21,31]
--read-raref       Use this option to threshold the number of reads to that of
                   the smallest dataset. [TODO]
--random-seed      Run SimkaMin with a random seed rather than a fixed one,
                   which is the default.
--heatmap-rows=<list>
                   A comma-separated list of column names of those columns to
                   to be used as row annotations for heatmaps. It is possible
                   to provide column index numbers in place of column names by
                   starting the list with a comma. E.g.:
                    --heatmap-rows=clustering1,clustering2
                    --heatmap-rows=,4,3
                   [By default no row annotation are applied.]
--heatmap-columns=<list>
                   Same as --heatmap-rows but for heatmap column annotations.
                   
EOF
	exit
}



function arg_parser {
	ARGS=$(getopt -o hVvd:i:s:o:w:c:m:n:r:k:g: --long help,version,verbose,debug,ds-collection:,ds-statistics:,out-dir:,work-dir:,ncpus:,memory:,repetitions:,kmer-list:,sampling-grid:,avg-smp-size,absolute,kmer-raref,read-raref,smp-norm:,random-seed,heatmap-rows:,heatmap-columns: -- "$@")
	if [[ $? -ne 0 ]]; then
		msg "Error in parsing input arguments ( $* )"
		exit 1;
	fi
	
	heatmap_rows_cmd=""
	heatmap_cols_cmd=""

	#TODO: check file existence
	
	eval set -- "$ARGS"
	while [ : ]; do
	  case "$1" in
		-h | --help)
		    display_usage;
		    exit 0;
		    ;;
		-V | -v | --version)
			vstr="$(basename ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
			msg "$vstr"
			exit 0;
			;;
		--verbose)
			VERBOSE=0
			;;
		--debug)
			set -x
			;;
		-c | --ds-collection)
			collection_fname=$2
			if ! [ -f ${collection_fname} ]; then
				msg "ERROR: ${collection_fname} is not a file."
				exit 1;
			fi
		    shift
		    ;;
		-g | --sampling-grid)
			sampling_rates=($( split "${2}" "," ))
			shift
			;;
		-i | --input-simka)
			in_list=$2
			if ! [ -f ${in_list} ]; then
				msg "ERROR: ${in_list} is not a file."
				exit 1;
			fi
			shift
			;;
		-k | --kmer-list)
			k_str=$2
			k_list=($( split $k_str "," ))
			shift
			;;
		-m | --memory)
			MEMG=$2
			shift
			;;
		-n | --ncpus)
			NCPU=$2
			shift
			;;
		-o | --out-dir)
			RESDIR=$2
			shift
			;;
		-r | --repetitions)
			NREPT=$2
			msg "WARNING: unused option $1"
			shift
			;;
		-s | --ds-statistics)
		    ds_statistics=$2
		    if ! [ -f ${ds_statistics} ]; then
				msg "ERROR: ${ds_statistics} is not a file."
				exit 1;
			fi
		    shift
		    ;;
		-w | --work-dir)
			WORKDIR=$2
			shift
			;;
		--smp-norm)
			sampling_rates_scale=$2
			shift
			;;
		--absolute)
			smp_ref="abs"
			;;
		--avg-smp-size)
			smp_ref="avg"
			;;
		--kmer-raref)
			smp_ref="min"
			;;
		--read-raref)
			read_rarefy=0
			;;
		--random-seed)
			rand_seed=0
			;;
		--heatmap-rows)
			heatmap_rows_cmd="-r $2"
			shift
			;;
		--heatmap-columns)
			heatmap_cols_cmd="-c $2"
			shift
			;;
		--) shift; 
		    break 
		    ;;
		*)
			msg "Error: unknown option $1"
			display_usage;
			exit 1;
			;;
	  esac
	  shift
	done
	
	# exit on mandatory parameters lacking
	if [[ "${in_list}" == "" ]]; then
		msg "ERROR: Please use option -i to select a Simka input list."
		exit 1
	fi
	if [[ "${ds_statistics}" == "" ]] && [[ "${smp_ref}" != "abs" ]] && [ ${read_rarefy} -ne 0 ]; then
		msg "ERROR: Please use option -s to select a the datasets statistics file."
		exit 1
	fi
	
	
	# collection details for heatmap and directories
	if [ -f ${collection_fname} ]; then
		COLLECTION_NAME=$( get_filename "${collection_fname}")
		heatmap_opts="-d ${collection_fname} ${heatmap_rows_cmd} ${heatmap_cols_cmd}"
	fi
	
	# try to select a default output directory if not provided
	if [[ "${RESDIR}" == "" ]]; then
		RESDIR="${PROJECT_DIR}/Results/${COLLECTION_NAME}/Simkamin"
	fi
	RESDRIR="${RESDIR}_REF${smp_ref}"
	mkdir -p ${RESDIR}
	
	INFO_PATH="${RESDIR}/${INFO_FNAME}"
	
	# string for printing sampling rates
	if [ $sampling_rates_scale -eq 100 ]; then
		smp_string="%"
	else
		smp_string="/${sampling_rates_scale}"
	fi
	
	
	# RAM memory in Megabytes, lower than available for safety
	MEMM=$( simka_giga2mega ${MEMG} )
	
	# if --read-raref is set, limit the maximum number of reads to that of the smallest dataset
	if [ ${read_rarefy} -eq 0 ]; then
		minreads=$( min $( ${RSCRIPTS}/get_table_values.R -s ";" -c nreads ${ds_statistics} ) )
		max_reads_str=" -max-reads ${minreads} "
	fi
	
}




# print output and exit if help or version requested.
help_args=($(is_help_version ${@}))
if [ $? -eq 0 ]; then
	arg_parser ${help_args[@]}
fi

# ****************************  VARIABLES SETUP  **************************** #

# DEFAULTS:
VERSION="1.0"
VERBOSE=1 #non verbose by default (TODO)

BRAYNAME="mat_abundance_braycurtis.csv"
JACCNAME="mat_presenceAbsence_jaccard.csv"
INFO_FNAME="info.txt"

PARAM_DIR="${PROJECT_DIR}/.parameters"
RSCRIPTS="${PROJECT_DIR}/Rscripts"
WORKDIR="${PROJECT_DIR}/.workdir/simka_${TIME}"
RESDIR=""
INFO_PATH=""

# get programs to be executed
program_file="${PARAM_DIR}/prg_simka"
progs=($( cat "${program_file}" ; ))
simka=${progs[0]}
simkamin=${progs[1]}

# parameters the user can set
in_list=""
COLLECTION_NAME=""
collection_fname=""
ds_statistics=""
NCPU=8
MEMG=8
NREPT=1
#sampling_rates=(1 5 10 20 30 40 50 60 70 80 90)  # percentage (/100)
sampling_rates=(1 5 10 50 100 500 1000 2500 5000 7500)  # percentage (/100)
sampling_rates_scale=10000
smp_ref="min"	 # sampling rates defined as fraction of smallest dataset
rand_seed=1		 # FALSE: fixed simka seed=100
read_rarefy=1	 # FALSE: use all reads from all datasets
max_reads_str="" # use all available reads for each dataset
heatmap_opts=""  # heatmap without legend
declare -a k_list

# load parameters previously prepared through parameters_setup.bash
DEFAULT_PARAM_FILE="${PARAM_DIR}/param_simkamin"
#if [[ " ${INPUT_ARGS[*]}" != *" -h"* ]] && [[ " ${INPUT_ARGS[*]}" != *" -v"* ]] && [[ " ${INPUT_ARGS[*]}" != *" --help"* ]] && [[ " ${INPUT_ARGS[*]}" != *" --version"* ]] ; then
	if [ -e ${DEFAULT_PARAM_FILE} ]; then
		source ${DEFAULT_PARAM_FILE}
	else
		msg "WARNING: No pre-compiled parameters found: you may want to run 'parameters_setup.bash' before."
	fi
#fi

# INPUT parameters
arg_parser ${INPUT_ARGS[@]}

mkdir -p ${WORKDIR}



# ****************************  SIMKA EXECUTION  **************************** #

# iterate on k-mer lengths
for k in ${k_list[@]} ;
do
	
	# current results folder
	curres="${RESDIR}/skmin_k${k}_smp${sampling_rates_scale}"
	cWORKDIR="${WORKDIR}_k${k}"
	
	msg ""
	msg "   Running Simka without sub-sampling..."
	cmd="${simka} -in ${in_list} -out ${curres} -out-tmp ${cWORKDIR} -kmer-size ${k} -abundance-min 2 -max-memory ${MEMM} -nb-cores ${NCPU} ${max_reads_str}"
	msg "${cmd}"
	eval $cmd
	msg "   ...Simka execution ended."
	
	# clean-up and decompression
	rm -rf ${cWORKDIR}
	gunzip ${curres}/*.gz
	
	
	# -------------------  HEATMAPS  ------------------- #
	msg
	msg "Plotting Heatmaps of dissimilarity matrices."

	bray_matrix="${curres}/${BRAYNAME}"
	jacc_matrix="${curres}/${JACCNAME}"

	bray_title="BC dissimilarity by Simka - k=${k}"
	jacc_title="Jaccard distance by Simka - k=${k}"

	bray_out="${curres}/heatmap_simkamin_BC.pdf"
	jacc_out="${curres}/heatmap_simkamin_JAC.pdf"

	cmd="Rscript ${RSCRIPTS}/plot_heatmapCluster.R ${heatmap_opts} ${bray_matrix} \"${bray_title}\" ${bray_out}"
	msg "${cmd}"
	eval $cmd

	cmd="Rscript ${RSCRIPTS}/plot_heatmapCluster.R ${heatmap_opts} ${jacc_matrix} \"${jacc_title}\" ${jacc_out}"
	msg "${cmd}"
	eval $cmd
	
	
done

# ****************************  VARIABLES SETUP  **************************** #

