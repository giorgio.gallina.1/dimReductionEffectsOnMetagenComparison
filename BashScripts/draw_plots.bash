#!/bin/bash

###############################################################################
# This script is used to obtain plots of the results. Some tuning of graphic  #
# parameters is likely to be necessary, though.                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Gallina Giorgio                                                     #
# Update: March 2023                                                          #
###############################################################################


source ~/.bashrc


# absolute path to project directory
script_link=$( readlink -f -- "$0"; )		# $PROJECT_DIR/BashScripts/run_bracken.bash
BASH_DIR=$( dirname -- "$script_link"; )	# $PROJECT_DIR/BashScripts
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )	# $PROJECT_DIR
RSCRIPTS="${PROJECT_DIR}/Rscripts"

# load general functions
source ${BASH_DIR}/utils.bash

RES_DIR_=$1
if [[ $RES_DIR_ == "" ]]; then
	RES_DIR_="${PROJECT_DIR}/Results"
fi

# RES_DIR="${RES_DIR_}/GOS37"

for ds in "GOS37" "GOS26";
do
	RES_DIR="${RES_DIR_}/${ds}"
	for lvl in "S" "G";
	do
		args=("${RES_DIR}/Spriss_MTDabs" "${RES_DIR}/Simkamin" "${RES_DIR}/Bracken_std_lvl${lvl}" "${PROJECT_DIR}/Datasets/GOS/${ds}/statistics_reads.txt")

		exec_cmd "${RSCRIPTS}/plot_cor_simkamin_bracken.R ${args[@]:1}"
		exec_cmd "${RSCRIPTS}/plot_bivar_simka_bracken.R ${args[@]:1}"
		exec_cmd "${RSCRIPTS}/plot_dist_simka_bracken.R ${args[@]:1}"
		exec_cmd "${RSCRIPTS}/plot_cor_spriss_bracken.R ${args[@]}"
		exec_cmd "${RSCRIPTS}/plot_bivar_spriss_bracken.R ${args[@]}"
		exec_cmd "${RSCRIPTS}/plot_dist_spriss_bracken.R ${args[@]}"
	done
done



RES_DIR="${RES_DIR_}/HMP12"

for lvl in "S" "G";
do
	args=("${RES_DIR}/Spriss_MTDabs" "${RES_DIR}/Simkamin" "${RES_DIR}/Bracken_BacArcVirPla_lvl${lvl}" "${PROJECT_DIR}/Datasets/HMP/HMP12/statistics_reads.txt")

	exec_cmd "${RSCRIPTS}/plot_cor_simkamin_bracken.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_bivar_simka_bracken.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_dist_simka_bracken.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_cor_spriss_bracken.R ${args[@]}"
	exec_cmd "${RSCRIPTS}/plot_bivar_spriss_bracken.R ${args[@]}"
	exec_cmd "${RSCRIPTS}/plot_dist_spriss_bracken.R ${args[@]}"
done





RES_DIR="${RES_DIR_}/CAMI_HMP12"

for lvl in "S" "G";
do
	args=("${RES_DIR}/Spriss_MTDabs" "${RES_DIR}/Simkamin" "${RES_DIR}/Bracken_BacArcVirPla_lvl${lvl}" "${PROJECT_DIR}/Datasets/CAMI_HMP/CAMI_HMP12/statistics_reads.txt")

	exec_cmd "${RSCRIPTS}/plot_cor_true_simkamin_bracken.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_bivar_true_simka_bracken.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_dist_simka_true.R ${args[@]:1}"
	exec_cmd "${RSCRIPTS}/plot_cor_true_spriss_bracken.R ${args[@]}"
	exec_cmd "${RSCRIPTS}/plot_bivar_true_spriss_bracken.R ${args[@]}"
	exec_cmd "${RSCRIPTS}/plot_dist_spriss_true.R ${args[@]}"
done
