#!/bin/bash

###########################################################################################
# This script is used to run the SIMKA-SPRISS analysis of a given collection of datasets. #
# Please, notice that all your files' path names MUST NOT CONTAIN ANY BLANK SPACE.        #
# Remember to update parameters_setup.bash when varibles here are modified                #
#                                                                                         #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ###  Author: Giorgio Gallina                                                       #### #
# ###  Update: March 2023                                                            #### #
###########################################################################################

# TODO: manage --theta-method=rel
#       one different theta for each dataset: sampling_rates.R must be modified

trap cleanup SIGINT SIGTERM ERR EXIT


# -----------------------  LOAD ENVIRONMENT  ----------------------- #

# command line parameters
INPUT_ARGS=($@)

#get project directory
file_link=$( readlink -f -- "$0"; )
BASH_DIR=$( dirname -- "$file_link"; )
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )

# load common functions
source ${BASH_DIR}/utils.bash

TIME=$( date +%y%m%d_%H%M%S )

# Set environment option
shopt -s extglob # patsub_replacement



# ---------------------------  FUNCTIONS  --------------------------- #

cleanup() {
	trap - SIGINT SIGTERM ERR EXIT
	
	msg "INTERRUPT SIGNAL DETECTED... CLEANUP SHOULD BE PERFORMED"
#	rm -rf ${TMP_DIR}
#	rm -rf ${SMP_DIR}
#	rm -f ${curres_dir}/*.kmc_pre
#	rm -f ${curres_dir}/*.kmc_suf
}


function display_usage {
	cat <<EOF
Usage: $(basename -- "${BASH_SOURCE[0]}") [-h] [OPTIONS]...

This script runs SPRISS on a grid of k-mer lengths 'k's and frequency cut-offs 'theta's for each of the input datasets.
Then, beta-diversity between frequent k-mers estimates is computed, and beta-diversity between SPRISS' subsamples.
There are some default values which are overwritten by those values set with parameters_setup.bash and these, in turn, are overwritten by command line input parameters.

Options:
-h, --help         Print this message and exit.
-V, --version      Print version of this script and exit.
-c <fname>, --ds-collection=<fname>
                   Path to file where the collection's table is stored with at
                   least two colum named 'label', 'ds_name'.
-d <DS_DIR>, --dataset-dir=<DS_DIR>
                   Path to the directory where the datasets are stored.
-e <extens>, --extension=<extens>
                   Extension of the datasets, starting with a dot (eg '.fasta',
                   '.fna', '.fq'...)
-l <ds_lists_dir>, --ds-lists-dir=<ds_lists_dir>
                   Directory where lists of the datasets in current collection
                   are stored. [Default: <DS_DIR>/<collection_name>/lists ]
-o <outdir>, --out-dir=<outdir>
                   Path to the directory where to store output results.
-w <WORKDIR>, --work-dir=<WORKDIR>
                   Path to the directory where to store temporary results.
-m <RAM>, --memory=<RAM>
                   Ammount of available RAM in Gigabytes. [Default: 8]
-n <nCPUs>, --NCPU=<nCPUs>
                   Number of CPUs available for computation. [Default: 8]
-r <repetitions>, --repetitions=>repetitions>
                   Number of executions for each parameters setting. Currently
                   ignored (ie --repetitions=1).
-k <k-list>, --kmer-list=<k-list>
                   Comma-separated list of k-mer lengths k. [Default: 21,31]
-s <stats>, --statistics-dir=<stats>
                   Directory where collection's kmer-statistics are stored.
                   [Default: <DS_DIR>/<collection_name>]
-t <theta-list>, --theta-list=<theta-list>
                   Comma-separated list of k-mer frequency cut-offs. They are
                   expressed as values to be devided by --theta-norm. See also
                   --theta-method to better understand how to select such
                   thetas. [Default: 0.00000001,0.00000002,0.00000005,0.0000001
                   ,0.00000015,0.0000002]
--theta-method=<method>
                   A string in ('min', 'avg', 'abs', 'rel').
                      abs: each theta is divided by the value of --theta-norm.
                           I.e., theta / theta_norm
                      avg: each theta is divided by the average dataset size,
                           which is equivalent to set --theta-norm=avg_ds_size.
                           I.e., theta / avg_ds_size
                      min: each theta is divided by the minimum dataset size,
                           which is equivalent to set --theta-norm=min_ds_size.
                           I.e., theta / min_ds_size
                           [default --theta-norm is 1 when method is abs]
                      rel: each theta is divided by the actual dataset size,
                           that is equivalent to --theta-norm=current_ds_size.
                           I.e. theta / current_ds_size
                           WARNING: This method is not implemented yet.
                   [Default: abs]
--theta-norm=<value>
                   When --theta-method=abs, which is the default, this is the
                   value by which each theta is divided, that is:
                   0 <= (theta / theta-norm) <= 1.
                   When thetas are given as integers, this value is analogous
                   to a dataset's size from which only k-mers apparing more
                   than such integer thetas are considered frequent.
                   If --theta-method is different from 'abs', than this value
                   is ignored.
                   [Default: 1]
--continue         Use this option if you do not want SPRISS and SIMKA to be
                   run again when their results are already available.
--read-raref       Use this option to threshold the number of reads to that of
                   the smallest dataset. [TODO]
--random-seed      Run SimkaMin with a random seed rather than a fixed one,
                   which is the default.
--heatmap-rows=<list>
                   A comma-separated list of column names of those columns to
                   to be used as row annotations for heatmaps. It is possible
                   to provide column index numbers in place of column names by
                   starting the list with a comma. E.g.:
                    --heatmap-rows=clustering1,clustering2
                    --heatmap-rows=,4,3
                   [By default no row annotation are applied.]
--heatmap-columns=<list>
                   Same as --heatmap-rows but for heatmap column annotations.
                   
EOF
	exit
}



function arg_parser {
	ARGS=$(getopt -o hVvc:d:e:k:l:m:n:o:r:s:t:w: --long help,version,verbose,debug,ds-collection:,dataset-dir:,extension:,statistics:,ds-lists-dir:,out-dir:,work-dir:,NCPU:,memory:,repetitions:,kmer-list:,theta-list:,theta-norm:,theta-method:,continue,read-raref,random-seed:,heatmap-rows:,heatmap-columns: -- "$@")
	if [[ $? -ne 0 ]]; then
		msg "Error in parsing input arguments ( $* )"
		exit 1;
	fi
	
	read_rarefy=1
	heatmap_rows_cmd=""
	heatmap_cols_cmd=""

	#TODO: check file existence
	
	eval set -- "$ARGS"
	while [ : ]; do
	  case "$1" in
		-h | --help)
		    display_usage;
		    exit 0;
		    ;;
		-V | -v | --version)
			vstr="$(basename -- ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
			msg "$vstr"
			exit 0;
			;;
		--verbose)
			VERBOSE=0
			;;
		--debug)
			set -x
			;;
		-c | --ds-collection)
			collection_fname=$2
			if ! [ -f ${collection_fname} ]; then
				msg "ERROR: ${collection_fname} is not a file."
				exit 1;
			fi
		    shift
		    ;;
		-d | --dataset-dir)
			DS_DIR=$2
			if ! [ -d ${DS_DIR} ]; then
				msg "ERROR: ${DS_DIR} is not a directory."
				exit 1;
			fi
			shift
			;;
		-e | --extension) #TODO automatic detection
			EXTENSION=$2
			shift
			;;
		-k | --kmer-list)
			K_LIST=($( split ${2} "," ))
			shift
			;;
		-l | --ds-lists-dir)
			DS_LISTS_DIR=$2
			# existence check later
			shift
			;;
		-m | --memory)
			MEMG=$2
			shift
			;;
		-n | --NCPU)
			NCPU=$2
			shift
			;;
		-o | --out-dir)
			res_rawdir=$2
			shift
			;;
		-r | --repetitions)
			NREPT=$2
			msg "WARNING: unused option $1"
			shift
			;;
		-s | --statistics)
			STATS_DIR=$2
			if ! [ -d ${STATS_DIR} ]; then
				msg "ERROR: ${STATS_DIR} is not a directory."
				exit 1;
			fi
			STATS_BASE=${STATS_DIR}/${STATS_BASENAME}
			shift
			;;
		-t | --theta-list)
			THETA_LIST=($( split $2 "," ))
			shift
			;;
		-w | --work-dir)
			WORKDIR=$2
			shift
			;;
		--theta-norm)
			THETA_NORM=$2
			shift
			;;
		--theta-method)
			THETA_METHOD=$2
			if [[ $THETA_METHOD != "min" ]] || [[ $THETA_METHOD != "avg" ]] || [[ $THETA_METHOD != "abs" ]]; then
				msg "ERROR: $2 is a wrong value for $1. Please choose one among 'abs', 'avg', 'min'."
			fi
			# deprecated since --theta-norm=1 by default for every theta-methods
			if [[ ${THETA_METHOD} == "abs" ]]; then
				THETA_NORM_DEF=1
			fi
			shift
			;;
		--continue)
			CONTINUE=0
			;;
		--read-raref)
			read_rarefy=0
			;;
		--random-seed)
			rand_seed=0
			;;
		--heatmap-rows)
			heatmap_rows_cmd="-r $2"
			shift
			;;
		--heatmap-columns)
			heatmap_cols_cmd="-c $2"
			shift
			;;
		--) shift; 
		    break 
		    ;;
		*)
			msg "Error: unknown option $1"
			display_usage;
			exit 1;
			;;
	  esac
	  shift
	done
	
	# exit on mandatory parameters lacking
	if [[ "${DS_DIR}" == "" ]]; then
		msg "ERROR: Please use option -d to select the datasets' directory."
		exit 1
	fi
	if [[ "${EXTENSION}" == "" ]]; then
		# TODO: try to get it from datasets
		msg "ERROR: Please use option -e to declare the datasets' file extension."
		exit 1
	fi
	
	
	# collection details for heatmap and directories
	if [ -s ${collection_fname} ]; then
		COLLECTION_NAME=$( get_filename "${collection_fname}")
		heatmap_opts="-d ${collection_fname} ${heatmap_rows_cmd} ${heatmap_cols_cmd}"
	fi
	
	# default directory where statistics are stored
	DS_COLLECTION_DIR="${DS_DIR}/${COLLECTION_NAME}"
	if [[ ${STATS_DIR} == "" ]]; then
		STATS_DIR="${DS_COLLECTION_DIR}"
		if ! [ -d ${STATS_DIR} ]; then
			msg "ERROR: ${STATS_DIR} is not a directory."
			exit 1;
		fi
	fi
	
	# try to select a default output directory if not provided
	if [ -z ${res_rawdir} ]; then
		res_rawdir="${PROJECT_DIR}/Results/${COLLECTION_NAME}/Spriss"
	fi
	RESDIR="${res_rawdir}_MTD${THETA_METHOD}"
	mkdir -p ${RESDIR}
	
	# complete workdir name
	if [[ ${WORKDIR} != "" ]]; then
		WORKDIR=${WORKDIR}/Spriss_MET${THETA_METHOD}_${TIME}
	else
		WORKDIR=${PROJECT_DIR}/.workdir/Spriss_MET${THETA_METHOD}_${TIME}
	fi
	mkdir -p ${WORKDIR}
	
	# datasets lists management
	if [[ ${DS_LISTS_DIR} == "" ]]; then
		DS_LISTS_DIR="${DS_DIR}/${COLLECTION_NAME}/lists"
	fi
	if ! [ -d ${DS_LISTS_DIR} ]; then
		msg "ERROR: ${DS_LISTS_DIR} (datasets' lists) is not a directory."
		exit 1
	fi
	
	# set default THETA_NORM if not provided
	if [[ ${THETA_NORM} == "" ]]; then
		THETA_NORM=${THETA_NORM_DEF}
	fi
	
	INFO_PATH="${RESDIR}/${INFO_FNAME}"
	
	# RAM memory in Megabytes, lower than available for safety
	MEMM=$( simka_giga2mega $MEMG )
	
	# if --read-raref is set, limit the maximum number of reads to that of the smallest dataset (TODO)
	if [ ${read_rarefy} -eq 0 ]; then
		minreads=$( min $( ${RSCRIPTS}/get_table_values.R -s ";" -c nreads ${ds_statistics} ) )
#		max_reads_str=" -max-reads ${minreads} "
	fi
}








# ---------------------   Print info & exit   ---------------------- #
help_args=($(is_help_version ${@}))
if [ $? -eq 0 ] ; then
	arg_parser ${help_args[@]}
fi


# --------------------   Read input arguments   -------------------- #

#set -x

# DEFAULTS
# Remember to change parameters_setup.bash if you modify these parameters!
RSCRIPTS_DIR="${PROJECT_DIR}/Rscripts"
PARAM_DIR="${PROJECT_DIR}/.parameters"
DS_LISTS_DIR=""
INFO_FNAME="info.txt"
INFO_PATH=""
STATS_DIR=""
STATS_BASENAME="statistics_"
STATS_SUFFIX="mers.txt"
DS_DIR=""
EXTENSION=""
RESDIR=""
WORKDIR=""
collection_fname=""
COLLECTION_NAME=""
SIMKA_BRAYNAME="mat_abundance_braycurtis.csv"
SIMKA_JACCNAME="mat_presenceAbsence_jaccard.csv"
SPRISS_FREQ_DIR_NAME="SprissFreq"
SPRISS_SMP_DIR_NAME="SprissSMP"
CONTINUE=1

VERSION=1.1
VERBOSE=1	# non verbose (TODO)
THETA_DEC_DIGITS=20
MEMG=8
NCPU=8
NREPT=1
K_LIST=(16 21 26 31)
THETA_LIST=(0.00000001 0.00000002 0.00000005 0.0000001 0.00000015 0.0000002)
THETA_METHOD="abs"
THETA_NORM_DEF=1
THETA_NORM=""
rand_seed=1	# FALSE: keep a fixed random seed
heatmap_opts=""

# LOAD PRE-DEFINED VARIABLE VALUES
source ${PARAM_DIR}/param_sprissimka

# MANAGE INPUT ARGUMENTS
arg_parser ${INPUT_ARGS[@]}


# -------------------   Define other variables   ------------------- #

# LOAD INVOCATION STRING OF THE PROGRAMS TO BE USED
spriss=$(cat ${PARAM_DIR}/prg_spriss)
simka_progs=($( cat "${PARAM_DIR}/prg_simka"))
simka=${simka_progs[0]}
simkamin=${simka_progs[1]}
HEATMAPPER="${RSCRIPTS_DIR}/plot_heatmapCluster.R"


list_paths="${DS_LISTS_DIR}/label_path.txt"
list_simka="${DS_LISTS_DIR}/simka.txt"
SMP_DIR="${WORKDIR}/tmp_sample"
TMP_DIR="${WORKDIR}/tmp_data"

# array of datasets paths
paths_raw=($( cat ${list_paths} ))
status=$?
# exit on failure
if [ $status -ne 0 ]; then
	msg "Cannot read file ${list_paths}"
	msg "Aborting."
	exit $status
fi
# building collection associative array ([label]=ds_path)
declare -A ds_paths
make_associative ds_paths ${paths_raw[@]:2} # (skip header)

# get ordered labels
labels=($( sort_array ${!ds_paths[@]} ))


			
fullExtension=${EXTENSION}
if [[ ${EXTENSION} == ".fa" ]] || [[ ${EXTENSION} == ".fna" ]] ; then
	fullExtension=".fasta"
elif [[ ${EXTENSION} == ".fq" ]] ; then
	fullExtension=".fastq"
fi


#if [ -f ${INFO_PATH} ]; then
#	bkpdir="${INFO_PATH}/bkp${TIME}"
#	mkdir -p $(dirname -- $bkpdir)
#	mv ${INFO_PATH} $bkpdir/
#fi
#echo "label,k,theta_real,nbkmers" > ${INFO_PATH}
if ! [ -s ${INFO_PATH} ] ; then
	echo "label,k,theta_real,tot_nreads,smp_nreads,tot_nkmers,smp_nkmers,smp_rate" > ${INFO_PATH}
fi


# --------------------   Execution per each k   -------------------- #
for k in ${K_LIST[@]} ;
do
	msg "" 
	msg ""
	msg ""
	msg "============================  K-MER LENGTH $k  ============================"
	msg ""
	
	# get vector of datasets sizes measured in #kmers
	current_stats="${STATS_DIR}/${STATS_BASENAME}${k}${STATS_SUFFIX}"
	nkemrs_raw=($( ${RSCRIPTS_DIR}/get_table_values.R -s ";" -c label,nkmers --rowMajor ${current_stats} ))
	declare -A nkmers
	make_associative nkmers ${nkmers_raw[@]}
	
	# adapt reference size if proper
	if [[ "${THETA_METHOD}" == "min" ]]; then
		THETA_NORM=$(min ${nkmers[@]})
	elif [[ "${THETA_METHOD}" == "avg" ]]; then
		THETA_NORM=$(avg ${nkmers[@]})
	elif [[ "${THETA_METHOD}" == "rel" ]]; then
		THETA_NORM=1
	fi
	THETA_LIST_CURRENT=($( division -p ${THETA_DEC_DIGITS} -d ${THETA_NORM} ${THETA_LIST[@]} ))
	
	
	for theta in ${THETA_LIST_CURRENT[@]}
	do
		msg ""
		msg "============================  THETA ${theta}  ============================"
		msg ""	
		
		# Set environment option for string expansion
		shopt -s extglob # patsub_replacement

		# Prepare output directory name
		# theta will be in the form: [^0]*([[:digit:]])*(0)E-+([[:digit:]])
		# that is <number>E-<exponent> such that theta = 0.<number> * 10^<exponent>
		# EG: theta=0.000040600, then theta_outdir="*_th406E-4", as 0.406*10^(-4)=0.0000406=theta
		th=${theta#*(0).}	# get decimal part
		th=${th%%*(0)}		# delete trailing zeros
		lz=${th%%[^0]*}		# get leading zeros
		expon="E-${#lz}"	# get exponent of 0.${th##*(0)} to get theta
		th=${th##*(0)}		# delete leading zeros
		
		# theta for title in plots (scientific notation)
#		theta_str=${th/#?/&.} # DEPRECATED (patsub_replacement not always present)
		theta_str=$(echo ${th} | sed "s/^./&./")
		if [[ ${theta_str} == *. ]]; then
			theta_str="${theta_str}0"
		fi
		theta_str="${theta_str}E-$(( ${#lz} + 1 ))" # theta in scientific notation
		
		# directory where results relative to current theta and k are stored
		theta_outdir="${RESDIR}/k${k}_th${th}${expon}"
		mkdir -p ${theta_outdir}
		
		# ---------------  GET SAMPLING STATISTICS  --------------- #
		smp_rates_path="${theta_outdir}/smpstats.txt"
		# <k> <theta> <delta> <epsilon> <statistics_path> <output_path>
		rawv=($( exec_cmd "Rscript ${RSCRIPTS_DIR}/spriss_sampling_rates.R $k $theta 0.1 -1 $current_stats $smp_rates_path" ))

		# save sample sizes into associative array
		rawv=($(cat $smp_rates_path)) # deprecated
		declare -A smp_rates
		make_associative smp_rates ${rawv[@]:2}


		# -----------------  Repeat to reduce "random noise"  ----------------- #
		for rept in $(seq $NREPT)
		do
			msg ""
			msg ""
			msg
			msg "=====================  Iteration $rept  ====================="
			msg

			# random seed: currently ignored
			if [ ${rand_seed} -eq 0 ]; then
				let seed=($RANDOM%250)+1
				seedstr="--seed ${seed}"
			else
				seedstr=""
			fi

			if [ $NREPT -eq 1 ]; then
				curres_dir=${theta_outdir}
			else
				curres_dir="$theta_outdir/run$rept"
			fi
			curres_freq_dir="${curres_dir}/${SPRISS_FREQ_DIR_NAME}"
			mkdir -p ${curres_freq_dir}
			mkdir -p ${TMP_DIR}
			rm -fr ${SMP_DIR}
			mkdir -p ${SMP_DIR}

			# SPRISS: SAMPLE AND GET FREQUENT K-MERS ESTIMATES FOR EACH DATASET
			msg " ~~~~~~~~~~~~~~~ Running SPRISS on each dataset ~~~~~~~~~~~~~~~"
			
			recompile_cond=0
			if [ ${CONTINUE} -eq 0 ] && [ -s ${curres_dir}/${SPRISS_SMP_DIR_NAME}/${SIMKA_BRAYNAME} ] && [ -s ${curres_dir}/${SPRISS_SMP_DIR_NAME}/${SIMKA_JACCNAME} ];
			then
				recompile_cond=1
			fi
			
			for label in ${labels[@]}
			do
				# run SPRISS, which also creates sample file
				theta_final=${theta} # redundant, but would be useful if theta-method=rel gets implemented.
				msg ""
				msg ""
				infile=${ds_paths[$label]}
				infname=$(get_filename ${infile})
				current_freq_file=$(ls ${curres_freq_dir}/*${infname}*_frequent_${k}-mers_estimates.txt 2> /dev/null)
				if [ ${recompile_cond} -eq 0 ] || ! [ -s ${current_freq_file} ]; then
					exec_cmd "python3 $spriss -i "$infile" -th $theta_final -k $k -m $MEMG --out-dir ${TMP_DIR} --threads $NCPU "

					# move frequent estimates
					mv ${TMP_DIR}/*${infname}*_frequent_${k}-mers_estimates.txt ${curres_freq_dir}/
					mv -v -t ${SMP_DIR}/ ${TMP_DIR}/${infname}_sample${fullExtension}
					rm -f ${TMP_DIR}/*.kmc_pre
					rm -f ${TMP_DIR}/*.kmc_suf
				fi

				if [ $rept -eq 1 ]; then
					echo "${label},${k},${theta_str},${smp_rates[$label]}" >> ${INFO_PATH}
				fi
			done

			# DISTANCE ANALYSIS BASED ON SPRISS' FREQUENT KMERS ESTIMATES
			freq_matrix_basename="${curres_freq_dir}/spriss_freq"
			msg ""
			msg ""
#			exec_cmd "Rscript ${RSCRIPTS_DIR}/betaDiversity_spriss.R $k $collection_fname ${curres_freq_dir} ${freq_matrix_basename}" #DEPRECATED
			exec_cmd "${PROJECT_DIR}/bin/betaDiversity_spriss $k $collection_fname ${curres_freq_dir} ${freq_matrix_basename}"
			
			# HEATMAPS OF DISTANCE MATRICES
			bray_matrix="${freq_matrix_basename}_BC.csv"
			jacc_matrix="${freq_matrix_basename}_JAC.csv"

			bray_title="BC dissimilarity on SPRISS\' frequent ${k}-mers estimates - theta=${theta_str}"
			jacc_title="Jaccard distance on SPRISS\' frequent ${k}-mers estimates - theta=${theta_str}"

			bray_out="${curres_dir}/heatmap_freqSpriss_BC.pdf"
			jacc_out="${curres_dir}/heatmap_freqSpriss_JAC.pdf"

			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${bray_matrix} \"${bray_title}\" ${bray_out}"

			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${jacc_matrix} \"${jacc_title}\" ${jacc_out}"
			



			msg ""
			msg " ~~~~~~~~~~~~~~~ Running SIMKA on SPRISS' samples ~~~~~~~~~~~~~~~"
			
			# new result folder
			curres="${curres_dir}/${SPRISS_SMP_DIR_NAME}"
			mkdir -p ${curres}
			
			
			if [ $recompile_cond -eq 0 ]; then
				# Create Simka input list for execution on SPRISS samples
				list_simkasmp="${SMP_DIR}/simkasmp.txt"
				# <DS_COLLECTION_DIR> <lbl_dsName> <suffix> <result>
				exec_cmd "Rscript ${RSCRIPTS_DIR}/make_simka_input_list.R ${SMP_DIR} ${collection_fname} _sample${fullExtension} ${list_simkasmp}"

				#RUNNING EXACT SIMKA ON SPRISS' SAMPLES
				msg ""
				msg ""
				exec_cmd "$simka -in $list_simkasmp -out ${curres} -out-tmp ${TMP_DIR} -kmer-size $k -abundance-min 2 -max-memory $MEMM -nb-cores $NCPU "
				gunzip ${curres}/*.gz
			fi
			
			# clean-up
			rm -fr ${TMP_DIR}
			# remove samples
			rm -r ${SMP_DIR}
			
			
			# HEATMAPS OF DISTANCE MATRICES
			bray_matrix="${curres}/${SIMKA_BRAYNAME}"
			jacc_matrix="${curres}/${SIMKA_JACCNAME}"

			bray_title="BC dissimilarity on SPRISS\' samples - k=${k}, theta=${theta_str}"
			jacc_title="Jaccard distance on SPRISS\' samples - k=${k}, theta=${theta_str}"

			bray_out="${curres_dir}/heatmap_SprissSMP_BC.pdf"
			jacc_out="${curres_dir}/heatmap_SprissSMP_JAC.pdf"

			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${bray_matrix} \"${bray_title}\" ${bray_out}"
			
			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${jacc_matrix} \"${jacc_title}\" ${jacc_out}"



# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  DEPRECATED  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #
#			# SIMKAMIN
#			msg ""
#			msg " ~~~~~~~~~~~~ Running SimkaMin with equivalent sampling size ~~~~~~~~~~~~"
#			
#			curres="${curres_dir}/${SIMKAMIN_DIR_NAME}"
#			mkdir -p ${curres} ${TMP_DIR}
#			
#			bray_matrix="${curres}/${SIMKA_BRAYNAME}"
#			jacc_matrix="${curres}/${SIMKA_JACCNAME}"
#
#			if [ $CONTINUE -ne 0 ] || ! [ -s $bray_matrix ] || ! [ -s $jacc_matrix ] ; then
#				exec_cmd "$simkamin -k $k -nb-kmers ${nb_kmers} -in $list_simka -out ${TMP_DIR} -filter -max-memory $MEMM -nb-cores $NCPU ${seedstr} "
#
#				rm -rf ${TMP_DIR}/simkamin
#				mv ${TMP_DIR}/* ${curres}/
#				gunzip ${curres}/*.gz
#			fi
#			
#			rm -rf ${TMP_DIR}
#			
#			# HEATMAPS OF DISTANCE MATRICES
#			bray_title="BC dissimilarity by SimkaMin - n_${k}mers=${nb_kmers}, theta=${theta_str}"
#			jacc_title="Jaccard distance by SimkaMin - n_${k}mers=${nb_kmers}, theta=${theta_str}"
#
#			bray_out="${curres_dir}/heatmap_SimkaMin_BC.pdf"
#			jacc_out="${curres_dir}/heatmap_SimkaMin_JAC.pdf"
#
#			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${bray_matrix} \"${bray_title}\" ${bray_out}"
#
#			exec_cmd "${HEATMAPPER} ${heatmap_opts} ${jacc_matrix} \"${jacc_title}\" ${jacc_out}"

# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  DEPRECATED  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< #

		done
	
	done
	
done


# TODO: average results
