#!/bin/bash

################################################################################
# ###   Shell script for running BRACKEN on given datasets and computing   ### #
# ###   Bray-Curtis dissimilarity matrix.                                  ### #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: February 2023                                                        #
################################################################################
#TODO: manage failures

# set -x



# ############################  LOAD ENVIRONMENT  ############################ #

# command line parameters
INPUT_ARGS=($@)

# WARNING: it is assumed that the user has previously executed:
#   conda create -c bioconda -n bracken blast kraken2 bracken krakentools
source ~/.bashrc
conda activate bracken


# absolute path to project directory
script_link=$( readlink -f -- "$0"; )		# $PROJECT_DIR/BashScripts/run_bracken.bash
BASH_DIR=$( dirname -- "$script_link"; )	# $PROJECT_DIR/BashScripts
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )	# $PROJECT_DIR

# load general functions
source ${BASH_DIR}/utils.bash

TIMESTAMP=$( date +"%y%m%d_%H%M%S" ; )



# ###############################  FUNCTIONS  ################################ #


function display_usage {
	cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [OPTIONS]...

This script runs KRAKEN2 and BRACKEN to estimate taxon abundance for a collection of metagenomic datasets and thence computes their estimated beta-diversity (Bray-Curtis and Jaccard) and print it as a heatmap in a PDF file.

Options:
-h, --help         Print this message and exit.
-V, --version      Print version of this script and exit.
-c <fname>, --ds-collection=<fname>
                   Path to file where the collection's table is stored with at
                   least two colum named 'label', 'ds_name'.
-p <fname>, --ds-path-collection=<fname>
                   Path to file to the datasets' label-to-paths table.
-s <fname>, --ds-statistics=<fname>
                   Path to file to the table of global (reads) statistics of
                   the datasets.
-r <refDB>, --reference-db=<refDB>
                   Path to the reference database as required by KRAKEN.
                   Default to $(dirname $(dirname "${BASH_SOURCE[0]}"))/Databases/std
-t <taxonLVL>, --taxon-level=<taxonLVL>
                   A single capital letter for the taxon level to analyse. For
                   instance, 'S' for species, 'G' for genus, 'F' for family...
                   Default 'S'.
-o <outdir>, --out-dir=<outdir>
                   Path to the directory where to store output results.
-w <workdir>, --work-dir=<workdir>
                   Path to the directory where to store temporary results.
-n <ncpus>, --ncpus=<ncpus>
                   Number of CPUs available for computation. [Default: 8]
--force-classification
                   Use this option to recompute KRAKEN 2 classification even if
                   already stored in working directory.
--force-distrib-creation
                   Use this option to recreate bracken kmers distributions even
                   if already stored in Databases directory.
--readlen-actual   Use this option to estimate abundances by using for each
                   dataset its actual average read length (time consuming).
--readlen-global   Estimate abundances by using the average read length of
                   all datasets (option selected by default).
--heatmap-rows=<list>
                   A comma-separated list of column names of those columns to
                   to be used as row annotations for heatmaps. It is possible
                   to provide column index numbers in place of column names by
                   starting the list with a comma. E.g.:
                    --heatmap-rows=clustering1,clustering2
                    --heatmap-rows=,4,3
                   [By default no row annotation are applied.]
--heatmap-columns=<list>
                   Same as --heatmap-rows but for heatmap column annotations.

EOF
	exit
}


function arg_parser {
	ARGS=$(getopt -o hVvc:n:o:p:r:s:t:w: --long help,version,verbose,debug,ds-collection:,ds-path-collection:,ds-statistics:,reference-db:,taxon-level:,out-dir:,work-dir:,ncpus:,force-classification,force-distrib-creation,readlen-actual,readlen-global,heatmap-rows:,heatmap-columns: -- "$@")
	if [[ $? -ne 0 ]]; then
		msg "Error in parsing input arguments ( $* )"
		exit 1;
	fi

	#TODO: check file existence
	heatmap_rows_cmd=""
	heatmap_cols_cmd=""
	
	eval set -- "$ARGS"
	while [ : ]; do
	  case "$1" in
		-h | --help)
		    display_usage;
		    exit 0;
		    ;;
		-V | -v | --version)
			vstr="$(basename ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
			msg "$vstr"
			exit 0;
			;;
		--verbose)
			VERBOSE=0
			;;
		--debug)
			set -x
			;;
		-c | --ds-collection)
			collection_fname=$2
			if ! [ -f ${collection_fname} ]; then
				msg "ERROR: ${collection_fname} is not a file."
				exit 1;
			fi
		    shift
		    ;;
		-n | --ncpus)
			NCPU=$2
			shift
			;;
		-o | --out-dir)
			RESDIR=$2
			shift
			;;
		-p | --ds-path-collection)
			DS_PATH_LIST=$2
			if ! [ -f ${DS_PATH_LIST} ]; then
				msg "ERROR: ${DS_PATH_LIST} is not a file."
				exit 1;
			fi
			shift
		    ;;
		-r | --reference-db)
			DATABASE=$2
			if ! [ -d ${DATABASE} ]; then
				msg "ERROR: database ${DATABASE} not found."
				exit 1;
			fi
			shift
			;;
		-s | --ds-statistics)
		    DS_STATISTICS=$2
		    if ! [ -f ${DS_STATISTICS} ]; then
				msg "ERROR: ${DS_STATISTICS} is not a file."
				exit 1;
			fi
		    shift
		    ;;
		-t | taxon | taxon-level)
			TAXON_LEVEL=$2
			if [[ "SGFOCPK" != *${TAXON_LEVEL}* ]]; then
				mgs "ERROR: wrong taxon level ${TAXON_LEVEL}. Accepted values are: S G F O C P K."
				msg "       Se BRACKEN manual for reference."
				msg "       Using default taxon level S (species)."
				TAXON_LEVEL="S"
			fi
			shift
			;;
		-w | --work-dir)
			WORKDIR=$2
			shift
			;;
		--force-classification)
			FORCE_CLASSIF=0
			;;
		--force-distrib-creation)
			FORCE_DISTRIB=0
			;;
		--readlen-actual)
			USE_GLOB_RLEN=1
			;;
		--readlen-global)
			USE_GLOB_RLEN=0
			;;
		--heatmap-rows)
			heatmap_rows_cmd="-r $2"
			shift
			;;
		--heatmap-columns)
			heatmap_cols_cmd="-c $2"
			shift
			;;
		--) shift; 
		    break 
		    ;;
		*)
			msg "Error: unknown option $1"
			display_usage;
			exit 1;
			;;
	  esac
	  shift
	done
	
	
	
	# exit on mandatory parameters lacking
	must_exit=1
	if [[ "${DATABASE}" == "" ]]; then
		msg "ERROR: Please use option -r to select a refernce database."
		must_exit=0
	fi
	if [[ "${DS_STATISTICS}" == "" ]]; then
		msg "ERROR: Please use option -s to provide datasets' general statistics."
		must_exit=0
	fi
	if [[ "${DS_PATH_LIST}" == "" ]]; then
		msg "ERROR: Please use option -p to provide datasets' paths."
		must_exit=0
	fi
	if [[ "${collection_fname}" == "" ]]; then
		msg "ERROR: Please use option -c to provide datasets collection details."
		must_exit=0
	fi
	if [ $must_exit -eq 0 ]; then
		exit 1
	fi
	
	
	
	# collection details for heatmap and directories
	if [ -f ${collection_fname} ]; then
		COLLECTION_NAME=$( get_filename "${collection_fname}")
		heatmap_opts="-d ${collection_fname} ${heatmap_rows_cmd} ${heatmap_cols_cmd}"
	fi
	
	DB_NAME=$( basename -- ${DATABASE} )
	WORKDIR="${WORKDIR}_${DB_NAME}"

	# try to select a default output directory if not provided
	if [[ ${RESDIR} == "" ]]; then
		RESDIR="$PROJECT_DIR}/Results/${COLLECTION_NAME}/Bracken"
	fi
	RESDIR="${RESDIR}_${DB_NAME}_lvl${TAXON_LEVEL}"
	
	
	if [ $VERBOSE -eq 0 ]; then
		vstr="$(basename ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
		msg "***************************************************"
		msg "$vstr"
		msg "DATABASE: ${DATABASE}"
		msg "DATASETS COLLECTION: ${collection_fname}"
		msg "DATASETS' PATHS COLLECTION: ${DS_PATH_LIST}"
		msg "DATASETS GENERAL STATISTICS: ${DS_STATISTICS}"
		msg "TAXON LEVEL: ${TAXON_LEVEL}"
		msg "RESULTS DIRECTORY: ${RESDIR}"
		msg "WORKING DIRECTORY: ${WORKDIR}"
		msg "N. of CPUs: ${NCPU}"
		msg "USING GLOBAL READ LENGTH: "$( [ $USE_GLOB_RLEN -eq 0 ] && echo "yes" || echo "no" )
		msg "FORCING CLASSIFICATION: "$( [ $FORCE_CLASSIF -eq 0 ] && echo "yes" || echo "no" )
		msg "FORCING DISTRIBUTION RE-BUILDING: "$( [ $FORCE_DISTRIB -eq 0 ] && echo "yes" || echo "no" )
		msg "***************************************************"
		msg
	fi
}


# nearest_val <my_value> <list of target values>
# integer values only!
# eg: nearest_val 50 3 27 10 55 100 48 30
#     returns 48 as it is the nearest value to 50
function nearest_val {
	base=$1
	tv1=$2
	shift
	
	# set first target value as best value
	best=$tv1
	# get distance
	let dif=$base-$tv1
	# get absolute distance
	bdif=${dif#-}
	
	for tv in "$@" ;
	do
		# get current distance
		let cdif=$base-$tv
		cdif=${cdif#-}
		
		# update best tg value and distance
		if [ $cdif -lt $bdif ] ; then
			best=$tv
			bdif=$cdif
		fi
	done
	msg $best
}





# print output and exit if help or version requested.
help_args=($(is_help_version ${@}))
if [ $? -eq 0 ]; then
	arg_parser ${help_args[@]}
fi

# ############################  VARIABLES SETUP  ############################# #

# DEFAULTS
VERSION="2.3"
VERBOSE=1
TAXON_LEVEL='S'
DATABASE="${PROJECT_DIR}/Databases/std"
DB_FIXED="${PROJECT_DIR}/Databases/fixed"
COLLECTION_NAME=""
collection_fname=""
heatmap_opts=""
DS_PATH_LIST=""
RESDIR=""
WORKDIR="${PROJECT_DIR}/.workdir/Bracken"
VERBOSE=1 #non verbose by default (TODO)
rlengths_ref=(50 75 100 150 200 250 300) #TODO: automatic length detection
NCPU=8
USE_GLOB_RLEN=0
FORCE_CLASSIF=1
FORCE_DISTRIB=1
curr_kmer_distrib=""
KMER_DISTRIB='printf -v curr_kmer_distrib "%s/database%smers.kmer_distrib" ${DATABASE} ${rlen}'

# pre-defined parameters
def_param="$PROJECT_DIR/.parameters/param_bracken"
if [ -e "$def_param" ]; then
#	readarray -t DEFAULT_ARGS < $def_param
#	status=$?
#	# try again with different method on failure
#	if [ $? -ne 0 ]; then
#		args=($( cat "$def_param" ))
#		status=$?
#	fi
#
#	if [ $status -eq 0 ]; then
#		arg_parser ${DEFAULT_ARGS[@]}
#	else
#		msg "Warning: no default parameters found. You might want to run 'parameters_setup.bash' before"
#	fi
	source ${def_param}
else
	msg "Warning: no default parameters found. You might want to run 'parameters_setup.bash' before"
fi

# INPUT
arg_parser ${INPUT_ARGS[@]}



# bracken abundances estimates directory
BRACKEN_ABUN_DIR="${WORKDIR}/run_${TIMESTAMP}"


RSCRIPTS="${PROJECT_DIR}/Rscripts"
BRACKEN_INSTALLER="${BASH_DIR}/install_krakenSuite_git.bash"
BRACKEN_BUILDER="${BASH_DIR}/bracken_build_stdDB.bash"

declare -a bracken_res

# controls for the selected database (indicate eventual building failures)
ctrl_dir="${DATABASE}/.controls"

# get programs to be executed
progs=($( cat "${PROJECT_DIR}/.parameters/prg_krakenTools" ; ))
kraken2=${progs[0]}
kraken2_build=${progs[1]}
bracken=${progs[2]}
bracken_build=${progs[3]}
beta_krt=${progs[4]}



# ############################   DATABASE SETUP   ############################ #

# change directory to $PROJECT_DIR
cd $PROJECT_DIR

# ----------  create directories if necessary  ---------- #
mkdir -p $WORKDIR
mkdir -p $ctrl_dir
mkdir -p $RESDIR
mkdir -p $BRACKEN_ABUN_DIR
msg "Storing results in: $RESDIR"
msg


# ----------  get datasets collection ([label]=ds_name)  ---------- #
# read file
rawv=($(cat $DS_PATH_LIST))
status=$?
# exit on failure
if [ $status -ne 0 ]; then
	msg "Cannot read file $DS_PATH_LIST"
	msg "Aborting."
	exit $status
fi

# building collection associative array ([label]=ds_path)
declare -A ds_collection
make_associative ds_collection ${rawv[@]:2} # (skip header)


# get ordered datasets' labels
labels=($(printf '%s\n' "${!ds_collection[@]}"|sort))



# ------------  Datasets' read lengths management  ------------ #	

msg
msg "Getting average read length for each dataset"

# store average read length of each dataset
# avg read lengths: ([label]=readlen.avg.int)
declare -A rlengths
rawlen=($( exec_cmd "${RSCRIPTS}/get_table_values.R -s ';' -c 'label,readlen.avg.int' --rowMajor ${DS_STATISTICS}" ))
make_associative rlengths ${rawlen[@]}

# get average read length of all datasets in the collection
rlen_totavg=$( avg ${rlengths[@]} )
rlen_def=${rlen_totavg}




# ---------------  Build BRACKEN indices  --------------- #

msg
msg
msg "Building Kraken 2 & Bracken indices..."
# Build BRACKEN indices for $rlen_totavg
exec_cmd "$BRACKEN_BUILDER $NCPU $rlen_totavg $DATABASE"
# success of database creation
brack_db_status=$?

# manage database creation failure
if [ $brack_db_status -ne 0 ] ; then
	msg ""
	msg "ERROR: Database building failed."
	
	# software name without installation path means Bioconda installation
	if [[ "$bracken" == "bracken" ]] ; then
		msg "Retrying by Git installation (currently using Bioconda)"
		
		# git install
		exec_cmd "bash ${BRACKEN_INSTALLER}"
		status=$?
		
		# reload program paths (unchanged in case of installation failure)
		progs=($( cat "${PROJECT_DIR}/.parameters/prg_krakenTools" ; ))
		kraken2=${progs[0]}
		kraken2_build=${progs[1]}
		bracken=${progs[2]}
		bracken_build=${progs[3]}
		beta_krt=${progs[4]}
		
		# if successful installation, try database building again
		if [ $status -eq 0 ]; then
			msg
			msg "Building Kraken 2 & Bracken indices..."
			# Build BRACKEN indices for $rlen_totavg
			exec_cmd "$BRACKEN_BUILDER $NCPU $rlen_totavg $DATABASE"
			# Check if the creation was successful with Bracken
			brack_db_status=$?
		else
			msg "Hard (git) installation failed."
		fi
	fi
fi

# On failure of database building, switch to pre-compiled databace indices
# (such indices, however, do not provide possibility to run BRACKEN with any
#  read length, but only 50, 75, 100, 150, 200, 250, or 300 are available).
if [ $brack_db_status -ne 0 ] ; then
	msg ""
	msg "WARNING: switching to pre-compiled default database $DB_FIXED"
	DATABASE=$DB_FIXED
	# use pre-compiled indices if available
	rlen_def=$( nearest_val $rlen_totavg ${rlengths_ref[@]} )
	for label in ${labels[@]} ;
	do
		# amend lengths for using pre-compiled indices
		rlengths[$label]=$rlen_def
	done
elif [ $USE_GLOB_RLEN -ne 0 ]; then
	rlen_def="ACTUAL"
fi

msg
msg "... Database building done."
msg



# ########################  DS ABUNDANCES ESTIMATION  ######################## #

# Estimate abundances with Bracken for each dataset
for label in ${labels[@]} ;
do
	dataset=${ds_collection[$label]}
	msg "# ====================================================================== #"
	msg "DATASET $label: $dataset"
	msg
	
	# Get dataset name and use it for output file-names' root
	ds_name=$( basename -- "$dataset" )
	ds_name=${ds_name%%.*}
	dataset_out="$RESDIR/$ds_name"
	current_kraken_res="$WORKDIR/$ds_name"
	current_bracken_abun="${BRACKEN_ABUN_DIR}/${ds_name}"
	
	# Get average read length to use for current dataset 
	if [ $USE_GLOB_RLEN -eq 0 ]; then
		# one global average length for every ds
		rlen=${rlen_totavg}
	else
		# actual read length of current ds
		rlen=${rlengths[${label}]}
	fi
	msg "Using read length: $rlen"
	current_kraken_res="${current_kraken_res}_rl${rlen}"
	
	# Get kmer distribution filename
	eval -- ${KMER_DISTRIB}
	
	# Build BRACKEN index (if not already built)
	if [ $FORCE_DISTRIB -eq 0 ] || ! [ -e $curr_kmer_distrib ]; then
		msg
		msg "Building BRACKEN index for read length ${rlen}"
		exec_cmd "$bracken_build -d ${DATABASE} -t ${NCPU} -l ${rlen} "
	fi

	# Classify reads with KRAKEN2
	if [ $FORCE_CLASSIF -eq 0 ] || ! [ -f ${current_kraken_res}.kraken ] || ! [ -f ${current_kraken_res}.kreport ]; then
		msg
		msg "Classifying dataset ${label} with KRAKEN 2"
		exec_cmd "$kraken2 --db ${DATABASE} --threads ${NCPU} --report ${current_kraken_res}.kreport ${dataset} > ${current_kraken_res}.kraken"
	fi
	
	# Estimate relative taxa abundances with BRACKEN
	msg
	msg "Estimating abundances with BRACKEN at taxon level ${TAXON_LEVEL}"
	exec_cmd "$bracken -d ${DATABASE} -i ${current_kraken_res}.kreport -o ${current_bracken_abun}.bracken -r ${rlen} -l ${TAXON_LEVEL}"
	
	msg
	msg "All done."
	
	# Prepare input list for BC dissimilarity measurement
	bracken_res+=(${current_bracken_abun}.bracken)
	
	msg
	msg
done

# store list of bracken abundances estimations  -- DISMISSED
# abun_list_res="${WORKDIR}/abunList.txt"
# ls -a1 ${WORKDIR}/*.bracken > ${abun_list_res}


# #######################  BRAY-CURTIS DISSIMILARITY  ######################## #

msg
msg "Computing Bray-Curtis and Jaccard dissimilarity matrices with estimated abundances." 

# Compute Bray-Curtis dissimilarity matrix Rscript
beta_out="$RESDIR/betadiv_taxon${TAXON_LEVEL}"
exec_cmd "$RSCRIPTS/betaDiversity_bracken.R -t ${TAXON_LEVEL} -o ${beta_out} ${collection_fname} ${BRACKEN_ABUN_DIR}"


# Compute Bray-Curtis dissimilarity matrix krakentools (deprecated)
beta_out_kt="$RESDIR/BrayCurtis_dissim_t${TAXON_LEVEL}.txt"
exec_cmd "${beta_krt} -i ${bracken_res[@]} --type bracken --level ${TAXON_LEVEL} > $beta_out_kt"




# ##############################  PLOT RESULTS  ############################## #

msg ""
msg "Plotting Heatmaps of dissimilarity matrices."

bray_matrix="${beta_out}_BC.csv"
jacc_matrix="${beta_out}_JAC.csv"

brack_db_status=1 #currently using one rlen only for all datasets
if [ $brack_db_status -eq 0 ]; then
	bray_title="BC dissimilarity by BRACKEN abundances, taxon ${TAXON_LEVEL}"
	jacc_title="Jaccard distance by BRACKEN abundances, taxon ${TAXON_LEVEL}"
else
	bray_title="BC dissimilarity by BRACKEN abundances - Read length ${rlen_def}, taxon ${TAXON_LEVEL}"
	jacc_title="Jaccard distance by BRACKEN abundances - Read length ${rlen_def}, taxon ${TAXON_LEVEL}"
fi

bray_out="${RESDIR}/heatmap_bracken_taxon${TAXON_LEVEL}_BC.pdf"
jacc_out="${RESDIR}/heatmap_bracken_taxon${TAXON_LEVEL}_JAC.pdf"

exec_cmd "Rscript ${RSCRIPTS}/plot_heatmapCluster.R ${heatmap_opts} ${bray_matrix} \"${bray_title}\" ${bray_out}"

exec_cmd "Rscript ${RSCRIPTS}/plot_heatmapCluster.R ${heatmap_opts} ${jacc_matrix} \"${jacc_title}\" ${jacc_out}"

