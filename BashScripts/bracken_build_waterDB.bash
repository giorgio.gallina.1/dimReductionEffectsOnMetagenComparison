#!/bin/bash

################################################################################
# ###   Shell script for running BRACKEN on given datasets and computing   ### #
# ###   Bray-Curtis dissimilarity matrix.                                  ### #
# #######                  --------------------------                  ####### #
# USAGE:                                                                       #
#  ./bracken_build_stdDB.bash <n_cpus> <r-len> <ref_db> [-f]                   #
# Arguments:                                                                   #
#      <n_cpus>:  Number of cpus available for computation (suggest >20).      #
#      <r-len>:   Reads length (see Bracken doc.).                             #
#      <ref_db>:  Name of reference database as for kraken2.                   #
#      -f:        Force re-building from scratch                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: February 2023                                                        #
################################################################################


function download_lib {
	local progr=$1
	local lib=$2
	local dbname=$3
	local ctrl=$4
	
	# return if control library already successfully downloaded
	if [ -e $ctrl ] && [ $(cat $ctrl) -eq 0 ] && [-d "${dbname}/library/${lib}" ]; then
		return 0
	fi
	
	# avoid problems with "which" alias
	which which &> /dev/null
	if [ $? -ne 0 ]; then
		alias which="/usr/bin/which"
	echo "WARNING: which program not found. It is now being aliased to '/usr/bin/which', but issues likely to rise." >&2
	fi
	
	MASKER="dustmasker"
	# try to symlink dustmasker if not in $PATH variable
	command -v ${MASKER} &> /dev/null
	status=$?
	if [ $status -ne 0 ]; then
		if [ -e "${project_dir}/.parameters/prg_${MASKER}" ]; then
			masker_path=$(cat "${project_dir}/.parameters/prg_${MASKER}")
			ln -s ${masker_path} /usr/local/bin/${MASKER}
			source ~/.bashrc
		fi
	fi
	
	# do not try with low complexity masking if dustmasker is not found in path
	if ! which $MASKER &> /dev/null; then
		cmds=("$progr $lib --db $dbname --use-ftp --no-masking" \
			  "$progr $lib --db $dbname --no-masking ")
	else	
		cmds=("$progr $lib --db $dbname" \
			  "$progr $lib --db $dbname --use-ftp" \
			  "$progr $lib --db $dbname --no-masking" \
			  "$progr $lib --db $dbname --use-ftp --no-masking ")
	fi
	      
	for cmd in "${cmds[@]}" ;
	do
		>&2 echo
		rm -rf $dbname/library/$lib
		>&2 echo $cmd
		eval "$cmd"
		status=$?
		# terminate if successfully downloaded
		if [ $status -eq 0 ] ; then
			echo $status > $ctrl
			echo "SUCCESSFULLY downloaded library $lib"
			echo
			return $staus
		fi
	done
	
	echo "$progr FAILED to download library $lib" >&2
	echo "" >&2
	echo $status > $ctrl
	return $status
}


# load environment
# WARNING: it is assumed that the user has previously executed:
#   conda create -c bioconda -n bracken blast kraken2 bracken krakentools
source ~/.bashrc
conda activate bracken



# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $project_dir/BashScripts/bracken_build_stdDB.bash
bash_dir=$( dirname -- "$file_link"; )		# $project_dir/BashScripts
project_dir=$( dirname -- "$bash_dir"; )	# $project_dir
# echo "Working directory: $project_dir"

# PARAMETERS
ncpu=$1
read_len=$2	# average (or minimum?) read length in the dataset to be analyzed
DBNAME=$3	# path to reference database as needed for kraken2
# clean databases directory ($DBNAME) if required
if [[ $4 == "-f" ]] ; then
	rm -rf $DBNAME/*
fi

# >>>>>>>>>> DISMISSED >>>>>>>>>>
bracken_build="${project_dir}/BRACKEN/Bracken-2.8/bracken-build"
krk2="${project_dir}/BRACKEN/kraken2-2.1.2/"
kraken2="$krk2/kraken2"
kraken2_build="$krk2/kraken2-build"
# ~~~~~~~~~~~~~~~~~~~~~
bracken_build="bracken-build"
bracken="bracken"
kraken2="kraken2"
kraken2_build="kraken2-build"
# <<<<<<<<<< DISMISSED <<<<<<<<<<

# get programs to be executed
progs=($( cat "${project_dir}/.parameters/prg_krakenTools" ; ))
kraken2=${progs[0]}
kraken2_build=${progs[1]}
bracken=${progs[2]}
bracken_build=${progs[3]}
#TODO: manage kraken2 if not in path (bracken option -x, problematic though)

downlib="$kraken2_build --download-library"
buildstd="$kraken2_build --standard"

# directory where exit codes are stored in order to avoid running
# again program already successfully executed.
ctrl_dir="$DBNAME/.controls"
mkdir -p $ctrl_dir




# change directory to $project_dir
cd $project_dir
gstatus=0


# skip db download if already successfully created
ctrl="${ctrl_dir}/.ready"
if ! [ -e "${ctrl}" ] || [ $(cat "$ctrl") -ne 0 ] ; then
	

	# DOWNLOAD TAXONOMY
	echo "Downloading taxonomy"
	ctrl="$ctrl_dir/.taxonomy"
	if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
		cmd="$kraken2_build --download-taxonomy --db $DBNAME"
		>&2 echo $cmd
		eval $cmd
		status=$?
		echo $status > $ctrl
		let gstatus+=$status
	fi


	# DOWNLOAD GENOMES IF NECESSARY
	echo
	echo "Downloading reference sequences library"
	
	ctrl="$ctrl_dir/.archaea"
	download_lib "${downlib}" archaea $DBNAME $ctrl
	let gstatus+=$?

	ctrl="$ctrl_dir/.viral"
	download_lib "${downlib}" viral $DBNAME $ctrl
	let gstatus+=$?

	ctrl="$ctrl_dir/.bacteria"
	download_lib "${downlib}" bacteria $DBNAME $ctrl
	let gstatus+=$?

	# plasmid has only ftp download, so not even trying rsync
	ctrl="$ctrl_dir/.plasmid"
#	download_lib "${downlib}" plasmid $DBNAME $ctrl
#	let gstatus+=$?
	if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
		cmds=("${downlib} plasmid --db ${DBNAME}" \
			  "${downlib} plasmid --db ${DBNAME} --no-masking" )
			  
		for cmd in "${cmds[@]}" ;
		do
			>&2 echo
			rm -rf $dbname/library/plasmid
			>&2 echo $cmd
			eval "$cmd"
			status=$?
			# terminate if successfully downloaded
			if [ $status -eq 0 ] ; then
				echo $status > $ctrl
				echo "SUCCESSFULLY downloaded library plasmid"
				echo
				break
			fi
		done
		if [ $status -ne 0 ]; then
			echo "${downlib} FAILED to download library plasmid" >&2
			echo "" >&2
			echo $status > $ctrl
		fi
	fi
	
	
		

	# BUILD DATABASE INDICES
	echo
	echo "Building KRAKEN 2 reference database"
	ctrl="$ctrl_dir/.kraken2_build"
	if [ $gstatus -eq 0 ] ; then
		status=0
		if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
			cmd="$kraken2_build --build --db $DBNAME"
			>&2 echo $cmd
			eval $cmd
			status=$?
			echo $status > $ctrl
		fi
	else
		echo 1 > $ctrl
#		echo "FAILED." >&2
#		echo "Trying again with standard program (${buildstd})." >&2
#		
#		ctrl="$ctrl_dir/.kraken2_build_std"
#		download_lib "${buildstd}" "" $DBNAME $ctrl
#		status=$?
#		
#		# if even this run has failed, return with error
#		if [ $status -ne 0 ] ; then
			echo "Fatal error." >&2
			exit $gstatus
		fi
			
	fi
	
	# Notify KRAKEN Database creation has been successfully created 
	echo $status > "${ctrl_dir}/.ready"
	
fi


	
# BUILD BRACKEN INDICES
echo
echo "Building BRACKEN reference database index for read length ${read_len}"
ctrl="$ctrl_dir/.bracken_build${read_len}"
if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
	cmd="$bracken_build -d ${DBNAME} -t ${ncpu} -l ${read_len}"
	>&2 echo $cmd
	eval $cmd
	status=$?
	echo $status > $ctrl
fi

echo
echo "BRACKEN database building completed."
echo

exit $status

