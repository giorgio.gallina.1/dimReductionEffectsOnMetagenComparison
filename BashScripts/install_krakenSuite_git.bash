#!/bin/bash

################################################################################
# ###   Shell script to be used in case of Bioconda installation failures. ### #
# #######                  --------------------------                  ####### #
#                                                                              #
# USAGE:                                                                       #
#  ./install_krakenSuite_git.bash                                              #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: January 2023                                                         #
################################################################################

# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $project_dir/BashScripts/bracken_build_stdDB.bash
bash_dir=$( dirname -- "$file_link"; )		# $project_dir/BashScripts
project_dir=$( dirname -- "$bash_dir"; )	# $project_dir

# project relevant directories
krakdir="${project_dir}/KRAKEN2"
brackdir="${project_dir}/BRACKEN"
param_dir="${project_dir}/.parameters"
scripts_subst_dir="${project_dir}/src"


# #########################  KRAKEN2 INSTALL  ######################### #

echo "* ********************  Installing KRAKEN2  ******************** *"

# clean up if already installed
rm -rf ${krakdir}
rm -rf ${brackdir}
mkdir -p ${krakdir}
mkdir -p ${brackdir}

cd ${krakdir}

# Download from git
echo "Downloading..."
cmd="wget -P ${krakdir} https://github.com/DerrickWood/kraken2/archive/refs/tags/v2.1.2.tar.gz"
>&2 echo $cmd
eval $cmd

# Decompress archive
echo "Decompressing..."
cmd="tar --directory ${krakdir} -xvzf ${krakdir}/v2.1.2.tar.gz"
>&2 $cmd
eval $cmd
status=$?

# delete archive or exit on error
if [ $status -eq 0 ]; then
	rm ${krakdir}/v2.1.2.tar.gz
else
	>&2 echo "ERROR: extraction of KRAKEN 2 archive failed with exit code $status. Aborting."
	exit $status
fi

# move to desired location
mv ${krakdir}/kraken2-2.1.2/* ${krakdir}/
if [ $? -eq 0 ]; then
	rm -r ${krakdir}/kraken2-2.1.2
fi


# SUBSTITUTE ISSUED SCRIPTS
# ftp issue
mv ${krakdir}/scripts/rsync_from_ncbi.pl ${krakdir}/scripts/rsync_from_ncbi.pl.bkp
cp ${scripts_subst_dir}/rsync_from_ncbi.pl ${krakdir}/scripts/
chmod +x ${krakdir}/scripts/rsync_from_ncbi.pl
# ftp issue
mv ${krakdir}/scripts/download_genomic_library.sh ${krakdir}/scripts/download_genomic_library.sh.bkp
cp ${scripts_subst_dir}/download_genomic_library.sh ${krakdir}/scripts/
chmod +x ${krakdir}/scripts/download_genomic_library.sh
# which issue
mv ${krakdir}/scripts/mask_low_complexity.sh ${krakdir}/scripts/mask_low_complexity.sh.bkp
cp ${scripts_subst_dir}/mask_low_complexity.sh ${krakdir}/scripts/
chmod +x ${krakdir}/scripts/mask_low_complexity.sh


# Install KRAKEN 2
echo "Installing..."
chmod +x ${krakdir}/install_kraken2.sh
cmd="$bash ${krakdir}/install_kraken2.sh \"${krakdir}\""
>&2 echo ${cmd}
eval ${cmd}
status=$?

if [ $status -ne 0 ]; then
	>&2 echo "KRAKEN 2 installation failed with exit status $status. Aborting."
	exit $status
fi


echo
echo "KRAKEN 2 installation completed."
echo
echo


# #########################  BRACKEN INSTALL  ######################### #

echo "* ********************  Installing BRACKEN  ******************** *"

cd ${brackdir}

# Download from git repository
echo "Downloading..."
cmd="wget -P ${brackdir} https://github.com/jenniferlu717/Bracken/archive/refs/tags/v2.8.tar.gz"
>&2 echo $cmd
eval $cmd

# Decompress archive
echo "Decompressing..."
cmd="tar --directory ${brackdir} -xvzf ${brackdir}/v2.8.tar.gz"
>&2 $cmd
eval $cmd
status=$?

# delete archive or exit on error
if [ $status -eq 0 ]; then
	rm ${brackdir}/v2.8.tar.gz
else
	>&2 echo "ERROR: extraction of BRACKEN archive failed with exit code $status. Aborting."
	exit $status
fi

# move to desired location
mv ${brackdir}/Bracken-2.8/* ${brackdir}/
if [ $? -eq 0 ]; then
	rm -r ${brackdir}/Bracken-2.8
fi

# fix unsuitable scripts
mv ${brackdir}/bracken-build ${brackdir}/bracken_build.bkp
cp ${scripts_subst_dir}/bracken-build ${brackdir}/
chmod +x ${brackdir}/bracken-build

# Install BRACKEN
echo "Installing..."
chmod +x ${brackdir}/install_bracken.sh
cmd="$bash ${brackdir}/install_bracken.sh"
>&2 echo ${cmd}
eval ${cmd}
status=$?

if [ $status -ne 0 ]; then
	>&2 echo
	>&2 echo "BRACKEN installation failed with exit status $status. Aborting."
	exit $status
fi

echo
echo "BRACKEN installation completed."
echo
echo -n "Ending up..."

echo "${krakdir}/kraken2" > "${param_dir}/prg_krakenTools"
echo "${krakdir}/kraken2-build" >> "${param_dir}/prg_krakenTools"
echo "${brackdir}/bracken" >> "${param_dir}/prg_krakenTools"
echo "${brackdir}/bracken-build" >> "${param_dir}/prg_krakenTools"
echo "beta_diversity.py" >> "${param_dir}/prg_krakenTools"

echo "... All done."

