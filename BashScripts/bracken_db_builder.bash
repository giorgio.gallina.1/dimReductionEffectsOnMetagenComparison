#!/bin/bash

################################################################################
# ###   Shell script for running BRACKEN on given datasets and computing   ### #
# ###   Bray-Curtis dissimilarity matrix.                                  ### #
# #######                  --------------------------                  ####### #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: February 2023                                                        #
################################################################################




# ##############################  ENVIRONMENT  ############################### #

# command line parameters
INPUT_ARGS=($@)

# WARNING: it is assumed that the user has previously executed:
#   conda create -c bioconda -n bracken blast kraken2 bracken krakentools
source ~/.bashrc
conda activate bracken


# absolute path to project directory
script_link=$( readlink -f -- "$0"; )		# $PROJECT_DIR/BashScripts/bracken_database_builder.bash
BASH_DIR=$( dirname -- "$script_link"; )	# $PROJECT_DIR/BashScripts
PROJECT_DIR=$( dirname -- "$BASH_DIR"; )	# $PROJECT_DIR

# load general functions
source ${BASH_DIR}/utils.bash

TIMESTAMP=$( date +"%y-%m-%d %H:%M:%S" ; )



# ###############################  FUNCTIONS  ################################ #


function display_usage {
	cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [OPTIONS]...

This script runs bracken-build repeatedly in order to build the desired database.


Options:
-h, --help         Print this message and exit.
-V, --version      Print version of this script and exit.
-d <dir>, --db-directory=<dir>, --db-dir=<dir>, --database=<dir>
                   Directory path where the database shall be created.
                   [Default: ${PROJECT_DIR}/Databases/std
                   or ${PROJECT_DIR}/Databases/std_nomask (whit -f opt).]
-f, --force-rebuilding
                   Use this option if you want the database to be build from
                   scratch even if part of it has already been created.
-l <rlen>, --read-length=<rlen>
                   Read length as needed by BRACKEN for abundance estimation.
                   It is common to use the average read length of the dataset
                   under classification. [Default: 100]
-n <ncpus>, --ncpus=<ncpus>
                   Number of CPUs available for computation. [Default: 8]
--no-mask          Use this option if you do not want low colpexity sequences
                   to be masked (i.e., excluded from database).
-r <commaSepList>, --references=<commaSepList>
                   Comma-separated list of references to download. See Kraken2
                   instructions. E.g.: -r bacteria,viral,archaea
                   [Default: archaea,bacteria,human,plasmid,UniVec_Core,viral]

EOF
	exit
}


function arg_parser {
	ARGS=$(getopt -o hVvd:fl:n:r: --long help,version,verbose,debug,db-dir:,db-directory:,database:,force-rebuilding,read-len:,read-length:,ncpus:,no-mask,references: -- "$@")
	if [[ $? -ne 0 ]]; then
		msg "Error in parsing input arguments ( $* )"
		exit 1;
	fi
	
	#DEFAULTS
	rebuild=1
	MASK_STR=""
	db_dir="${PROJECT_DIT}/Databases/std"
	DB_DIR=""
	READ_LEN=100
	VERBOSE=1
	NCPU=8
	REF_LIST=(archaea bacteria human plasmid UniVec_Core viral)
	VERSION=1.0
	
	
	eval set -- "$ARGS"
	while [ : ]; do
	  case "$1" in
		-h | --help)
		    display_usage;
		    exit 0;
		    ;;
		-V | -v | --version)
			vstr="$(basename ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
			msg "$vstr"
			exit 0;
			;;
		--verbose)
			VERBOSE=0
			;;
		--debug)
			set -x
			;;
		-d | --db-directory | --db-dir | --databse)
			DB_DIR=$2
			shift
			;;
		-f | --force-rebuilding)
			rebuild=0
			;;
		-l | --read-length | --read-len)
			READ_LEN=$2
			if [ ${READ_LEN} -lt 5 ] >/dev/null; then
				msg "ERROR: expected integer number (>= 5) for read length. Found instead: ${READ_LEN}"
				exit 1
			fi
			shift
			;;
		-n | --ncpus)
			NCPU=$2
			shift
			;;
		--no-mask)
			MASK_STR="--no-masking"
			;;
		-r | --references)
			REF_LIST=($(split $2 "," ))
			shift
			;;
		--) shift; 
		    break 
		    ;;
		*)
			msg "Error: unknown option $1"
			display_usage;
			exit 1;
			;;
	  esac
	  shift
	done
	
	
	# default DB_DIR if needed
	if [[ ${DB_DIR} == "" ]]; then
		DB_DIR=${db_dir}
		
		# Append "_nomask" to DB name when no-mask is invoked
		if [[ ${MASK_STR} != "" ]]; then
			DB_DIR="${DB_DIR}_nomask"
		fi
	fi
	
	# make DB_DIR if not already present
	mkdir -p ${DB_DIR}/.controls
	
	# remove everything from database before starting build it from scratch
	if [ ${rebuild} -eq 0 ]; then
		rm -rf $DB_DIR/*
		rm -rf $DB_DIR/.*
	fi
	
	
	if [ $VERBOSE -eq 0 ]; then
		vstr="$(basename ${BASH_SOURCE[0]}) version $VERSION" #TODO: manage quotes
		msg "****************************************************************************"
		msg "$vstr"
		msg "DATABASE: ${DB_DIR}"
		msg "REFERENCES: (${REF_LIST[@]})"
		msg "READ LENGTH: ${READ_LEN}"
		msg "N. of CPUs: ${NCPU}"
		msg "FORCING REBUILDING: "$( [ $rebuild -eq 0 ] && echo "yes" || echo "no" )
		msg "MASKING LOW COMPLEXITY SEQ.: "$( [[ $MASK_STR == "" ] && echo "no" || echo "yes" )
		msg "****************************************************************************"
		msg
	fi
}




function download_lib {
	local progr=$1
	local lib=$2
	local dbname=$3
	local ctrl=$4
	
	# return if control library already successfully downloaded
	if [ -e $ctrl ] && [ $(cat $ctrl) -eq 0 ]; then
		if ! [ -d "${dbname}/library/${lib}" ]; then
			msg "WARNING: ${lib} database not found, but rebuilding is disabled. You may want to run this script with -f option."
		fi
		return 0
	fi

# DEPRECATED >>>>>>>>>>
#	# avoid problems with "which" alias
#	which which &> /dev/null
#	if [ $? -ne 0 ]; then
#		alias which="/usr/bin/which"
#	msg "WARNING: which program not found. It is now being aliased to '/usr/bin/which', but issues likely to rise."
#	fi
# DEPRECATED <<<<<<<<<<	
	
	MASKER="dustmasker"
	# try to symlink dustmasker if not in $PATH variable
	command -v ${MASKER} &> /dev/null
	status=$?
	if [ $status -ne 0 ]; then
		if [ -e "${PROJECT_DIR}/.parameters/prg_${MASKER}" ]; then
			masker_path=$(cat "${PROJECT_DIR}/.parameters/prg_${MASKER}")
			ln -s ${masker_path} /usr/local/bin/${MASKER}
			source ~/.bashrc
		fi
	fi
	
	
	if [[ ${lib} != "plasmid" ]]; then
		cmds=("$progr $lib --db $dbname ${MASK_STR}" \
		      "$progr $lib --db $dbname --use-ftp ${MASK_STR}")
	else
		cmds=("$progr $lib --db $dbname --use-ftp ${MASK_STR}")
	fi
	
	      
	for cmd in "${cmds[@]}" ;
	do
		msg
		rm -rf $dbname/library/$lib
		exec_cmd "${cmd}"
		status=$?
		# terminate if successfully downloaded
		if [ $status -eq 0 ] ; then
			echo $status > $ctrl
			msg "SUCCESSFULLY downloaded library $lib"
			msg
			return $staus
		fi
	done
	
	msg "$progr FAILED to download library $lib" >&2
	msg
	echo $status > $ctrl
	return $status
}





# ##############################  MAIN PROGRAM  ############################## #


# INPUT
arg_parser ${INPUT_ARGS[@]}


# get programs to be executed
progs=($( cat "${PROJECT_DIR}/.parameters/prg_krakenTools" ; ))
kraken2=${progs[0]}
kraken2_build=${progs[1]}
bracken=${progs[2]}
bracken_build=${progs[3]}
#TODO: manage kraken2 if not in path (bracken option -x, problematic though)

downlib="$kraken2_build --download-library"
buildstd="$kraken2_build --standard"

# directory where exit codes are stored in order to avoid running
# again program already successfully executed.
ctrl_dir="$DB_DIR/.controls"
mkdir -p $ctrl_dir




# change directory to $PROJECT_DIR
cd $PROJECT_DIR
gstatus=0



# skip db download if already successfully created
ctrl="${ctrl_dir}/.ready"
if ! [ -e "${ctrl}" ] || [ $(cat "$ctrl") -ne 0 ]; then
	

	# DOWNLOAD TAXONOMY
	msg "Downloading taxonomy"
	ctrl="$ctrl_dir/.taxonomy"
	if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
		exec_cmd "$kraken2_build --download-taxonomy --db $DB_DIR"
		status=$?
		echo $status > $ctrl
		let gstatus+=$status
	else
		# CHECK DB EXISTENCE
		if ! [ -d "${DB_DIR}/taxonomy" ]; then
			msg "WARNING: taxonomy directory not found, but taxonomy download is disabled. You may want to run this script with -f option, or delete file $ctrl"
		fi
	fi


	# DOWNLOAD REQUIRED GENOMES
	msg
	msg "***************  Downloading reference sequences library  ***************"
	
	for library in ${REF_LIST[@]}
	do
		ctrl="${ctrl_dir}/.${library}"
		download_lib "${downlib}" ${library} ${DB_DIR} ${ctrl}
		let gstatus+=$?
	done
	
	
		

	# BUILD DATABASE INDICES
	msg
	msg "****************  Building KRAKEN 2 reference database  ****************"
	ctrl="$ctrl_dir/.kraken2_build"
	if [ $gstatus -eq 0 ] ; then
		status=0
		if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
			exec_cmd "$kraken2_build --build --db $DB_DIR"
			status=$?
			echo $status > $ctrl
		fi
	else
		echo 1 > $ctrl
		msg "Fatal error."
		echo 1 > $ctrl
		exit 1
	fi
	
	# Notify KRAKEN Database creation has been successfully created 
	echo $status > "${ctrl_dir}/.ready"
	
	details="${ctrl_dir}/details.txt"
	echo "Date of creation: ${TIMESTAMP}" > ${details}
	echo "Masking low complexity sequences: "$( [[ $MASK_STR == "" ] && echo "no" || echo "yes" ) >> ${details}
	
fi


	
# BUILD BRACKEN INDICES
msg
msg "Building BRACKEN reference database index for read length ${READ_LEN}"
ctrl="$ctrl_dir/.bracken_build${READ_LEN}"
if ! [ -e "$ctrl" ] || [ $(cat "$ctrl") -ne 0 ] ; then
	exec_cmd "$bracken_build -d ${DB_DIR} -t ${NCPU} -l ${READ_LEN}"
	status=$?
	echo $status > $ctrl
fi

msg
msg "BRACKEN database building completed."
msg

exit $status

