#!/bin/bash

# UNCOMPLETED SCRIPT. DO NOT USE IT!
echo "Error: this script is not meant to be executed since it is not complete."
exit 1


dbname="/nfsd/bcb/bcbg/GallinaG/BRACKEN/ntdb/"
ncpus=$1
krak="/nfsd/bcb/bcbg/GallinaG/BRACKEN/kraken2-2.1.2/kraken2"
krakb="${krak}-build"
brakd="/nfsd/bcb/bcbg/GallinaG/BRACKEN/Bracken-2.8"
brak1="${brakd}/src/kmer2read_distr"

mkdir -p $dbname

echo $krakb --download-taxonomy --db $dbname
&2> echo $krakb --download-taxonomy --db $dbname
$krakb --download-taxonomy --db $dbname

echo $krakb --download-library nt --db $dbname
&2> echo $krakb --download-library nt --db $dbname
$krakb --download-library nt --db $dbname

echo $krakb --build --threads $ncpus --db $dbname
&2> echo $krakb --build --threads $ncpus --db $dbname
$krakb --build --threads $ncpus --db $dbname


cd $dbname

echo $krak --db=${dbname} --threads=$ncpus <( find -L library \(-name "*.fna" -o -name "*.fa" -o -name "*.fasta" \) -exec cat {} + )  > database.kraken
>&2 echo $krak --db=${dbname} --threads=$ncpus <( find -L library \(-name "*.fna" -o -name "*.fa" -o -name "*.fasta" \) -exec cat {} + )  > database.kraken
$krak --db=${dbname} --threads=$ncpus <( find -L library \(-name "*.fna" -o -name "*.fa" -o -name "*.fasta" \) -exec cat {} + )  > database.kraken


$brak1 --seqid2taxid ${dbname}/seqid2taxid.map --taxonomy ${dbname}/taxonomy --kraken database.kraken --output database${READ_LEN}mers.kraken -t ${ncpus}
        
#         -k ${KMER_LEN} -l ${READ_LEN}
#        `${dbname}`  = location of the built Kraken 1.0 or Kraken 2.0 database
#        `${THREADS}`    = number of threads to use [recommended: 20]
#        `${KMER_LEN}`   = length of kmer used to build the Kraken database 
#                                Kraken 1.0 default kmer length = 31
#                                Kraken 2.0 default kmer length = 35
#                                [default: 35]
#        `${READ_LEN}`   = the read length of your data 
#                                e.g., if you are using 100 bp reads, set it to `100`. 
                                
                            
                            
python generate_kmer_distribution.py -i database${READ_LEN}mers.kraken -o database${READ_LEN}mers.kmer_distrib
