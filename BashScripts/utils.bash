#!/bin/bash

###############################################################################
# Useful functions used across many bash scripts are gathered here.           #
# This script should be sourced at the very beginning of another script in    #
# this project.                                                               #
# Please do not move bash scripts or, if you do so, move them all together.   #
#                                                                             #
# Author: Giorgio Gallina                                                     #
# Update: February 2023                                                       #
###############################################################################

#get project directories
#utils_link=$( readlink -f -- "$0"; )
#BASH_BASEDIR=$( dirname -- "$utils_link"; )
#PROJECT_DIR=$( dirname -- "$BASH_BASEDIR"; )
#RSCRIPTS_BASEDIR="${PROJECT_DIR}/Rscripts"
#RESULTS_BASEDIR="${PROJECT_DIR}/Results"
#WORK_BASEDIR="${PROJECT_DIR}/.workdir"
#PARAM_BASEDIR="${PROJECT_DIR}/.parameters"
#DATASETS_BASEDIR="${PROJECT_DIR}/Datasets"
#DATABASES_BASEDIR="${PROJECT_DIR}/Databases"




setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}



# Print message in error stream
msg() {
  echo >&2 -e "${@-}"
}


# return 0 if input arguments require help or version (used for avoiding
# startup computations)
is_help_version() {
	HV_OPTS=($(getopt -q -o hv --long help,version -- "$@")) &>/dev/null
	echo ${HV_OPTS[@]}
	if [[ ${HV_OPTS[0]} == "--" ]]; then
		# FALSE (non help nor version)
		return 1
	else
		# TRUE (help or version)
		return 0
	fi
}



# split <string_to_split> <delimiter>
function split {
	old=$IFS
	IFS=$2
	read -ra splitted <<< $1
	IFS=$old
	echo ${splitted[@]}
}


# remove directory path and extensions from input file paths
function get_filename {
	while [ $# -gt 0 ];
	do
		fullname=$( basename -- $1 )
		trimmed_name=${fullname%%.*}
		echo ${trimmed_name}
		shift
	done
}



# this function takes an associative array by reference as 1st argument, and
# a list of key values.
# make_associative <out_assoc_array> <key1> <value1> [<key2> <value2> ...]
function make_associative {
	# associative array passed by reference
	declare -n out_assoc_array="${1}"
	shift
	i=0
	while [ ${#} -gt 0 ];
	do
		out_assoc_array+=([${1}]=${2})
		shift 2
	done
}

# print input arguments in alphanumeric order
function sort_array {
	printf '%s\n' "${@}"|sort
}


function round {
	# manage precision
	precision=0
	ARGS=$( getopt -o p: --long precision: -- $@ )
	eval set -- ${ARGS}
	if [[ "$1" == *-p* ]]; then
		precision=$2
		shift 2
	fi
	shift
	
	let prec_augm=${precision}+1
	
	# manage rounding
	printf -v rounder_div "1%0${prec_augm}d" 0
	rounder=$( echo "scale=${prec_augm};5 / ${rounder_div}" | bc )
	
	# divisions execution
	while [ $# -gt 0 ]
	do
		# echo round result
		echo "scale=${precision};(${1} + ${rounder}) / 1" | bc
		shift
	done
}



function min {
	m=$1
	shift
	while [ $# -gt 0 ]
	do
		if [ $1 -lt $m ]; then
			m=$1
		fi
		shift
	done
	echo $m
}



function max {
	M=$1
	shift
	while [ $# -gt 0 ]
	do
		if [ $1 -gt $M ]; then
			M=$1
		fi
		shift
	done
	echo $M
}


# get average
# Usage: avg [-p <precision>] [<value1> [<value2> ...]]
function avg {
	# manage precision
	precision=0
	ARGS=$( getopt -o p: --long precision: -- $@ )
	eval set -- ${ARGS}
	if [[ "$1" == *-p* ]]; then
		precision=$2
		shift 2
	fi
	shift
	
	# sum values
	count=$#
	sum=0
	let big_prec=${precision}+5
	while [ $# -gt 0 ]
	do
#		let sum+=$1
		sum=$( echo "scale=${big_prec};${sum} + $1" | bc )
		shift
	done
	
	# divide correctly
	division -p ${precision} -d ${count} ${sum}
}


# Get decimal value of the division
# Usage: division [-p <PRECISION>] -d <DENOMINATOR> <NUMERATOR1> [<NUMERATOR2> ...]
# Example: division -p 4 -d 3 5 12 22
#   result: (1.6666 4.0000 7.3333)
function division {
	#defaults
	precision=4
	
	# input management
	ARGS=$( getopt -o p:d: --long precision:,denominator: -- $@ )
	eval set -- ${ARGS}
	while [ : ]
	do
		case $1 in
			-p | --precision)
				precision=$2
				shift 2
				;;
			-d | --denominator)
				denom=$2
				shift 2
				;;
			--)
				shift
				break
				;;
			*)
				msg "Wrong option $1"
				return 1
				;;
		esac
	done
	
	let prec_augm=${precision}+1
	
	# manage rounding
	printf -v rounder_div "1%0${prec_augm}d" 0
	rounder=$( echo "scale=$(($precision+1));5 / ${rounder_div}" | bc )
	
	# divisions execution
	while [ $# -gt 0 ]
	do
		# division result cut at precision + 1
		value_raw=$(echo "scale=${prec_augm};${1} / ${denom}" | bc)
		# echo result with correct rounding and precision
		echo "scale=${precision};(${value_raw} + ${rounder}) / 1" | bc
		shift
	done
}


# given the available RAM memory in GB, get the max RAM memory in MB that simka can use 
function simka_giga2mega {
	# assure simka does not exceed maximum memory
	echo $(($1 * 900))
}


function exec_cmd {
	cmd="$*"
	msg "${cmd}"
	eval ${cmd}
}

# read two columns from a table and store its content into an associative array passed by reference.
# Usage: read_label_path -o <ARRAY_REF> -i <LABEL_PATH_FNAME> -s <SEPARATOR> --has-header -l <LABEL_COL> -v <VALUE_COL>
function table_columns_to_array {
	# TODO
	
	# input management
	ARGS=$( getopt -o i:l:o:s:v: --long has-headerinput:,label:,output:,separator:,value: -- $@ )
	eval set -- ${ARGS}
	has_header=1
	label_col='${1}'
	value_col='${2}'
	while [ : ]
	do
		case $1 in
			--has-header)
				has_header=0
				shift
				;;
			-i | --input)
				input=$2
				shift 2
				;;
			-l | --labels)
				label_col=$2
				shift 2
				;;
			-o | --output)
				declare -n out_array=${2}
				shift 2
				;;
			-s | --sep | --separator)
				sep=$2
				shift 2
				;;
			-v | --values)
				value_col=$2
				shift 2
				;;
			--)
				shift
				break
				;;
			*)
				msg "Error: Unknown option $1 passed to table_columns_to_array"
				return 1
				;;
		esac
	done
	
	cstr="-c ${label_col},${value_col}"
	testint=${label_col}${value_col}
	if [ ${testint} -eq ${testint} ] &>/dev/null ; then
		cstr="-c ,${label_col},${value_col}"
	fi
	raw_array=($( ${RSCRIPTS_BASEDIR}/get_table_values.R -s ${sep} ${cstr} --rowMajor ${input} ))
	
	make_associative out_array ${raw_array[@]}
}
