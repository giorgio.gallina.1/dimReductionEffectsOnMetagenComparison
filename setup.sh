#!/bin/sh

echo "=========  Updating and installing necessary software  ========="
sudo apt update

sudo apt -y install which wget rsync git curl sed gzip tar bc
sudo apt -y install build-essential r-base gcc perl
sudo apt -y install make cmake libboost-all-dev autoconf automake libtool gettext pkg-config
sudo apt -y install r-cran-ecodist r-cran-vegan r-bioc-shortread r-cran-biocmanager r-cran-devtools r-base-core r-cran-littler
sudo apt -y install r-cran-rcolorbrewer r-cran-pheatmap r-cran-tidyverse r-cran-argparse
sudo apt -y install default-jre
sudo apt -y install bowtie2
sudo apt -y install sra-toolkit
sudo apt -y install ncbi-blast+
sudo apt -y install libomp-dev

echo
echo "================  Installing Simka and SimkaMin  ================="
sudo apt -y install simka simkamin

echo
echo "================  Initializing conda environment  ================"
if ! [ -x "$(command -v conda)" ] && ! [ -x "$(command -v anaconda)" ] && ! [ -x "$(command -v miniconda)" ] ; then
	echo "WARNING: conda not found. If you want to automatically install it, please uncomment the 2 lines below from file setup.sh" >&2
#	wget -O Miniconda3-latest-Linux-x86_64.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
#	bash Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
fi

conda update -y -n base -c defaults conda
conda config  --add channels defaults
conda config  --add channels bioconda
conda config  --add channels conda-forge
conda  install -y numpy

conda create -y -c bioconda -n bracken blast kraken2 bracken krakentools


echo
echo "===============  Downloading and installing SPRISS  ==============="

# reload environment
source ~/.bashrc

script_link=$( readlink -f -- "$0"; )	# $PROJECT_DIR/setup.sh
PROJECT_DIR=$( dirname -- "$script_link"; )	# $PROJECT_DIR
SPRISS_DIR="${PROJECT_DIR}/SPRISS"

# if there already is a SPRISS directory, rename it
if [ -d ${SPRISS_DIR} ] ; then
	bkp="${SPRISS_DIR}_bkp$(date +%y%m%d_%H%M%S)"
	echo "WARNING: ${SPRISS_DIR} already exists. Moving it to ${bkp}"
	mv ${SPRISS_DIR} "${bkp}"
fi

# clone SPRISS project from git
git clone https://github.com/VandinLab/SPRISS.git

# Clean-up SPRISS project
cd ${SPRISS_DIR}
rm -r ${SPRISS_DIR}/SAKEIMA
mv ${SPRISS_DIR}/SPRISS/* ${SPRISS_DIR}/
rm -r ${SPRISS_DIR}/SPRISS

# Add ah-hoc scripts for the analysis
cp ${PROJECT_DIR}/src/spriss_uf.py ${SPRISS_DIR}/scripts/
mv ${SPRISS_DIR}/scripts/create_sample.cpp ${SPRISS_DIR}/scripts/create_sample.bkp.cpp
cp ${PROJECT_DIR}/src/create_sample.cpp ${SPRISS_DIR}/scripts/

# Install SPRISS
make

if [ $? -eq 0 ]; then
	echo "SPRISS installation completed."
#	chmod +x ${SPRISS_DIR}/scripts/*
else
	echo "SPRISS intallation failed. Please try installing it by yourself."
fi


# Compile Dissimilarity Calculator
mkdir -p ${PROJECT_DIR}/bin
g++ -fopenmp -o ${PROJECT_DIR}/bin/get_dissimilarity_matrix ${PROJECT_DIR}/src/Cdissimilarities.cpp
g++ -fopenmp -o ${PROJECT_DIR}/bin/betaDiversity_spriss ${PROJECT_DIR}/src/betaDiversity_spriss.cpp
g++ -fopenmp -o ${PROJECT_DIR}/bin/betaDiversity_cami ${PROJECT_DIR}/src/betaDiversity_camitrue.cpp



# ###############  Give exec. permission to scripts  ############### #
echo
echo "Giving execution permission to project scripts"
BASH_DIR="${PROJECT_DIR}/BashScripts"
RSCRIPT_DIR="${PROJECT_DIR}/Rscripts"
chmod +x ${BASH_DIR}/*
chmod +x ${RSCRIPT_DIR}/*
chmod +x ${PROJECT_DIR}/Datasets/Datasets_preparation/*.bash


# =======================  Write parameters  ======================= #

echo
echo "Ending up."
param_dir="$PROJECT_DIR/.parameters"
mkdir -p $param_dir

echo "${SPRISS_DIR}/scripts/spriss_uf.py" > "${param_dir}/prg_spriss"

echo "kraken2" > "${param_dir}/prg_krakenTools"
echo "kraken2-build" >> "${param_dir}/prg_krakenTools"
echo "bracken" >> "${param_dir}/prg_krakenTools"
echo "bracken-build" >> "${param_dir}/prg_krakenTools"
echo "beta_diversity.py" >> "${param_dir}/prg_krakenTools"

echo "simka" > "${param_dir}/prg_simka"
echo "simkaMin" >> "${param_dir}/prg_simka"

# avoid problems with "which" command
which which &> /dev/null
if [ $? -ne 0 ]; then
	alias which="/usr/bin/which"
	echo "WARNING: which program not found. It is now being aliased to '/usr/bin/which', but issues likely to rise." >&2
fi

# Find dustmasker
MASKER="dustmasker"

command -v ${MASKER} &> /dev/null
status=$?
if [ $status -ne 0 ]; then
	conda activate bracken
	command -v ${MASKER} &> /dev/null
	status=$?
	if [ $status -ne 0 ]; then
		echo "WARNING: dustmasker installation failed." >&2
	else
		echo $(command -v "dustmasker") > "${param_dir}/prg_dustmasker"
	fi
	conda deactivate
else
	echo $(command -v "dustmasker") > "${param_dir}/prg_dustmasker"
fi


echo "System setup completed."
echo
