################################################################################
# This script is used for calculating SimkaMin subsample sizes compatible with
# SPRISS subsamples
#
# USAGE
# Rscript sampling_rates.R <k> <theta> <delta> <epsilon> <input_filename> <output_filename>
#    <k>: k-mer length
#    <theta>: k-mer frequency cutoff (see SPRISS theta parameter)
#    <delta>: SPRISS confidence parameter in (0,1)
#    <epsilon>: SPRISS accuracy parameter in (0,1), use -1 for default value
#    <input_filename>: path/to/datasetsKmersStatistics
#    <output_filename>: path/to/output/results
#
# ============================================================================ #
# Author: Giorgio Gallina
# Year: 2022
################################################################################


# get input args
args <- commandArgs(trailingOnly = T)

k <- as.integer(args[1])
theta <- as.numeric(args[2])
delta <- as.numeric(args[3])
epsilon <- as.numeric(args[4])
input_fname <- args[5]
output_fname <- args[6]
# labels_fname <- args[6]

# read data-sets summaries to compute sampling size for simkamin
message(paste0("Reading file ", input_fname, "..."))
datasets.smry <- read.table(input_fname, header=T, sep=";")#, row.names="label")
# message(paste0("...read! - ", nrow(datasets.smry), " rows found."))
datasets.smry <- datasets.smry[(datasets.smry$k == k),]
row.names(datasets.smry) <- datasets.smry$label

# read labels sets
# labels <- read.table(file=labels_fname, header=F)
# lbl_sets <- strsplit(labels[,1], "_")

message("Compunting subsample size for SimkaMin to be compatible with SPRISS subsample size...")

# COMPUTING SPRISS SAMPLE SIZES PER EACH DATASET

# default value of epsilon if required
if( (epsilon <= 0) | (epsilon > 1) )
{
  epsilon <- theta - (2 / datasets.smry$nkmers)
}
names(epsilon) <- row.names(datasets.smry)
# compute size l of SPRISS bags
l <- floor(0.9 / (theta * datasets.smry$kmersPerRead.avg))
names(l) <- row.names(datasets.smry)

# compute number m of bags
tmp <- 2 * l * datasets.smry$kmersPerRead.max
sigmak <- datasets.smry$alphabet ^ k
ind <- tmp > sigmak
tmp[ind] <- sigmak[ind]
m <- ( 2 / ( (epsilon * l * datasets.smry$kmersPerRead.avg)**2 ) )  *  ( floor(log2(tmp)) + log(2/delta) )
m <- ceiling(m)

# compute sampling rates
smprate.reads <- m*l 

# COMPUTING EQUIVALENT SAMPLE SIZES FOR SIMKAMIN
# raw subsamples' sizes
smprate.kmers <- smprate.reads * datasets.smry$kmersPerRead.avg
# get index of subsamples' sizes greater than original datasets' sizes
index <- smprate.kmers < datasets.smry$nkmers
# exclude subsamples greater than original
smprate.kmers.strict <- smprate.kmers[index]

# notify user if some dataset has been ignored due to subsample size constraints
if(sum(!index) > 0)
{
  message("There are ", sum(!index), " datasets too small, hence excluded.")
  message(" These are: {", paste(datasets.smry[!index, "label"], collapse=", "), "}")
}

# WRITE OUTPUT
# print(row.names(datasets.smry), quote=F)
print(smprate.kmers.strict)
write.table(smprate.kmers.strict, file=output_fname, quote=F, col.names=F)
message("Done.")



# ADJUST SIMKA_SMP INPUT FILE





# # GETTING EQUIVALENT SIMKAMIN SAMPLE SIZE PER GRUOP OF LABELS
# for (lblset in lbl_sets)
# {
#   
# }
# 
# 
# 
# 
# 
# # sampling rate desired for simkamin
# nb_kmers <- round(mean(smprate.kmers))
# print(nb_kmers)
# 
# message("...done.")
# message(paste0("nb_kmers = ", nb_kmers))
# 
# # percentage of kmers selected per dataset
# message("Percentage of kmers selected from each dataset:")
# rate <- 100 * nb_kmers / datasets.smry$nkmers
# names(rate) <- labels
# for (clbl in labels)
# {
#   message(paste0("  ", clbl, ": ", round(rate[clbl], digits=2), "%"))
# }
