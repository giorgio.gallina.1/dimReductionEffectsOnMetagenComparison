#!/usr/bin/env Rscript

################################################################################
# ~~~~~~~~~~                                                        ~~~~~~~~~~ #
# This script prints the overall datasets size in a collection of datasets.    #
# Such overall size shall be used as a reference size.                         #
#                                                                              #
# USAGE:                                                                       #
# ./get_ds_reference_size.R <gstat_fname> <function> <list of k>               #
#                                                                              #
# INPU ARGUMENTS:                                                              #
#    <gstat_fname>:  Path/to/file of datasets global 8read) statistics.        #
#    <function>:     An R function to compute overall size given all sizes     #
#                    (eg "min", "mean"...)                                     #
#    <list of k>:    Space-separated list of k-mer lengths k.                  #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: February 2023                                                        #
# ############################################################################ #



library("tidyverse")

# GET RSCRIPT LOCATION
get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)
utils <- paste0(rdir, "/utils.R")
# LOAD utils.R
source(utils)



args <- commandArgs(T)
gstat.fname <- args[1]
func <- args[2]
klist <- as.integer(args[-c(1,2)])


# read statistics table
gstat <- read.table(file=gstat.fname, header=T, sep=";", quote="")

# get number of kmers for each dataset
nkmers <- ds.statistics.kmer.tally(gstat, klist)

# apply the desired function to select the overall datasets size
nkmers.glob <- apply(nkmers, 2, func) %>%
               round()

# print overall sizes
cat(nkmers.glob)

