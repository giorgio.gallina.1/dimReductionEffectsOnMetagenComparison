#!/usr/bin/Rscript

################################################################################
# Simkamin comparison plots with bracken distances.                            #
#                                                                              #
# USAGE:                                                                       #
# ./plot_bivar_simka_bracken.R <SIMKA_DIR> <BRACKEN_DIR> <GLOB_STATS> \        #
#    <OUTPUT_DIR>                                                              #
#                                                                              #
# PARAMETERS:                                                                  #
#    <SIMKA_DIR>:     Directory where SimkaMin collective results are stored.  #
#                     Should be $project_dir/Results/Simkamin                  #
#    <BRACKEN_DIR>:   Directory where Bracken results are stored.              #
#                     Should be $project_dir/Results/Bracken                   #
#    <GLOB_STATS>:    Path/to/filename of global (read) datasets statistics.   # 
#    <OUTPUT_DIR>:    Directory where results of this script.                  #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: March 2023                                                           #
################################################################################


library("tidyverse")
library(RColorBrewer, quietly = TRUE)

# GET RSCRIPT LOCATION
get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)

utils <- paste0(rdir, "/utils.R")
# LOAD utils.R
source(utils)



# 
# plot2pdf.bivar <- function(fname, x, y, main, xlab, ylab, legend=NULL, pointsize=12, ...)
# {
#   pdf(fname, width=8, height=6)
#   
#   plotting.scatter.bivar(x, y, main, xlab, ylab, legend, ...)
#   
#   dev.off()
# }



# ****************************  PARAMETERS SETUP  **************************** #

# input arguments
args <- commandArgs(T)

SIMKA_DIR  <- sub(pattern = "/$",
                  replacement = "",
                  x = args[1])
DIV_DIR <- dirname(SIMKA_DIR)
BRACKEN_DIR <- sub(pattern = "/$",
                   replacement = "",
                   x = args[2])
GLOB_STATS  <- args[3]
if (length(args) > 3)
{
  OUTPUT_DIR  <- args[4]
}else
{
  OUTPUT_DIR <- paste0(
    DIV_DIR, "/",
    basename(SIMKA_DIR),
    "__vs__",
    basename(BRACKEN_DIR)
  )
}

dir.create(OUTPUT_DIR, recursive = TRUE)


# constants
PLOT_BASENAME <- "plot_bivar_simka"
PLOT_EXTENS   <- ".pdf"
COR_FNAME     <- "cor_simkamin_bracken.csv"
SMP_DETL_BN   <- paste0(OUTPUT_DIR, "/sampling_details_k")
SMP_DETL_EXTN <- ".csv"
SIMKAMIN_BC   <- "mat_abundance_braycurtis.csv"
SIMKAMIN_JAC  <- "mat_presenceAbsence_jaccard.csv"
SPEARM_FNAME  <- paste0(OUTPUT_DIR, "/cor_spearman.csv")
PEARSON_FNAME <- paste0(OUTPUT_DIR, "/cor_pearson.csv")
# INFO_FNAME   <- paste0(SIMKA_DIR, "/info.txt")

# taxon level as the last char of BRACKEN directory
TAXON_LVL <- sub('.*(?=.$)', '', BRACKEN_DIR, perl=T)
BRACKEN_BC   <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_BC.csv")
BRACKEN_JAC  <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_JAC.csv")


PLOT_MAIN_BASE_BC <- "BC Dissimilarities Comparison"
PLOT_XLAB_BC <- "BRACKEN BC dissimilarity"
PLOT_YLAB_BC <- "Simka BC dissimilarity"
PLOT_MAIN_BASE_JAC <- "Jaccard Distances Comparison"
PLOT_XLAB_JAC <- "BRACKEN Jaccard distance"
PLOT_YLAB_JAC <- "Simka Jaccard distance"


# ****************************  READ INPUT FILES  **************************** #

# get list of simkamin result folders ( "skmin_k${k}_smp${smp}" )
simkalist <- list.dirs(path = SIMKA_DIR, full.names = F, recursive = F)
# get simkamin BC and JACCARD matrices file-names
simkalist_bc  <- list.files(path = SIMKA_DIR,
                           pattern=paste0(SIMKAMIN_BC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )
simkalist_jac <- list.files(path = SIMKA_DIR,
                           pattern=paste0(SIMKAMIN_JAC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )

# read Bracken dissimilarity matrices
mat_bracken_bc  <- read.dissimat(BRACKEN_BC)
mat_bracken_jac <- read.dissimat(BRACKEN_JAC)

# read datasets global statistics
read_stats <- read.table(file = GLOB_STATS,
                         header = TRUE, row.names = "label",
                         sep = ";", quote = "")

# # read info about sampling rates
# info <- read.table(file = INFO_FNAME,
#                    header = TRUE,
#                    sep = ",", quote ="")
# # select only useful information
# info_collection <- info[, c("k", "smp_rate", "nb_kmers", "ref_kmers")] %>%
#   unique()
# SMP_NORM <- info[1, "smp_norm"]


# *************************  GET FURTHER PARAMETERS  ************************* #

# read list of k-mer length values k
k_list <- sub(pattern = ".*_k", replacement = "", x = simkalist) %>%
          sub(pattern = "_.*", replacement = "") %>%
          as.integer() %>%
          unique()

# read list of sampling rates
smp_list <- sub(pattern = ".*_smp", replacement = "", x = simkalist) %>%
  gsub(pattern = "[^[:digit:]]", replacement = "") %>%
  as.integer() %>% unique() %>% sort()
# smp_list <- smp_list[-c(1,2)]
N_SMP <- length(smp_list)
smp_list <- c(smp_list[-1], 0)
SMP_RATES <- sprintf(fmt = "n.kmers: %.4g", smp_list)
names(SMP_RATES) <- smp_list
LEGEND_TOT <- c(SMP_RATES[-N_SMP], simka="Simka exact")


# datasets ordered labels
labels <- colnames(mat_bracken_bc)  %>%  sort()
N_labs <- length(labels)

# convert dissimilarity matrices into distance vectors
dist_bracken_bc  <- mat2dist(mat_bracken_bc,  labels)
dist_bracken_jac <- mat2dist(mat_bracken_jac, labels)



# ****************************  SAMPLING DETAILS  **************************** #

# 
# # DS x K : datasets sizes in #kmers
# ds_sizes <- ds.statistics.kmer.tally(read_stats, k_list)
# 
# # SMP x K : smaple sizes in #kmers
# smp_kmers <- ds.simkamin.sampling.size.fast(ds_sizes, smp_list)
# 
# for (k in k_list)
# {
#   kc <- as.character(k)
#   fname <- paste0(SMP_DETL_BN, k, SMP_DETL_EXTN)
#   
#   curr_sizes <- ds_sizes[labels, kc]
#   sample_details <- data.frame(label = labels,
#                                ds_size = curr_sizes,
#                                row.names = labels)
#   for (smp in smp_list)
#   {
#     smpc <- as.character(smp)
#     index_name = paste0("smp", smpc)
#     sample_details[labels, index_name] <- smp_kmers[smpc, kc] * 100 / curr_sizes
#   }
#   
#   write.csv(x = sample_details,
#             file = fname,
#             append = FALSE,
#             quote = FALSE,
#             row.names = FALSE,
#             col.names = TRUE)
# }



# *****************************  PREPARE OUTPUT  ***************************** #
# # correlation matrices
# cor_spear <- data.frame()
# cor_pears <- data.frame()
# cor_rowbc <- 1


# iterate over k-mer length
for (k in k_list)
{
  # # ************************  CORRELATION MAT. SETUP  ************************ #
  # cor_rowjac <- cor_rowbc + 1
  # cor_pears[cor_rowbc, "dist_fun"] = cor_spear[cor_rowbc, "dist_fun"] <- "Bray-Curtis"
  # cor_pears[cor_rowjac, "dist_fun"] = cor_spear[cor_rowjac, "dist_fun"] <- "Jaccard"
  # cor_pears[cor_rowbc, "k"] = cor_pears[cor_rowjac, "k"] <- k
  # cor_spear[cor_rowbc, "k"] = cor_spear[cor_rowjac, "k"] <- k
  
  
  # ************************  READ SIMKAMIN MATRICES  ************************ #
  
  mat_skmBC_collection  <- list()
  mat_skmJAC_collection <- list()
  dists_collection_bc <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                ncol = length(smp_list),
                                dimnames = list(label = NULL,
                                                smp = sprintf("%d", (smp_list))))
  dists_collection_jac <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                 ncol = length(smp_list),
                                 dimnames = list(label = NULL,
                                                 smp = sprintf("%d", (smp_list))))
  for (smp in smp_list)
  {
    smpc <- sprintf("%d", smp)
    
    pattern   <- paste0("_k", k, "_smp", smpc, "/")
    fname_bc  <- grep(pattern = pattern, x = simkalist_bc,  value = TRUE)
    fname_jac <- grep(pattern = pattern, x = simkalist_jac, value = TRUE)
    
    dists_collection_bc[,smpc] <- 
      (mat_skmBC_collection[[smpc]]  <- read.dissimat(fname_bc)) %>%
      mat2dist(labels=labels)
    
    dists_collection_jac[,smpc] <- (
      mat_skmJAC_collection[[smpc]] <- read.dissimat(fname_jac)) %>%
      mat2dist(labels=labels)
      
  }
  
  
  # **********************  PLOT GROUP BETA-DIVERSITY  *********************** #
  
  # group plot
  plot_grtitle_bc  <- paste0(PLOT_MAIN_BASE_BC, " - k = ", k)
  plot_grtitle_jac <- paste0(PLOT_MAIN_BASE_JAC, " - k = ", k)
  fname_bc  <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_BC", PLOT_EXTENS)
  fname_jac <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_JAC", PLOT_EXTENS)
  
  # select sampling rates for the group
  N_MIDDLE_GRP <- 5
  m_avail <- length(smp_list) - 2
  n_middle_grp <- min(m_avail, N_MIDDLE_GRP)
  m_step  <- (m_avail / n_middle_grp) %>% as.integer()
  index1  <- 2 + round( m_step / 2 )
  group_sel <- seq(index1, length(smp_list) - 1, by = m_step)[1:n_middle_grp]
  group_sel <- c(1, group_sel, length(smp_list))
  group_sel_names <- sprintf("%d", smp_list[group_sel])
  
  legend <- LEGEND_TOT[group_sel]
  
  group_dists_bc <- dists_collection_bc[, group_sel_names]
  group_dists_jac <- dists_collection_jac[, group_sel_names]
  
  leg_pos_ctrl_bc <- sum(group_dists_bc - dist_bracken_bc, na.rm = TRUE) < 0
  leg_pos_ctrl_jac <- sum(group_dists_jac - dist_bracken_jac, na.rm = TRUE) < 0
  leg_pos_bc <- "bottomright"
  if(leg_pos_ctrl_bc)
  {
    leg_pos_bc <- "topleft"
  }
  leg_pos_jac <- "bottomright"
  if(leg_pos_ctrl_jac)
  {
    leg_pos_jac <- "topleft"
  }
  
  # save scatter plots on device
  plot2pdf.bivar(fname = fname_bc,
           x = dist_bracken_bc,
           y = group_dists_bc,
           main = plot_grtitle_bc,
           xlab = PLOT_XLAB_BC,
           ylab = PLOT_YLAB_BC,
           legend = legend,
           legend_pos = leg_pos_bc)
  plot2pdf.bivar(fname = fname_jac,
           x = dist_bracken_jac,
           y = group_dists_jac,
           main = plot_grtitle_jac,
           xlab = PLOT_XLAB_JAC,
           ylab = PLOT_YLAB_JAC,
           legend = legend,
           legend_pos = leg_pos_jac)
  
  
  
  # for (smp in smp_list)
  # {
  #   smpc <- as.character(smp)
  #   
  #   # ***************************  PLOT SMP B-DIV  *************************** #
  #   
  #   # current simple bray-curtis
  #   plot_title <- paste0(PLOT_MAIN_BASE_BC, " - k=", k, ", smp=", smpc, "%")
  #   fname <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_smp", smpc, "_BC", PLOT_EXTENS)
  #   y_bc <- dists_collection_bc[,smpc]
  #   plot2pdf.bivar(fname = fname,
  #            x = dist_bracken_bc,   y = y_bc,
  #            main = plot_title,
  #            xlab = PLOT_XLAB_BC,
  #            ylab = PLOT_YLAB_BC)
  #   
  #   # current simple jaccard
  #   plot_title <- paste0(PLOT_MAIN_BASE_JAC, " - k=", k, ", smp=", smpc, "%")
  #   fname <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_smp", smpc, "_JAC", PLOT_EXTENS)
  #   y_jac <- dists_collection_jac[,smpc]
  #   plot2pdf.bivar(fname = fname,
  #            x = dist_bracken_jac,   y = y_jac,
  #            main = plot_title,
  #            xlab = PLOT_XLAB_JAC,
  #            ylab = PLOT_YLAB_JAC)
  #   
  #   
  #   # ************************  CORRELATION MATRICES  ************************ #
  #   
  #   smpstr <- paste0("smp", smpc)
  #   cor_pears[cor_rowbc, smpstr] <- cor(dist_bracken_bc, y_bc, method = "pearson")
  #   cor_spear[cor_rowbc, smpstr] <- cor(dist_bracken_bc, y_bc, method = "spearman")
  #   cor_pears[cor_rowjac, smpstr] <- cor(dist_bracken_jac, y_jac, method = "pearson")
  #   cor_spear[cor_rowjac, smpstr] <- cor(dist_bracken_jac, y_jac, method = "spearman")
  #   
  #   
  #   
  #   
  # }
  # cor_rowbc <- cor_rowjac + 1
}



# ***************************  STORE CORRELATIONS  *************************** #
# write.csv(x = cor_pears,
#           file = PEARSON_FNAME,
#           quote = FALSE,
#           row.names = FALSE,
#           col.names = TRUE)
# write.csv(x = cor_spear,
#           file = SPEARM_FNAME,
#           quote = FALSE,
#           row.names = FALSE,
#           col.names = TRUE)
