#!/usr/bin/Rscript

################################################################################
# Simkamin comparison plots and correlation with bracken distances.            #
#                                                                              #
# USAGE:                                                                       #
# ./plot_simkamin_bracken_cmp.R <simkamin_dir> <bracken_dir> <glob_stats> \    #
#    <output_dir>                                                              #
#                                                                              #
# PARAMETERS:                                                                  #
#    <simkamin_dir>:  Directory where SimkaMin collective results are stored.  #
#                     Should be $project_dir/Results/Simkamin                  #
#    <bracken_dir>:   Directory where Bracken results are stored.              #
#                     Should be $project_dir/Results/Bracken                   #
#    <glob_stats>:    Path/to/filename of global (read) datasets statistics.   # 
#    <output_dir>:    Directory where results of this script.                  #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: January 2023                                                         #
################################################################################


library("tidyverse")
# library(RColorBrewer, quietly = TRUE)

# GET RSCRIPT LOCATION
get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)

utils <- paste0(rdir, "/utils.R")
# LOAD utils.R
source(utils)


# ****************************  LOCAL FUNCTIONS  ***************************** #
# # read dissimilarity matrix
# read.dissimat <- function(filename)
# {
#   mat <- read.csv(file = filename,
#                   header = TRUE,
#                   row.names = 1,
#                   sep = ";",
#                   quote = "")
#   return (mat)
# }
# 
# # given a dissimilarity matrix and a list of labels, get the distances as vector
# mat2dist <- function(mat, labels=NULL)
# {
#   dist <- c()
#   
#   if(is.null(labels))
#   {
#     labels <- colnames(mat) %>% sort()
#   }
#   
#   N <- length(labels)
#   for (i in 1:(N-1))
#   {
#     lab1 <- labels[i]
#     for (j in (i+1):N)
#     {
#       lab2 <- labels[j]
#       label <- paste0(lab1, "_", lab2)
#       dist[label] <- mat[lab1, lab2]
#     }
#   }
#   
#   return (dist)
# }

# plot beta-div x against beta-div y
scatter.bivar <- function(x, y, main, xlab, ylab, legend=NULL)
{
  # manage multiple scatters at once (let diff. variables be diff. columns)
  y <- as.matrix(y)
  if (nrow(y) != length(x))
  {
    if (ncol(y) != length(x))
    {
      message("Incompatible dimensions of x and y. No plot drawn.")
      return (NA)
    }
    y <- t(y)
  }
  
  # dimensions
  N_val <- nrow(y) # = length(x)
  M_var <- ncol(y) # number of variables to plot against x
  
  # get dissimilarity ranges
  xrange <- range(x)
  yrange <- apply(y, MARGIN =  2, FUN = range)  %>%  range()
  trange <- c(xrange, yrange)  %>%  range() %>% + c(-0.05, 0.05)
  
  # plot first variable y
  res.plot <- plot(x = x, y = y[,1],
                   main = main,
                   xlab = xlab,   ylab = ylab,
                   xlim = trange, ylim = trange,
                   pch=20, col=1)
  
  # plot other variables y in different colors
  if (M_var > 1)
  {
    for (i in 2:M_var)
    {
      points(x = x, y = y[,i],
             pch=20, col=(i+1))
    }
  }
  
  # reference line (bisector)
  abline(0, 1, col=2)
  
  # print legend if provided and reasonable
  if (!is.null(legend) & M_var > 1)
  {
    legend <- c(legend, "reference")
    legend(x = "bottomright",
           legend = legend,
           col = c(1, 3:(M_var+1), 2),
           pch = c(rep(20, M_var), NA),
           lty = c(rep(NA, M_var), 1),
           cex = 1.2 )
  }
  
  return (res.plot)
  
}

plot2pdf <- function(fname, x, y, main, xlab, ylab, legend=NULL)
{
  pdf(fname, width=(16*6/9), height=6)
  
  scatter.bivar(x, y, main, xlab, ylab, legend)
  
  dev.off()
}



# ****************************  PARAMETERS SETUP  **************************** #

# input arguments
args <- commandArgs(T)
simkamin_dir <- args[1]
bracken_dir  <- args[2]
glob_stats   <- args[3]
output_dir   <- args[4]

# constants
PLOT_BASENAME <- "plot_bivar_scatter"
PLOT_EXTENS   <- ".pdf"
COR_FNAME     <- "cor_simkamin_bracken.csv"
SMP_DETL_BN   <- paste0(output_dir, "/sampling_details_k")
SMP_DETL_EXTN <- ".csv"
SIMKAMIN_BC   <- "mat_abundance_braycurtis.csv"
SIMKAMIN_JAC  <- "mat_presenceAbsence_jaccard.csv"
SPEARM_FNAME  <- paste0(output_dir, "/cor_spearman.csv")
PEARSON_FNAME <- paste0(output_dir, "/cor_pearson.csv")
BRACKEN_BC    <- paste0(bracken_dir, "/", "betadiv_BC.csv")
BRACKEN_JAC   <- paste0(bracken_dir, "/", "betadiv_JAC.csv")


PLOT_MAIN_BASE_BC <- "BC Dissimilarities Comparison"
PLOT_XLAB_BC <- "BRACKEN BC dissimilarity"
PLOT_YLAB_BC <- "SimkaMin BC dissimilarity"
PLOT_MAIN_BASE_JAC <- "Jaccard Distances Comparison"
PLOT_XLAB_JAC <- "BRACKEN Jaccard distance"
PLOT_YLAB_JAC <- "SimkaMin Jaccard distance"


# ****************************  READ INPUT FILES  **************************** #

# get list of simkamin result folders ( "skmin_k${k}_smp${smp}" )
simkalist <- list.dirs(path = simkamin_dir, full.names = F, recursive = F)
# get simkamin BC and JACCARD matrices file-names
simkalist_bc  <- list.files(path = simkamin_dir,
                           pattern=paste0(SIMKAMIN_BC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )
simkalist_jac <- list.files(path = simkamin_dir,
                           pattern=paste0(SIMKAMIN_JAC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )

# read Bracken dissimilarity matrices
mat_bracken_bc  <- read.dissimat(BRACKEN_BC)
mat_bracken_jac <- read.dissimat(BRACKEN_JAC)

# read datasets global statistics
read_stats <- read.table(file = glob_stats,
                         header = TRUE, row.names = "label",
                         sep = ";", quote = "")


# *************************  GET FURTHER PARAMETERS  ************************* #

# read list of k-mer length values k
k_list <- sub(pattern = ".*_k", replacement = "", x = simkalist) %>%
          sub(pattern = "_.*", replacement = "") %>%
          as.integer() %>%
          unique()

# read list of sampling rates
smp_list <- sub(pattern = ".*_smp", replacement = "", x = simkalist) %>%
            gsub(pattern = "[^[:digit:]]", replacement = "") %>%
            as.integer() %>% unique() %>% sort()

# datasets ordered labels
labels <- colnames(mat_bracken_bc)  %>%  sort()
N_labs <- length(labels)

# convert dissimilarity matrices into distance vectors
dist_bracken_bc  <- mat2dist(mat_bracken_bc,  labels)
dist_bracken_jac <- mat2dist(mat_bracken_jac, labels)



# ****************************  SAMPLING DETAILS  **************************** #

dir.create(output_dir, recursive = TRUE)

# DS x K : datasets sizes in #kmers
ds_sizes <- ds.statistics.kmer.tally(read_stats, k_list)

# SMP x K : smaple sizes in #kmers
smp_kmers <- ds.simkamin.sampling.size.fast(ds_sizes, smp_list)

for (k in k_list)
{
  kc <- as.character(k)
  fname <- paste0(SMP_DETL_BN, k, SMP_DETL_EXTN)
  
  curr_sizes <- ds_sizes[labels, kc]
  sample_details <- data.frame(label = labels,
                               ds_size = curr_sizes,
                               row.names = labels)
  for (smp in smp_list)
  {
    smpc <- as.character(smp)
    index_name = paste0("smp", smpc)
    sample_details[labels, index_name] <- smp_kmers[smpc, kc] * 100 / curr_sizes
  }
  
  write.csv(x = sample_details,
            file = fname,
            append = FALSE,
            quote = FALSE,
            row.names = FALSE,
            col.names = TRUE)
}



# *****************************  PREPARE OUTPUT  ***************************** #
# correlation matrices
cor_spear <- data.frame()
cor_pears <- data.frame()
cor_rowbc <- 1


# iterate over k-mer length
for (k in k_list)
{
  
  
  # ************************  CORRELATION MAT. SETUP  ************************ #
  cor_rowjac <- cor_rowbc + 1
  cor_pears[cor_rowbc, "dist_fun"] = cor_spear[cor_rowbc, "dist_fun"] <- "Bray-Curtis"
  cor_pears[cor_rowjac, "dist_fun"] = cor_spear[cor_rowjac, "dist_fun"] <- "Jaccard"
  cor_pears[cor_rowbc, "k"] = cor_pears[cor_rowjac, "k"] <- k
  cor_spear[cor_rowbc, "k"] = cor_spear[cor_rowjac, "k"] <- k
  # ************************  READ SIMKAMIN MATRICES  ************************ #
  
  mat_skmBC_collection  <- list()
  mat_skmJAC_collection <- list()
  dists_collection_bc <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                ncol = length(smp_list),
                                dimnames = list(label = NULL,
                                                smp   = as.character(smp_list)))
  dists_collection_jac <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                 ncol = length(smp_list),
                                 dimnames = list(label = NULL,
                                                 smp   = as.character(smp_list)))
  for (smp in smp_list)
  {
    smpc <- as.character(smp)
    
    pattern   <- paste0("_k", k, "_smp", smpc, "/")
    fname_bc  <- grep(pattern = pattern, x = simkalist_bc,  value = TRUE)
    fname_jac <- grep(pattern = pattern, x = simkalist_jac, value = TRUE)
    
    dists_collection_bc[,smpc] <- 
      (mat_skmBC_collection[[smpc]]  <- read.dissimat(fname_bc)) %>%
      mat2dist(labels=labels)
    
    dists_collection_jac[,smpc] <- (
      mat_skmJAC_collection[[smpc]] <- read.dissimat(fname_jac)) %>%
      mat2dist(labels=labels)
      
  }
  
  
  # **********************  PLOT GROUP BETA-DIVERSITY  *********************** #
  
  # group plot
  plot_grtitle_bc  <- paste0(PLOT_MAIN_BASE_BC, " - k = ", k)
  plot_grtitle_jac <- paste0(PLOT_MAIN_BASE_JAC, " - k = ", k)
  fname_bc  <- paste0(output_dir, "/", PLOT_BASENAME, "_group_k", k, "_BC", PLOT_EXTENS)
  fname_jac <- paste0(output_dir, "/", PLOT_BASENAME, "_group_k", k, "_JAC", PLOT_EXTENS)
  
  # select sampling rates for the group
  m_avail <- length(smp_list) - 2
  m_step  <- (m_avail / 3) %>% as.integer()
  index1  <- 2 + round( m_step / 2)
  group_sel <- seq(index1, length(smp_list) - 1, by = m_step)
  group_sel <- c(1, group_sel, length(smp_list))
  group_sel_names <- as.character(smp_list[group_sel])
  
  legend <- paste0("Sample Rate ~", group_sel_names, "%")
  
  group_dists_bc <- dists_collection_bc[, group_sel_names]
  group_dists_jac <- dists_collection_jac[, group_sel_names]
  
  # save scatter plots on device
  plot2pdf(fname = fname_bc,
           x = dist_bracken_bc,
           y = group_dists_bc,
           main = plot_grtitle_bc,
           xlab = PLOT_XLAB_BC,
           ylab = PLOT_YLAB_BC,
           legend = legend)
  plot2pdf(fname = fname_jac,
           x = dist_bracken_jac,
           y = group_dists_jac,
           main = plot_grtitle_jac,
           xlab = PLOT_XLAB_JAC,
           ylab = PLOT_YLAB_JAC,
           legend = legend)
  
  
  
  for (smp in smp_list)
  {
    smpc <- as.character(smp)
    
    # ***************************  PLOT SMP B-DIV  *************************** #
    
    # current simple bray-curtis
    plot_title <- paste0(PLOT_MAIN_BASE_BC, " - k=", k, ", smp=", smpc, "%")
    fname <- paste0(output_dir, "/", PLOT_BASENAME, "_k", k, "_smp", smpc, "_BC", PLOT_EXTENS)
    y_bc <- dists_collection_bc[,smpc]
    plot2pdf(fname = fname,
             x = dist_bracken_bc,   y = y_bc,
             main = plot_title,
             xlab = PLOT_XLAB_BC,
             ylab = PLOT_YLAB_BC)
    
    # current simple jaccard
    plot_title <- paste0(PLOT_MAIN_BASE_JAC, " - k=", k, ", smp=", smpc, "%")
    fname <- paste0(output_dir, "/", PLOT_BASENAME, "_k", k, "_smp", smpc, "_JAC", PLOT_EXTENS)
    y_jac <- dists_collection_jac[,smpc]
    plot2pdf(fname = fname,
             x = dist_bracken_jac,   y = y_jac,
             main = plot_title,
             xlab = PLOT_XLAB_JAC,
             ylab = PLOT_YLAB_JAC)
    
    
    # ************************  CORRELATION MATRICES  ************************ #
    
    smpstr <- paste0("smp", smpc)
    cor_pears[cor_rowbc, smpstr] <- cor(dist_bracken_bc, y_bc, method = "pearson")
    cor_spear[cor_rowbc, smpstr] <- cor(dist_bracken_bc, y_bc, method = "spearman")
    cor_pears[cor_rowjac, smpstr] <- cor(dist_bracken_jac, y_jac, method = "pearson")
    cor_spear[cor_rowjac, smpstr] <- cor(dist_bracken_jac, y_jac, method = "spearman")
    
    
    
    
  }
  cor_rowbc <- cor_rowjac + 1
}



# ***************************  STORE CORRELATIONS  *************************** #
write.csv(x = cor_pears,
          file = PEARSON_FNAME,
          quote = FALSE,
          row.names = FALSE,
          col.names = TRUE)
write.csv(x = cor_spear,
          file = SPEARM_FNAME,
          quote = FALSE,
          row.names = FALSE,
          col.names = TRUE)
