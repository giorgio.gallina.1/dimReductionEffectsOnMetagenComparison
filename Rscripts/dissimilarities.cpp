#include <Rcpp.h>
#include <omp.h>
#include <iostream>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//



// [[Rcpp::plugins(openmp)]]
// [[Rcpp::export]]
NumericMatrix par_bcdist(NumericMatrix abundances)
{
  int dbg = 0;
  int nProcessors = std::max(omp_get_max_threads() - 1, 1);
  // std::cout << nProcessors << std::endl;
  CharacterVector labels = colnames(abundances);
  int N = abundances.cols();
  long int N_rows = abundances.rows();
  
  
  List partials;
  List part_sums;
  for (int i = 0; i <= nProcessors; i++)
  {
    NumericMatrix zm(N, N);
    NumericVector zv(N);
    rownames(zm) = labels;
    colnames(zm) = labels;
    partials.insert(i, zm);
    part_sums.insert(i, zv);
  }
  
  
  
#pragma omp parallel for                          \
  shared(N_rows, partials, part_sums, abundances) \
    schedule(dynamic) num_threads(nProcessors)
    for (int i = 0; i < N_rows; i++)
    {
      int tid = omp_get_thread_num();
      // std::cout << tid << " ";
      NumericMatrix cmatrix = as<NumericMatrix>(partials[tid]);
      NumericVector csums = as<NumericVector>(part_sums[tid]);
      for (int l1 = 0; l1 < N; l1++)
      {
        csums[l1] += abundances(i, l1);
        for (int l2 = l1+1; l2 < N; l2++)
        {
          double diff = std::abs(abundances(i, l1) - abundances(i, l2));
          cmatrix(l1, l2) += diff;
        }
      }
      // NumericVector abd = abundances( i , _ );
      
      // std::cout << tid << std::endl << abd << std::endl << cmatrix << std::endl << csums << std::endl;
    }
#pragma omp barrier
    
    
    NumericMatrix bcd = as<NumericMatrix>(partials[0]);
  NumericVector tot_sums = as<NumericVector>(part_sums[0]);
  for (int i = 1; i < nProcessors; i++)
  {
    bcd += as<NumericMatrix>(partials[i]);
    tot_sums += as<NumericVector>(part_sums[i]);
  }
  
  
  for (int l1 = 0; l1 < N; l1++)
  {
    double N1 = tot_sums(l1);
    for (int l2 = l1+1; l2 < N; l2++)
    {
      bcd(l1, l2) /= (N1 + tot_sums(l2));
      bcd(l2, l1) = bcd(l1, l2);
    }
  }
  
  return bcd;
}


// [[Rcpp::plugins(openmp)]]
// [[Rcpp::export]]
NumericMatrix par_jacdist(NumericMatrix abundances)
{
  int dbg = 0;
  int nProcessors = std::max(omp_get_max_threads() - 1, 1);
  // std::cout << nProcessors << std::endl;
  CharacterVector labels = colnames(abundances);
  int N = abundances.cols();
  long int N_rows = abundances.rows();
  
  
  List part_inters;
  List part_cardin;
  for (int i = 0; i <= nProcessors; i++)
  {
    NumericMatrix zm(N, N);
    NumericVector zv(N);
    rownames(zm) = labels;
    colnames(zm) = labels;
    part_inters.insert(i, zm);
    part_cardin.insert(i, zv);
  }
  
  
  
  #pragma omp parallel for                          \
  shared(N_rows, part_inters, part_cardin, abundances, nProcessors) \
  schedule(dynamic) num_threads(nProcessors)
  for (int i = 0; i < N_rows; i++)
  {
    int tid = omp_get_thread_num();
    // std::cout << tid << " ";
    NumericMatrix cmatrix = as<NumericMatrix>(part_inters[tid]);
    NumericVector csums = as<NumericVector>(part_cardin[tid]);
    for (int l1 = 0; l1 < N; l1++)
    {
      bool ab1 = (bool)abundances(i, l1);
      csums[l1] += ab1;
      for (int l2 = l1+1; l2 < N; l2++)
      {
        short int intersection = (short int)(ab1 & ((bool)abundances(i, l2)));
        cmatrix(l1, l2) += (double) intersection;
      }
    }
    // NumericVector abd = abundances( i , _ );
    
    // std::cout << tid << std::endl << abd << std::endl << cmatrix << std::endl << csums << std::endl;
  }
  #pragma omp barrier
  
  
  NumericMatrix inters = as<NumericMatrix>(part_inters[0]);
  NumericVector tot_cardins = as<NumericVector>(part_cardin[0]);
  for (int i = 1; i < nProcessors; i++)
  {
    inters += as<NumericMatrix>(part_inters[i]);
    tot_cardins += as<NumericVector>(part_cardin[i]);
  }
  for (int l1 = 0; l1 < N; l1++)
  {
    for (int l2 = l1+1; l2 < N; l2++)
    {
      inters(l2, l1) = inters(l1, l2);
    }
  }
  
  NumericVector sum_cardin_v = rep(tot_cardins, N) + rep_each(tot_cardins, N);
  NumericVector unions = sum_cardin_v - as<NumericVector>(inters);
  unions.attr("dim") = Dimension(N, N);
  // NumericMatrix unions = as<NumericMatrix>(unions_v);
  
  NumericVector jaccard = (unions - as<NumericVector>(inters)) / (unions);
  jaccard.attr("dim") = Dimension(N,N);
  rownames(jaccard) = labels;
  colnames(jaccard) = labels;
  
  for (int i = 0; i < N; i++)
  {
    jaccard(i,i) = 0;
  }
  
  return as<NumericMatrix>(jaccard);
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R

generate_dbg_smp <- function(size = 4000)
{
  p = c(0.3, rep(0.007, 100))
  smp <- sample(x = (0:100), size = size, replace = TRUE, prob = p)
  return (smp)
}

samples <- matrix(generate_dbg_smp(6000), ncol=6, dimnames = list(kmer=NULL, ds=c("A1", "A2", "A3", "B1", "B2", "C1")))

par_jacdist(samples)

library(ecodist)

distance(t(samples), method = "jaccard")

*/
