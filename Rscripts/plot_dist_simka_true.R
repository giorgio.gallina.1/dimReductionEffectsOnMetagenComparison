#!/usr/bin/Rscript

################################################################################
# Simkamin comparison plots and correlation with true distances.               #
#                                                                              #
# USAGE:                                                                       #
# ./plot_dist_simka_true.R <SIMKA_DIR> <BRACKEN_DIR> <GLOB_STATS> \            #
#    <OUTPUT_DIR>                                                              #
#                                                                              #
# PARAMETERS:                                                                  #
#    <SIMKA_DIR>:     Directory where SimkaMin collective results are stored.  #
#                     Should be $project_dir/Results/Simkamin                  #
#    <BRACKEN_DIR>:   Directory where Bracken results are stored.              #
#                     Should be $project_dir/Results/Bracken                   #
#    <GLOB_STATS>:    Path/to/filename of global (read) datasets statistics.   # 
#    <OUTPUT_DIR>:    Directory where results of this script.                  #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: March 2023                                                           #
################################################################################


library("tidyverse")
library(RColorBrewer, quietly = TRUE)

# GET RSCRIPT LOCATION
get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)

utils <- paste0(rdir, "/utils.R")
# LOAD utils.R
source(utils)



# ****************************  PARAMETERS SETUP  **************************** #

# input arguments
args <- commandArgs(T)

SIMKA_DIR  <- sub(pattern = "/$",
                  replacement = "",
                  x = args[1])
DIV_DIR <- dirname(SIMKA_DIR)
BRACKEN_DIR <- sub(pattern = "/$",
                   replacement = "",
                   x = args[2])
GLOB_STATS  <- args[3]
if (length(args) > 3)
{
  OUTPUT_DIR  <- args[4]
}else
{
  OUTPUT_DIR <- paste0(
    DIV_DIR, "/",
    basename(SIMKA_DIR),
    "__vs__TRUTH"
  )
}

dir.create(OUTPUT_DIR, recursive = TRUE)


# constants
SMP_DETL_BN   <- paste0(OUTPUT_DIR, "/sampling_details_k")
SMP_DETL_EXTN <- ".csv"
SIMKAMIN_BC   <- "mat_abundance_braycurtis.csv"
SIMKAMIN_JAC  <- "mat_presenceAbsence_jaccard.csv"
# INFO_FNAME   <- paste0(SIMKA_DIR, "/info.txt")

# taxon level as the last char of BRACKEN directory
# TAXON_LVL <- sub('.*(?=.$)', '', BRACKEN_DIR, perl=T)
# TRUE_BC_FNAME   <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_BC.csv")
# TRUE_JAC_FNAME  <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_JAC.csv")
TRUE_BC_FNAME  <- paste0(DIV_DIR, "/braycurtis_true.csv")
TRUE_JAC_FNAME <- paste0(DIV_DIR, "/jaccard_true.csv")

PLOT_BASENAME <- "plot_dist_simka"
PLOT_EXTENS   <- ".pdf"
PLOT_MAIN_BASE_BC <- "SimkaMin BC dissimilarities"
PLOT_XLAB <- "Sketch size (number of distinct k-mers)"
PLOT_YLAB_BC <- "BC dissimilarity"
PLOT_MAIN_BASE_JAC <- "SimkaMin Jaccard Distances"
PLOT_YLAB_JAC <- "Jaccard distance"


# ****************************  READ INPUT FILES  **************************** #

# get list of simkamin result folders ( "skmin_k${k}_smp${smp}" )
simkalist <- list.dirs(path = SIMKA_DIR, full.names = F, recursive = F)
# get simkamin BC and JACCARD matrices file-names
simkalist_bc  <- list.files(path = SIMKA_DIR,
                           pattern=paste0(SIMKAMIN_BC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )
simkalist_jac <- list.files(path = SIMKA_DIR,
                           pattern=paste0(SIMKAMIN_JAC, "$"),
                           full.names = TRUE,
                           recursive = TRUE )

# read Bracken dissimilarity matrices
mat_true_bc  <- read.dissimat(TRUE_BC_FNAME)
mat_true_jac <- read.dissimat(TRUE_JAC_FNAME)

# read datasets global statistics
read_stats <- read.table(file = GLOB_STATS,
                         header = TRUE, row.names = "label",
                         sep = ";", quote = "")

# # read info about sampling rates
# info <- read.table(file = INFO_FNAME,
#                    header = TRUE,
#                    sep = ",", quote ="")
# # select only useful information
# info_collection <- info[, c("k", "smp_rate", "nb_kmers", "ref_kmers")] %>%
#   unique()
# SMP_NORM <- info[1, "smp_norm"]


# *************************  GET FURTHER PARAMETERS  ************************* #

# read list of k-mer length values k
k_list <- sub(pattern = ".*_k", replacement = "", x = simkalist) %>%
          sub(pattern = "_.*", replacement = "") %>%
          as.integer() %>%
          unique()

# read list of sampling rates
smp_list <- sub(pattern = ".*_smp", replacement = "", x = simkalist) %>%
  gsub(pattern = "[^[:digit:]]", replacement = "") %>%
  as.integer() %>% unique() %>% sort()
# smp_list <- smp_list[-c(1,2)]
N_SMP <- length(smp_list)
smp_list <- c(smp_list[-1], 0)
SMP_RATES <- sprintf(fmt = "SimkaMin n. kmers: %.1e", smp_list)
names(SMP_RATES) <- smp_list
# LEGEND_TOT <- c(SMP_RATES[-N_SMP], simka="Simka exact")


# datasets ordered labels
labels <- colnames(mat_true_bc)  %>%  sort()
N_labs <- length(labels)

# convert dissimilarity matrices into distance vectors
dist_true_bc  <- mat2dist(mat_true_bc,  labels)
dist_true_jac <- mat2dist(mat_true_jac, labels)



# iterate over k-mer length
for (k in k_list)
{
  # ************************  READ SIMKAMIN MATRICES  ************************ #
  
  mat_skmBC_collection  <- list()
  mat_skmJAC_collection <- list()
  dists_collection_bc <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                ncol = length(smp_list),
                                dimnames = list(label = NULL,
                                                smp = sprintf("%d", (smp_list))))
  dists_collection_jac <- matrix(nrow = (N_labs * (N_labs - 1) / 2),
                                 ncol = length(smp_list),
                                 dimnames = list(label = NULL,
                                                 smp = sprintf("%d", (smp_list))))
  for (smp in smp_list)
  {
    smpc <- sprintf("%d", smp)
    
    pattern   <- paste0("_k", k, "_smp", smpc, "/")
    fname_bc  <- grep(pattern = pattern, x = simkalist_bc,  value = TRUE)
    fname_jac <- grep(pattern = pattern, x = simkalist_jac, value = TRUE)
    
    dists_collection_bc[,smpc] <- 
      (mat_skmBC_collection[[smpc]]  <- read.dissimat(fname_bc)) %>%
      mat2dist(labels=labels)
    
    dists_collection_jac[,smpc] <- (
      mat_skmJAC_collection[[smpc]] <- read.dissimat(fname_jac)) %>%
      mat2dist(labels=labels)
      
  }
  
  
  # **********************  PLOT GROUP BETA-DIVERSITY  *********************** #
  
  # group plot
  plot_grtitle_bc  <- paste0(PLOT_MAIN_BASE_BC, " - k = ", k)
  plot_grtitle_jac <- paste0(PLOT_MAIN_BASE_JAC, " - k = ", k)
  fname_bc  <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_BC", PLOT_EXTENS)
  fname_jac <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "_k", k, "_JAC", PLOT_EXTENS)
  
  
  plot_smp <- smp_list[-N_SMP]
  plot_smp[N_SMP] <- 5*smp_list[N_SMP-1]
  
  # save plots
  plotting.distances(pdf_name = fname_bc,
                     smp_grid = plot_smp,
                     ref_dists = dist_true_bc,
                     distances = dists_collection_bc,
                     LWD = 2,
                     main = plot_grtitle_bc,
                     xlab = PLOT_XLAB,
                     ylab = PLOT_YLAB_BC,
                     legend_title = "True Dissimilarity")
  plotting.distances(pdf_name = fname_jac,
                     smp_grid = plot_smp,
                     ref_dists = dist_true_jac,
                     distances = dists_collection_jac,
                     LWD = 2,
                     main = plot_grtitle_jac,
                     xlab = PLOT_XLAB,
                     ylab = PLOT_YLAB_JAC,
                     legend_title = "True Distance")
}

