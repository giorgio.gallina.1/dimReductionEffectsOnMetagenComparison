#!/usr/bin/Rscript

################################################################################
# This script prints the desired value from a stored table                  ####
# (it makes things easier for shell scripting)                              ####
#                                                                           ####
# USAGE                                                                     ####
# Rscript retrieve_value.R <fname> <col2rowNames> <header> <row> <col> [<sep>] #
#    <fname>:   path/to/fileName where the table is stored.                 ####
#    <col2rowNames>:  number or name of the column to be used as row.names  ####
#                     Type 'NULL' for default row.names.                    ####
#    <header>:  Either 'T' if table header is stored, or 'F' otherwise.     ####
#    <row>:     Row name or number of element to be read.                   ####
#    <col>:     Column name or number of element to be read.                ####
#    <sep>:     Character that distincts different columns.                 ####
# ========================================================================= ####
# Author: Giorgio Gallina                                                   ####
# Year: 2023                                                                ####
################################################################################

args <- commandArgs(T)

file <- args[1]
col2rownames <- args[2]
if(col2rownames == "NULL")
{
  col2rownames <- NULL
}
header <- ((toupper(args[3]) == "T") | (toupper(args[3]) == "TRUE"))
row <- args[4]
col <- args[5]
sep <- ""
if(length(args) > 5)
{
  sep <- args[6]
}

tab <- read.table(file=file, header=header, sep=sep, row.names=col2rownames)

print(tab[row, col])
