#!/usr/bin/env Rscript

################################################################################
# SPRISS comparison plots and correlation with bracken distances.              #
#                                                                              #
# USAGE:                                                                       #
# ./plot_cor_spriss_bracken.R <SPRISS_DIR> <SIMKA_DIR> <BRACKEN_DIR> \         #
#    <GLOB_STATS> [<OUTPUT_DIR>]                                               #
#                                                                              #
# PARAMETERS:                                                                  #
#    <SPRISS_DIR>:    Directory where SPRISS collective results are stored.    #
#                     Should be $project_dir/Results/Spriss_MTDabs             #
#    <SIMKA_DIR>:     Directory where SIMKA collective results are stored.     #
#                     Should be $project_dir/Results/Simkamin                  #
#    <BRACKEN_DIR>:   Directory where Bracken results are stored.              #
#                     Should be $project_dir/Results/Bracken                   #
#    <GLOB_STATS>:    Path/to/filename of global (read) datasets statistics.   # 
#    <OUTPUT_DIR>:    Directory where results of this script are written.      #
#                                                                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                      #
# Update: March 2023                                                           #
################################################################################


library("tidyverse")
# library(RColorBrewer, quietly = TRUE)

# GET RSCRIPT LOCATION
get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)

# LOAD utils.R
utils <- paste0(rdir, "/utils.R")
source(utils)


# ****************************  LOCAL FUNCTIONS  ***************************** #



# ****************************  PARAMETERS SETUP  **************************** #

# input arguments
args <- commandArgs(T)
SPRISS_DIR <- sub(pattern = "/$",
                  replacement = "",
                  x = args[1])
SIMKA_DIR  <- sub(pattern = "/$",
                   replacement = "",
                   x = args[2])
BRACKEN_DIR <- sub(pattern = "/$",
                   replacement = "",
                   x = args[3])
GLOB_STATS  <- args[4]
if (length(args) > 4)
{
  OUTPUT_DIR  <- args[5]
}else
{
  OUTPUT_DIR <- paste0(
    dirname(SPRISS_DIR), "/",
    basename(SPRISS_DIR),
    "__vs__",
    basename(BRACKEN_DIR)
  )
}

# taxon level as the last char of BRACKEN directory
TAXON_LVL <- sub('.*(?=.$)', '', BRACKEN_DIR, perl=T)

# constants
SPRISS_SMP_DIR  <- "SprissSMP"
SPRISS_FREQ_DIR <- "SprissFreq"
# SIMKAMIN_DIR    <- "SimkaMin"
SIMKA_BC_FNAME  <- "mat_abundance_braycurtis.csv"
SIMKA_JAC_FNAME <- "mat_presenceAbsence_jaccard.csv"
FREQ_BC         <- "spriss_freq_BC.csv"
FREQ_JAC        <- "spriss_freq_JAC.csv"
SPEARM_BASENAME <- paste0(OUTPUT_DIR, "/cor_spearman_k")
PEARSON_BASENAME<- paste0(OUTPUT_DIR, "/cor_pearson_k")
BRACKEN_BC     <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_BC.csv")
BRACKEN_JAC    <- paste0(BRACKEN_DIR, "/", "betadiv_taxon", TAXON_LVL, "_JAC.csv")
INFO_FNAME     <- paste0(SPRISS_DIR, "/info.txt")

PLOT_BASENAME   <- "plot_cor"
PLOT_MAIN_BASE_BC <- "BC Dissimilarities Comparison"
PLOT_XLAB <- "Minimum Frequency Threshold (theta)"
PLOT_YLAB_BC_SPEARMAN <- "Spearman Correlation"
PLOT_YLAB_BC_PEARSON <- "Pearson Correlation"
PLOT_MAIN_BASE_JAC <- "Jaccard Distances Comparison"
PLOT_YLAB_JAC_SPEARMAN <- "Spearman Correlation"
PLOT_YLAB_JAC_PEARSON <- "Pearson Correlation"


# ****************************  READ INPUT FILES  **************************** #

# get list of result folders ( "k${k}_th${theta}" )
reslist <- list.dirs(path = SPRISS_DIR, full.names = F, recursive = F)

# read Bracken dissimilarity matrices
mat_bracken_bc  <- read.dissimat(BRACKEN_BC)
mat_bracken_jac <- read.dissimat(BRACKEN_JAC)

N_DATASETS <- nrow(mat_bracken_bc)

# read datasets global statistics
read_stats <- read.table(file = GLOB_STATS,
                         header = TRUE, row.names = "label",
                         sep = ";", quote = "")

# read info about sampling rates (theta - numb-kmers)
info <- read.table(file = INFO_FNAME,
                   header = TRUE,
                   sep = ",", quote ="")
# select only useful information
info_collection <- info[, c("k", "theta_real", "smp_nkmers")] %>%
                   unique()


# *************************  GET FURTHER PARAMETERS  ************************* #

# read list of k-mer length values k
k_list <- sub(pattern = ".*k", replacement = "", x = reslist) %>%
          sub(pattern = "_.*", replacement = "") %>%
          as.integer() %>%
          unique()



# *****************************  PREPARE OUTPUT  ***************************** #


dir.create(path = OUTPUT_DIR, recursive = TRUE)


for (k in k_list)
{
  # read list of sampling rates
  theta_list <- sub(pattern = ".*th", replacement = "", x = reslist) %>%
    gsub(pattern = "[^[:digit:].eE-]", replacement = "") %>%
    unique() %>% sort()
  
  
  # correlations table
  cor_spear_bc <- data.frame()
  cor_pears_bc <- data.frame()
  cor_spear_jac <- data.frame()
  cor_pears_jac <- data.frame()
  cor_index <- 1
  
  spearm_bc_fname <- paste0(SPEARM_BASENAME, k, "_BC.csv")
  pearson_bc_fname <- paste0(PEARSON_BASENAME, k, "_BC.csv")
  spearm_jac_fname <- paste0(SPEARM_BASENAME, k, "_JAC.csv")
  pearson_jac_fname <- paste0(PEARSON_BASENAME, k, "_JAC.csv")
  
  
  thetas_num <- theta_str2num(theta_list)
  thetas_order <- order(thetas_num)
  thetas_num <- thetas_num[thetas_order]
  theta_list <- theta_list[thetas_order]
  
  
  # ***********  SIMKA EXACT kmer DISSIMILARITIES  ********** #
  
  matcmp_bc <- paste0(SIMKA_DIR, "/skmin_k", k, "_smp0/", SIMKA_BC_FNAME) %>%
               read.dissimat()
  matcmp_jac <- paste0(SIMKA_DIR, "/skmin_k", k, "_smp0/", SIMKA_JAC_FNAME) %>%
                read.dissimat()
  
  # get available labels
  current_labels <- row.names(matcmp_bc) %>% sort()
  
  # get bracken distances relative to available datasets (reference)
  dist_ref_bc <- mat2dist(mat_bracken_bc, labels = current_labels)
  dist_ref_jac <- mat2dist(mat_bracken_jac, labels = current_labels)
  
  # get simka exact distances
  dist_cmp_bc <- mat2dist(matcmp_bc, labels = current_labels)
  dist_cmp_jac <- mat2dist(matcmp_jac, labels = current_labels)
  
  # correlation test
  simka.spearman_bc <- cor(dist_ref_bc, dist_cmp_bc,
                           use = "complete.obs",
                           method = "spearman")
  simka.spearman_jac <- cor(dist_ref_jac, dist_cmp_jac,
                            use = "complete.obs",
                            method = "spearman")
  
  simka.pearson_bc <- cor(dist_ref_bc, dist_cmp_bc,
                          use = "complete.obs",
                          method = "pearson")
  simka.pearson_jac <- cor(dist_ref_jac, dist_cmp_jac,
                           use = "complete.obs",
                           method = "pearson")
  
  freq_line = NULL
  
  for (theta_index in 1:length(theta_list))
  {
    # theta as directory string
    theta <- theta_list[theta_index]
    # theta as number
    theta_num <- thetas_num[theta_index]
    
    current_dir <- paste0(SPRISS_DIR, "/k", k, "_th", theta)
    
    # # save simkamin sampling sizes in output table
    # info_selection <- (info_collection$k == k) & (info_collection$theta_real == theta_num)
    # sample_size <- info_collection[info_selection, "nbkmers"] %>% unique()
    # cor_pears_bc[cor_index, "simkamin_smp_size"] = cor_spear_bc[cor_index, "simkamin_smp_size"] <- sample_size
    # cor_pears_jac[cor_index, "simkamin_smp_size"] = cor_spear_jac[cor_index, "simkamin_smp_size"] <- sample_size
    # 
    
    
    
    # SPRISS SAMPLES
    
    matcmp_bc <- paste(current_dir, SPRISS_SMP_DIR, SIMKA_BC_FNAME, sep = "/") %>%
      read.dissimat()
    matcmp_jac <- paste(current_dir, SPRISS_SMP_DIR, SIMKA_JAC_FNAME, sep = "/") %>%
      read.dissimat()
    
    # get available labels - skip if fewer than available
    current_labels <- row.names(matcmp_bc) %>% sort()
    current_n_datasets <- length(current_labels)
    if(current_n_datasets < N_DATASETS)
    {
      next
    }
    
    cor_pears_bc[cor_index, "n_ds"] = cor_spear_bc[cor_index, "n_ds"] <- current_n_datasets
    cor_pears_jac[cor_index, "n_ds"] = cor_spear_jac[cor_index, "n_ds"] <- current_n_datasets
    
    # get bracken distances relative to available datasets (reference)
    dist_ref_bc <- mat2dist(mat_bracken_bc, labels = current_labels, na.to.one = TRUE)
    dist_ref_jac <- mat2dist(mat_bracken_jac, labels = current_labels, na.to.one = TRUE)
    
    # save theta in output table
    cor_pears_bc[cor_index, "theta"] = cor_spear_bc[cor_index, "theta"] <- theta_num
    cor_pears_jac[cor_index, "theta"] = cor_spear_jac[cor_index, "theta"] <- theta_num
    
    
    # get spriss distances based on frequent kmers estimates
    dist_cmp_bc <- mat2dist(matcmp_bc, labels = current_labels)
    dist_cmp_jac <- mat2dist(matcmp_jac, labels = current_labels)
    
    # correlation test
    spearman_bc <- cor.test(dist_ref_bc, dist_cmp_bc,
                            alternative = "two.sided",
                            method = "spearman")
    spearman_jac <- cor.test(dist_ref_jac, dist_cmp_jac,
                             alternative = "two.sided",
                             method = "spearman")
    
    pearson_bc <- cor.test(dist_ref_bc, dist_cmp_bc,
                           alternative = "two.sided",
                           method = "pearson")
    pearson_jac <- cor.test(dist_ref_jac, dist_cmp_jac,
                            alternative = "two.sided",
                            method = "pearson")
    
    # store values
    cor_spear_bc[cor_index, "cor_spriss_smp"]    <- spearman_bc$estimate
    cor_spear_bc[cor_index, "pvalue_spriss_smp"] <- spearman_bc$p.value
    
    cor_spear_jac[cor_index, "cor_spriss_smp"]    <- spearman_jac$estimate
    cor_spear_jac[cor_index, "pvalue_spriss_smp"] <- spearman_jac$p.value
    
    cor_pears_bc[cor_index, "cor_spriss_smp"]    <- pearson_bc$estimate
    cor_pears_bc[cor_index, "pvalue_spriss_smp"] <- pearson_bc$p.value
    
    cor_pears_jac[cor_index, "cor_spriss_smp"]    <- pearson_jac$estimate
    cor_pears_jac[cor_index, "pvalue_spriss_smp"] <- pearson_jac$p.value
    
    
    
    
    
    # SPRISS FREQUENCY
    matcmp_bc <- paste(current_dir, SPRISS_FREQ_DIR, FREQ_BC, sep = "/") %>%
                 read.dissimat()
    matcmp_jac <- paste(current_dir, SPRISS_FREQ_DIR, FREQ_JAC, sep = "/") %>%
                  read.dissimat()
    
    if(nrow(matcmp_bc) < N_DATASETS & is.null(freq_line))
    {
      freq_line <- theta_num
    }
    
    # get spriss distances based on frequent kmers estimates
    dist_cmp_bc <- mat2dist(matcmp_bc, labels = current_labels, na.to.one = TRUE)
    dist_cmp_jac <- mat2dist(matcmp_jac, labels = current_labels, na.to.one = TRUE)
    
    # correlation test
    spearman_bc <- cor.test(dist_ref_bc, dist_cmp_bc,
                            alternative = "two.sided",
                            method = "spearman")
    spearman_jac <- cor.test(dist_ref_jac, dist_cmp_jac,
                             alternative = "two.sided",
                             method = "spearman")
    
    pearson_bc <- cor.test(dist_ref_bc, dist_cmp_bc,
                           alternative = "two.sided",
                           method = "pearson")
    pearson_jac <- cor.test(dist_ref_jac, dist_cmp_jac,
                            alternative = "two.sided",
                            method = "pearson")
    
    # store values
    cor_spear_bc[cor_index, "cor_freq"]    <- spearman_bc$estimate
    cor_spear_bc[cor_index, "pvalue_freq"] <- spearman_bc$p.value
    
    cor_spear_jac[cor_index, "cor_freq"]    <- spearman_jac$estimate
    cor_spear_jac[cor_index, "pvalue_freq"] <- spearman_jac$p.value
    
    cor_pears_bc[cor_index, "cor_freq"]    <- pearson_bc$estimate
    cor_pears_bc[cor_index, "pvalue_freq"] <- pearson_bc$p.value
    
    cor_pears_jac[cor_index, "cor_freq"]    <- pearson_jac$estimate
    cor_pears_jac[cor_index, "pvalue_freq"] <- pearson_jac$p.value
    
    
    
    
    cor_index = cor_index + 1
  }
  
  
  write.csv(file = spearm_bc_fname,
            x = cor_spear_bc,
            quote = F,
            row.names = F,
            col.names = T)
  write.csv(file = spearm_jac_fname,
            x = cor_spear_jac,
            quote = F,
            row.names = F,
            col.names = T)
  write.csv(file = pearson_bc_fname,
            x = cor_pears_bc,
            quote = F,
            row.names = F,
            col.names = T)
  write.csv(file = pearson_jac_fname,
            x = cor_pears_jac,
            quote = F,
            row.names = F,
            col.names = T)
  
  simka_legend <- "Simka exact vs. BRACKEN"
  plot_name <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "Spearman_BC_k", k, ".pdf")
  plotting.ptlines.pdf(pdfname = plot_name,
             x = cor_spear_bc[,"theta"],
             y = cor_spear_bc[,c("cor_freq", "cor_spriss_smp")],
             const_ref = simka.spearman_bc,
             log = "x",
             main = paste0(PLOT_MAIN_BASE_BC, " - k=", k),
             xlab = PLOT_XLAB,
             ylab = PLOT_YLAB_BC_SPEARMAN,
             legend = c("SPRISS freq. vs. BRACKEN", "SPRISS sample vs. BRACKEN"),
             const_legend = simka_legend,
             vline = freq_line)
  
  plot_name <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "Pearson_BC_k", k, ".pdf")
  plotting.ptlines.pdf(pdfname = plot_name,
             x = cor_pears_bc[,"theta"],
             y = cor_pears_bc[,c("cor_freq", "cor_spriss_smp")],
             const_ref = simka.pearson_bc,
             log = "x",
             main = paste0(PLOT_MAIN_BASE_BC, " - k=", k),
             xlab = PLOT_XLAB,
             ylab = PLOT_YLAB_BC_PEARSON,
             legend = c("SPRISS freq. vs. BRACKEN", "SPRISS sample vs. BRACKEN"),
             const_legend = simka_legend,
             vline = freq_line)
  
  
  plot_name <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "Spearman_JAC_k", k, ".pdf")
  plotting.ptlines.pdf(pdfname = plot_name,
             x = cor_spear_jac[,"theta"],
             y = cor_spear_jac[,c("cor_freq", "cor_spriss_smp")],
             const_ref = simka.spearman_jac,
             log = "x",
             main = paste0(PLOT_MAIN_BASE_JAC, " - k=", k),
             xlab = PLOT_XLAB,
             ylab = PLOT_YLAB_JAC_SPEARMAN,
             legend = c("SPRISS freq. vs. BRACKEN", "SPRISS sample vs. BRACKEN"),
             const_legend = simka_legend,
             vline = freq_line)
  
  plot_name <- paste0(OUTPUT_DIR, "/", PLOT_BASENAME, "Pearson_JAC_k", k, ".pdf")
  plotting.ptlines.pdf(pdfname = plot_name,
             x = cor_pears_jac[,"theta"],
             y = cor_pears_jac[,c("cor_freq", "cor_spriss_smp")],
             const_ref = simka.pearson_jac,
             log = "x",
             main = paste0(PLOT_MAIN_BASE_JAC, " - k=", k),
             xlab = PLOT_XLAB,
             ylab = PLOT_YLAB_JAC_PEARSON,
             legend = c("SPRISS freq. vs. BRACKEN", "SPRISS sample vs. BRACKEN"),
             const_legend = simka_legend,
             vline = freq_line)
  
  

}

