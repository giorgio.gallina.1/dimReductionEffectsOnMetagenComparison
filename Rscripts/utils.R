#!/usr/bin/env Rscript

################################################################################
# This is a collection of useful functions.                                    #
#                                                                              #
# Author: Giorgio Gallina                                                      #
# Update: March 2023                                                           #
################################################################################

COLOR_PALETTE <- NULL

# get kmer count 
ds.statistics.kmer.tally <- function(glob_statistics_df, k_list)
{
  library("tidyverse")
  
  k_list <- matrix(k_list, nrow=1, dimnames = list(row=NULL, col=k_list))
  nks <- ncol(k_list)
  
  # number of nitrogenous bases in each datasets (repeated in as many columns as #k_list)
  nbases <- (as.numeric(glob_statistics_df$nreads) * as.numeric(glob_statistics_df$readlen.avg)) %>%
            round() %>%
            rep(times = nks) %>%
            matrix(ncol=nks, dimnames = list(label=row.names(glob_statistics_df), col=NULL))   
  
  # number of reads rearranged for matrix operation
  nreads <- matrix(glob_statistics_df$nreads, ncol=1, dimnames = list(label=glob_statistics_df$label, col=NULL))
  
  # get number of kmers for each dataset, for each k
  nkmers <- nbases - (nreads %*% (k_list-1))
  colnames(nkmers) <- k_list
  
  return (nkmers)
}

# 
# # get sampling size in number of kmers for given datasets and PERCENTUAL sampling
# # rate, for each desired value k of k-mer length
# ds.simkamin.sampling.size.slow <- function(glob_statistics, k_list, smp_rate_target)
# {
#   library("tidyverse")
#   
#   smp_nkmers <- ds.statistics.kmer.tally(glob_statistics, k_list) %>%
#                 (apply(MARGIN = 2, FUN = mean) * smp_rate_target / 100) %>%
#                 round()
#                 
#   return (smp_nkmers)
# }

# # given:
# #   tot_kmers = matrix of datasets sizes (=total #kmers) having rows for ds and 
# #               columns for k-mer lengths k,
# #   smp_rate_target = grid of sampling rates in PERCENTUALS (column matrix),
# # get the size in number of kmers of each sub-sample rate.
# #
# # EXAMPLE
# # mean.by.column(tot_kmers) = (100, 120, 130)
# # smp_rate_target = (10, 50, 100)T
# # smp_nkmers = ( 10,  12,  13)
# #              ( 50,  60,  65)
# #              (100, 120, 130)
# ds.simkamin.sampling.size.fast <- function(tot_kmers, smp_rate_target)
# {
#   smp_rate_target <- matrix(smp_rate_target / 100, ncol=1, dimnames = list(smprate=smp_rate_target, NULL))
#   smp_nkmers <- smp_rate_target %*% apply(tot_kmers, MARGIN = 2, FUN = mean)
#   smp_nkmers <- round(smp_nkmers)
#   colnames(smp_nkmers) <- colnames(tot_kmers)
#   
#   return (smp_nkmers)
# }


plotting.chooseColors <- function(colors = plotting.getColors(), n = NULL)
{
  library(tidyverse)
  
  pairwise_color_cmp <- function(col1, col2)
  {
    return (255 * (col1 - col2) / (col1 + col2 + 0.001))
  }
  
  
  if(is.null(n))
  {
    n <- length(colors)
  }
  
  color.dist <- function(c_ref, c_cmp)
  {
    cc2 <- c(substr(c_cmp, 2, 3),
             substr(c_cmp, 4, 5),
             substr(c_cmp, 6, 7))
    cc2 <- paste0("0x", cc2) %>% strtoi() %>%
      matrix(ncol = 3, byrow = FALSE)
    # clearness <- apply(X = cc2,
    #                    MARGIN = 1,
    #                    FUN = mean)
    cc2 <- cbind(cc2,
                 pairwise_color_cmp(cc2[,1], cc2[,2]),
                 pairwise_color_cmp(cc2[,3], cc2[,1]),
                 pairwise_color_cmp(cc2[,2], cc2[,3]))
    # cc2[,1:3] <- 255 * (cc2[,1:3] / clearness)
    
    cc1 <- c(substr(c_ref, 2, 3),
             substr(c_ref, 4, 5),
             substr(c_ref, 6, 7))
    cc1 <- paste0("0x", cc1) %>% strtoi() %>%
           rep(each = nrow(cc2)) %>%
           matrix(ncol = 3)
    # clearness <- apply(X = cc1,
    #                    MARGIN = 1,
    #                    FUN = mean)
    cc1 <- cbind(cc1,
                 pairwise_color_cmp(cc1[,1], cc1[,2]),
                 pairwise_color_cmp(cc1[,3], cc1[,1]),
                 pairwise_color_cmp(cc1[,2], cc1[,3]))
    # cc1[,1:3] <- 255 * (cc1[,1:3] / clearness)
    
    my.norm = function(v)
    {
      sqrt(sum(v**2))
    }
    d <- apply(X = (cc1 - cc2),
               MARGIN = 1,
               FUN = my.norm)
    
    return (d)
  }
  
  second.max <- function(v)
  {
    if(length(v) == 1)
    {
      return(list(max = v[1], index = 1))
    }
    
    ind <- c(1,1)
    max <- v[ind]
    for (ci in 2:length(v))
    {
      element <- v[ci]
      if (element > max[1])
      {
        max[2] <- max[1]
        max[1] <- element
        ind[2] <- ind[1]
        ind[1] <- ci
      }else if(element > max[2])
      {
        max[2] <- element
        ind[2] <- ci
      }
    }
    return (list(max=max[2], index=ind[2]))
  }
  
  N_AVAIL <- length(colors)
  choice1 <- c(4, 5, 12, 6, 8, 9)
  choice <- choice1[1] #as.integer(runif(1) * 10000) %% N_AVAIL
  result <- colors[choice]
  colors <- colors[-choice]
  distances <- color.dist(result, colors)
  for (i in 2:length(choice1))
  {
    choice1[choice1 > choice] <- choice1[choice1 > choice] - 1
    choice <- choice1[i]
    col_choice <- colors[choice]
    distances <- distances[-choice]
    colors <- colors[-choice]
    result <- c(result, col_choice)
    d_upd <- color.dist(col_choice, colors)
    distances <- pmin.int(d_upd, distances)
  }
  for (i in 1:length(colors))
  {
    # choice = second.max(distances)$index
    choice = which.max(distances)
    col_choice <- colors[choice]
    distances <- distances[-choice]
    colors <- colors[-choice]
    result <- c(result, col_choice)
    d_upd <- color.dist(col_choice, colors)
    distances <- pmin.int(d_upd, distances)
  }
  
  # result <- c(result[-1], result[1])
  return(result)
  
}


# get a vector of colors to be used in graphs to distinguish different samples
plotting.getColors <- function(ncolors = 15, renew = FALSE, random = FALSE)
{
  if ((renew) | is.null(COLOR_PALETTE))
  {
    library(RColorBrewer, quietly = TRUE)
    paletteTot <- brewer.pal(8, "Accent")
    paletteTot <- c(paletteTot[-4], brewer.pal(8, "Set2"))
    if (random)
    {
      COLOR_PALETTE <<- sample(x = paletteTot,
                            size = length(paletteTot),
                            replace = FALSE)
    }else
    {
      COLOR_PALETTE <<- paletteTot
    }
    
  }
  
  return (COLOR_PALETTE[1:ncolors])
}


theta_str2num <- function(thetas)
{
  theta_num <- paste0("0.", thetas) %>% as.numeric()
}


# read dissimilarity matrix
read.dissimat <- function(filename)
{
  # filecon <- file(description = filename, open = "r")
  mat <- read.csv(file = filename,
                  header = TRUE,
                  row.names = 1,
                  sep = ";",
                  quote = "")
  
  # flush(filecon)
  # close(filecon)
  return (mat)
}



# given a dissimilarity matrix and a list of labels, get the distances as vector
mat2dist <- function(mat, labels=NULL, na.to.one = FALSE)
{
  library(tidyverse)
  
  dist <- c()
  pair_labels <- c()
  
  if(is.null(labels))
  {
    labels <- colnames(mat) %>% sort()
  }
  
  N <- length(labels)
  for (i in 1:(N-1))
  {
    lab1 <- labels[i]
    for (j in (i+1):N)
    {
      lab2 <- labels[j]
      label <- paste0(lab1, "_", lab2)
      # if(is.na(label))
      # {
      #   message(i, lab1, j, lab2, label)
      # }
      pair_labels <- c(pair_labels, label)
      tryCatch(
        {
          value <- mat[lab1, lab2]
          dist[label] <- value
          },
        error = function(cond)
        {
          dist[label] <<- NA
          # message("NA at label ", label)
        }
      )
      
    }
  }
  
  if(na.to.one)
  {
    dist[is.na(dist)] <- 1.0
  }
  return (dist[pair_labels])
}


plotting.scatter2pdf <- function(fname, x, y, main, xlab, ylab, legend=NULL, height=6, pointsize=12, ...)
{
  pdf(fname, width=(16*height/9), height=height, pointsize = pointsize)
  
  scatter.bivar(x, y, main, xlab, ylab, legend, ...)
  
  dev.off()
}


# plot lines. Every column correspond to a different line
plotting.ptlines.pdf <- function(pdfname, x, y, main, xlab, ylab,
                                 targets = NULL,
                                 legend = NULL, legend_position = "bottomleft",
                                 const_ref = NULL,
                                 const_legend = NULL,
                                 legend_horiz = FALSE,
                                 target_legend = NULL,
                                 height = 7,
                                 PCH = 20,
                                 xgap = NULL,
                                 log = "",
                                 pointsize = 12,
                                 vline = NULL,
                                 ...)
{
  TYPE <- "o"
  LWD <- 3
  CEX <- 1.3
  lcex <- 0.95
  
  # get colors for plotting
  color_palette <- plotting.getColors(random = FALSE) %>%
                   plotting.chooseColors()
  
  # figure dimensions
  margins <- par("mar")
  
  
  N_consts <- length(const_ref)
  
  if(!is.null(const_ref) && is.null(const_legend))
  {
    const_legend <- "Reference"
    if (N_consts > 1)
    {
      const_legend <- paste0(const_legend, 1:N_consts)
    }
  }
  
  
  # manage multiple plots at once (let diff. variables be diff. columns)
  x <- as.matrix(x)
  y <- as.matrix(y)
  if (nrow(y) != nrow(x))
  {
    if (ncol(y) != nrow(x))
    {
      message("Incompatible dimensions of x and y. No plot drawn.")
      return (NA)
    }
    y <- t(y)
  }
  
  # dimensions
  N_val <- nrow(y) # = length(x)
  M_var <- ncol(y) # number of variables to plot against x
  
  
  # LEGEND SETUP
  if (!is.null(legend) & M_var > 1)
  {
    if (!is.null(targets) & !is.null(target_legend))
    {
      legend <- matrix(c(legend, target_legend), nrow=2, byrow = TRUE) %>%
                as.vector()
      pchs = rep(c(PCH, NA), times = M_var)
      ltys = rep(c("solid", "dotdash"), times = M_var)
      colors = rep(color_palette[1:M_var], each = 2)
    }
    else
    {
      pchs = rep(PCH, times = M_var)
      ltys = rep("solid", times = M_var)
      colors = color_palette[1:M_var]
    }
    
    if (!is.null(const_ref))
    {
      legend <- c(legend, const_legend)
      pchs <- c(pchs, rep(NA, N_consts))
      ltys <- c(ltys, rep("twodash", N_consts))
      colors <- c(colors, color_palette[M_var+(1:N_consts)])
    }
    
    if (is.null(legend_position))
    {
      lx <- ly <- 0
      bty <- 'n'
      xpd <- NA
      legend_horiz = FALSE
      
      # dev.off()
      par(mar=c(margins[1:3], 0.8), cex = CEX)
      plot.new()
      l <- legend(x = lx, y = ly,
                  bty=bty,
                  legend = legend,
                  pch = pchs,
                  lty = ltys,
                  lwd = LWD,
                  cex = lcex,
                  ...)
      w <- grconvertX(l$rect$w, to='ndc') - grconvertX(0, to='ndc')
      margins <- par("mar")
      dev.off()
    }else
    {
      lx <- legend_position
      ly <- NULL
      bty <- 'o'
      w <- 0
      xpd <- TRUE
    }
  }
  

  
  pdf(pdfname, width=(height*4/3), height=height)
  
  par(mar=margins, cex = CEX)
  par(omd=c(0, 1-w, 0, 1))
  
  
  # get dissimilarity ranges
  yrange <- range(c(y, targets, const_ref), na.rm = TRUE)# + c(-0.12, 0.05)
  yrdist <- yrange[2] - yrange[1]
  yrange <- yrange + (yrdist * c(-0.3, 0.05))
  
  # adjust xlim if more x values are provided
  xrange = NULL
  if (ncol(x) != 1)
  {
#    xrange <- range(x, na.rm = TRUE)
    # xwidth <- xrange[2] - xrange[1]
    # xmargin <- xwidth * 0.005
    # xrange <- xrange + c(-xmargin, xmargin)
  }
  
  # plot first variable y
  if (is.null(xgap))
  {
    res.plot <- plot(x = x[,1], y = y[,1],
                     log = log,
                     type = TYPE,
                     pch = PCH,
                     lwd = LWD,
                     cex = CEX,
                     main = main,
                     xlab = xlab,   ylab = ylab,
                     xlim = xrange, ylim = yrange,
                     col=color_palette[1])
    if (!is.null(targets))
    {
      abline(h = targets[1],
             col = color_palette[1],
             lwd = LWD,
             lty = "dotdash")
    }
  }else
  {
    library(plotrix)
    res.plot <- gap.plot(x = x[,1], y = y[,1],
                         log = log,
                         gap = xgap,
                         gap.axis = "x",
                         type = TYPE,
                         pch = PCH,
                         lwd = LWD,
                         cex = CEX,
                         main = main,
                         xlab = xlab,   ylab = ylab,
                         xlim = xrange, ylim = yrange,
                         col=color_palette[1])
    if (!is.null(targets))
    {
      abline(h = targets[1],
             col = color_palette[1],
             lwd = LWD,
             lty = "dotdash")
    }
  }
  

  
  # plot other variables y in different colors
  if (M_var > 1)
  {
    for (i in 2:M_var)
    {
      if (ncol(x) == 1)
      {  j <- 1; }
      else
      {  j <- i; }
      lines(x = x[,j], y = y[,i],
            # log = log,
            type = TYPE,
            pch = PCH,
            lwd = LWD,
            cex = CEX,
            col=color_palette[i])
      if (!is.null(targets))
      {
        abline(h = targets[i],
               col = color_palette[i],
               lwd = LWD,
               lty = "dotdash")
      }
    }
  }
  
  if(!is.null(const_ref))
  {
    abline(h = const_ref,
           col = color_palette[M_var+(1:N_consts)],
           lwd = LWD,
           lty = "twodash")
  }
  
  if(!is.null(vline))
  {
    abline(v = vline,
           col = color_palette[M_var + N_consts + 1],
           lwd = LWD,
           lty = "longdash")
  }
  
  # print legend
  if(is.null(legend_position))
  {
    lx <- par('usr')[2]
    ly <- par('usr')[4]
    if(par('xlog'))
    {
      lx <- 10 ^ lx
    }
    if(par('ylog'))
    {
      ly <- 10 ^ ly
    }
  }
  if (!is.null(legend) & M_var > 1)
  {
    legend(x = lx, y = ly,
           bty = bty,
           xpd = xpd,
           legend = legend,
           col = colors,
           pch = pchs,
           lty = ltys,
           lwd = LWD,
           cex = lcex,
           horiz = legend_horiz,
           ...)
  }
  
  dev.off()
  
  return (res.plot)
}


# plot beta-div x against beta-div y
plotting.scatter.bivar <- function(x, y, main, xlab, ylab, legend=NULL, z=NULL, zlegend=NULL, legend_pos="bottomright", ...)
{
  # manage multiple scatters at once (let diff. variables be diff. columns)
  y <- as.matrix(y)
  if (nrow(y) != length(x))
  {
    if (ncol(y) != length(x))
    {
      message("Incompatible dimensions of x and y. No plot drawn.")
      return (NA)
    }
    y <- t(y)
  }
  if (!is.null(z))
  {
    z <- as.matrix(z)
    if (nrow(z) != length(z))
    {
      if (ncol(z) != length(z))
      {
        message("Incompatible dimensions of x and z. No plot drawn.")
        return (NA)
      }
      z <- t(z)
    }
    if(is.null(zlegend))
    {
      zlegend <- "Reference"
    }
  }
  
  # dimensions
  N_val <- nrow(y) # = length(x)
  M_var <- ncol(y) # number of variables to plot against x
  
  # get dissimilarity ranges
  xrange <- range(x, na.rm = TRUE)
  yrange <- range(c(y,z), na.rm = TRUE)
  trange <- c(xrange, yrange)  %>%  range() %>% + c(-0.05, 0.05)
  
  col_palette <- brewer.pal(11, "BrBG")[-c(5:7)][9-(M_var:1)]
  cols <- col_palette
  pchs <- c(25, rep(18, M_var-2), 19)
  ltys <- rep(NA, M_var)
  
  # plot first variable y
  res.plot <- plot(x = x, y = y[,1],
                   main = main,
                   xlab = xlab,   ylab = ylab,
                   xlim = trange, ylim = trange,
                   pch = pchs[1], col = col_palette[1])
  
  
  # reference line (bisector)
  abline(0, 1, col="chocolate1")
  
  # plot other variables y in different colors
  if (M_var > 1)
  {
    for (i in M_var:2)
    {
      points(x = x, y = y[,i],
             pch = pchs[i], col = col_palette[i])
    }
  }

  # plot z
  if (!is.null(z))
  {
    points(x = x, y = z,
           pch = 8,
           col = "darkmagenta")
    
    #update legend
    legend <- c(legend, zlegend)
    pchs <- c(pchs, 8)
    cols <- c(cols, "darkmagenta")
    ltys <- c(ltys, NA)
  }
  
  # prepare legend
  cols = c(cols, "chocolate1")
  pchs = c(pchs, NA)
  ltys = c(ltys, 1)
  
  # print legend if provided and reasonable
  if (!is.null(legend) & M_var > 1)
  {
    legend <- c(legend, "perfect match")
    legend(x = legend_pos,
           legend = legend,
           col = cols,
           pch = pchs,
           lty = ltys,
           cex = 1.1,
           ...)
  }
  
  return (res.plot)
}



plotting.distances <- function(pdf_name,
                               smp_grid, distances, ref_dists = NULL,
                               main, xlab, ylab,
                               legend_position = NULL,
                               height = 8,
                               log = "x",
                               legend_title = NULL,
                               vsplit = NULL,
                               LWD = 1,
                               ...)
{
  TYPE <- "l"
  CEX <- 1.3
  lcex <- 0.9
  PDF_PTSIZE <- 14 # 12 * CEX
  PDF_RATIO = 3 / 4
  
  # get colors for plotting
  color_palette <- plotting.getColors(random = FALSE) %>%
                   plotting.chooseColors()
  
  # figure dimensions
  margins <- par("mar")
  margins_multiplier <- c(-0.3, 0.02)
  
  
  # manage multiple plots at once (let diff. variables be diff. columns)
  smp_grid <- as.matrix(smp_grid)
  distances <- as.matrix(distances)
  if (nrow(distances) != nrow(smp_grid))
  {
    if (ncol(distances) != nrow(smp_grid))
    {
      message("Incompatible dimensions of smp_grid and distances. No plot drawn.")
      message(pdf_name)
      message("smp_grid:  ", nrow(smp_grid), "x", ncol(smp_grid))
      message("distances: ", nrow(distances), "x", ncol(distances))
      return (NA)
    }
    distances <- t(distances)
  }
  
  # dimensions
  N_val <- nrow(distances) # = length(smp_grid)
  M_var <- ncol(distances) # number of variables to plot against smp_grid
  
  # get dissimilarity ranges
  yrange <- range(distances, na.rm = TRUE)# + c(-0.12, 0.05)
  yrdist <- yrange[2] - yrange[1]
  
  # adjust xlim if more smp_grid values are provided
  xrange = NULL
  if (ncol(smp_grid) != 1)
  {
    #    xrange <- range(smp_grid, na.rm = TRUE)
    # xwidth <- xrange[2] - xrange[1]
    # xmargin <- xwidth * 0.005
    # xrange <- xrange + c(-xmargin, xmargin)
  }
  
  # LEGEND SETUP
  legend <- NULL
  lcolors <- "black";
  line_colors <- rep(1, M_var)
  
  if(!is.null(ref_dists))
  {
    legend <- c("d > 0.9",
                "0.8 < d < 0.9",
                "0.6 < d < 0.8",
                "0.4 < d < 0.6",
                "0.2 < d < 0.4",
                "d < 0.2")
    lcolors <- color_palette[c(6, 1:5)]
    line_colors[TRUE] <- 1
    line_colors[ref_dists < 0.2] <- 6
    line_colors[(ref_dists >= 0.2) & (ref_dists < 0.4)] <- 5
    line_colors[(ref_dists >= 0.4) & (ref_dists < 0.6)] <- 4
    line_colors[(ref_dists >= 0.6) & (ref_dists < 0.8)] <- 3
    line_colors[(ref_dists >= 0.8) & (ref_dists < 0.9)] <- 2
    # line_colors[ref_dists >= 0.9] <- 6
  }
  
  N_leg <- length(legend)
  ltys = rep("solid", times = N_leg)
  
  if (!is.null(legend))
  {
    
    pdf("tmp_legend.pdf", width=(height/PDF_RATIO), height=height, pointsize = PDF_PTSIZE)
    par(cex = CEX)
    
    if (is.null(legend_position))
    {
      lx <- ly <- 0
      bty <- 'n'
      xpd <- NA
      margins_multiplier[1] <- - margins_multiplier[2]
      
      yrange_tmp <- yrange + (yrdist * margins_multiplier)
      
      # dev.off()
      par(mar=c(margins[1:3], 0.8))
      plot.new()
      l <- legend(x = lx, y = ly,
                  bty=bty,
                  xpd = xpd,
                  legend = legend,
                  lty = ltys,
                  lwd = 4,
                  # cex = lcex,
                  title = legend_title,
                  ...)
      
      w <- grconvertX(l$rect$w, to='ndc') - grconvertX(0, to='ndc')
      margins <- par("mar")
      dev.off()
      file.remove("tmp_legend.pdf")
    }else
    {
      lx <- legend_position
      ly <- NULL
      bty <- 'o'
      w <- 0
      xpd <- TRUE
    }
  }
  
  yrange <- yrange + (yrdist * margins_multiplier)
  
  
  pdf(pdf_name, width=(height/PDF_RATIO), height=height, pointsize = PDF_PTSIZE)
  par(cex = CEX)
  
  par(mar=margins)
  par(omd=c(0, 1-w, 0, 1))
  
  # plot first variable distances
  res.plot <- plot(x = smp_grid[,1], y = distances[,1],
                   log = log,
                   type = TYPE,
                   lwd = LWD,
                   cex = CEX,
                   main = main,
                   xlab = xlab,   ylab = ylab,
                   xlim = xrange, ylim = yrange,
                   col=lcolors[line_colors[1]])
  
  
  # plot other variables distances in different colors
  if (M_var > 1)
  {
    for (i in 2:M_var)
    {
      if (ncol(smp_grid) == 1)
      {  j <- 1; }
      else
      {  j <- i; }
      lines(x = smp_grid[,j], y = distances[,i],
            type = TYPE,
            lwd = LWD,
            cex = CEX,
            col=lcolors[line_colors[i]])
    }
  }
  
  if(is.null(vsplit))
  {
    vsplit <- smp_grid[length(smp_grid) - 1]
  }
  abline(v = vsplit, col = "red", lty = "twodash", lwd = 3)
  
  
  # print legend
  if(is.null(legend_position))
  {
    lx <- par('usr')[2]
    ly <- par('usr')[4]
    if(par('xlog'))
    {
      lx <- 10^(lx)
    }
  }
  if (!is.null(legend) & M_var > 1)
  {
    legend(x = lx, y = ly,
           bty = bty,
           xpd = xpd,
           legend = legend,
           col = lcolors,
           lty = ltys,
           lwd = 4,
           # cex = lcex,
           title = legend_title,
           ...)
  }
  
  dev.off()
  
  return (res.plot)
}


plot2pdf.bivar <- function(fname, x, y, main, xlab, ylab, legend=NULL, pointsize=13, legend_pos = "bottomrigth", ...)
{
  pdf(fname, width=8, height=6, pointsize = pointsize)
  
  plotting.scatter.bivar(x, y, main, xlab, ylab, legend = legend, legend_pos = legend_pos, ...)
  
  dev.off()
}




jacc.fun.basic <- function(two_samples, rows = TRUE, cols = c(1, 2))
{
  inters <- sum( as.numeric( apply(X = samples[rows, cols],
                                   MARGIN = 1,
                                   FUN = min) > 0 ) )
  union <- sum( as.numeric( apply(X = samples[rows, cols],
                                  MARGIN = 1,
                                  FUN = max) > 0 ) )
  return ( 1 - (inters / union) )
}

jacc.fun.parallel.refbycopy <- function(two_samples)
{
  library(doParallel, quietly = T)
  library(parallel, quietly = T)
  library(foreach, quietly = T)
  n_cores <- detectCores()
  if (n_cores > 1)
  {
    n_cores <- n_cores - 1
  }
  myCluster <- makeCluster(n_cores, type = "FORK")
  registerDoParallel(myCluster)
  
  n_values <- nrow(two_samples)
  n_values_per_chunk <- round(n_values / n_cores)
  
  raw_seq <- seq(from = 1,
                 to = n_values,
                 by = n_values_per_chunk)
  raw_seq[n_cores + 1] <- n_values + 1
  
  indices <- matrix(data = c(raw_seq[1:n_cores], (raw_seq[2:(n_cores+1)] - 1)),
                    byrow = TRUE, nrow = 2)
  
  summary.jacc <- foreach(core_ind = 1:n_cores, .combine = "+") %dopar%
    {
      rows <- seq(from = indices[1, core_ind],
                  to   = indices[2, core_ind])
      intersection <- sum( as.numeric( apply(X = two_samples[rows, 1:2],
                                             MARGIN = 1,
                                             FUN = min) > 0 ) )
      union <- sum( as.numeric( apply(X = two_samples[rows, 1:2],
                                      MARGIN = 1,
                                      FUN = max) > 0 ) )
      c(intersection, union)
    }
  
  stopCluster(myCluster)
  
  return (1 - (summary.jacc[1] / summary.jacc[2]))
}


jacc.fun.parallel <- function(column1, column2)
{
  if (column1 == column2)
  {
    return (0.0)
  }
  
  library(doParallel, quietly = T)
  library(parallel, quietly = T)
  library(foreach, quietly = T)
  n_cores <- detectCores()
  if (n_cores > 1)
  {
    n_cores <- n_cores - 1
  }
  myCluster <- makeCluster(n_cores, type = "FORK")
  registerDoParallel(myCluster)
  
  n_values <- nrow(samples)
  n_values_per_chunk <- round(n_values / n_cores)
  
  raw_seq <- seq(from = 1,
                 to = n_values,
                 by = n_values_per_chunk)
  raw_seq[n_cores + 1] <- n_values + 1
  
  indices <- matrix(data = c(raw_seq[1:n_cores], (raw_seq[2:(n_cores+1)] - 1)),
                    byrow = TRUE, nrow = 2)
  
  summary.jacc <- foreach(core_ind = 1:n_cores, .combine = "+") %dopar%
    {
      rows <- seq(from = indices[1, core_ind],
                  to   = indices[2, core_ind])
      intersection <- sum( as.numeric( apply(X = samples[rows, c(column1, column2)],
                                             MARGIN = 1,
                                             FUN = min) > 0 ) )
      union <- sum( as.numeric( apply(X = samples[rows, c(column1, column2)],
                                      MARGIN = 1,
                                      FUN = max) > 0 ) )
      c(intersection, union)
    }
  
  stopCluster(myCluster)
  
  return (1 - (summary.jacc[1] / summary.jacc[2]))
}


bcdissim <- function(samples)
{
  samples <- as.matrix(samples)
  N_smp <- ncol(samples)
  smp_names <- colnames(samples)
  smp_sizes <- apply(X = samples, MARGIN = 2, FUN = sum)
  
  dis_mat <- matrix(0, nrow = N_smp, ncol = N_smp,
                    dimnames = list(sample=smp_names, sample=smp_names))
  
  for (smp1 in 1:(N_smp-1))
  {
    name1 <- smp_names[smp1]
    for (smp2 in (smp1+1):N_smp)
    {
      name2 <- smp_names[smp2]
      
      dis_mat[name1, name2] = dis_mat[name2, name1] <-
        sum(abs(samples[,smp1] - samples[,smp2])) / (smp_sizes[smp1] + smp_sizes[smp2])
    }
  }
  return (dis_mat)
}

bray.fun.parallel.withcopy <- function(two_samples)
{
  library(doParallel, quietly = T)
  library(parallel, quietly = T)
  library(foreach, quietly = T)
  n_cores <- detectCores()
  if (n_cores > 1)
  {
    n_cores <- n_cores - 1
  }
  myCluster <- makeCluster(n_cores, type = "FORK")
  registerDoParallel(myCluster)
  
  n_values <- nrow(two_samples)
  n_values_per_chunk <- round(n_values / n_cores)
  
  raw_seq <- seq(from = 1,
                 to = n_values,
                 by = n_values_per_chunk)
  raw_seq[n_cores + 1] <- n_values + 1
  
  indices <- matrix(data = c(raw_seq[1:n_cores], (raw_seq[2:(n_cores+1)] - 1)),
                    byrow = TRUE, nrow = 2)
  
  smp_sizes <- apply(X = two_samples, MARGIN = 2, FUN = sum)
  
  bray.nonOverlap <- foreach(core_ind = 1:n_cores, .combine = "+") %dopar%
    {
      rows <- seq(from = indices[1, core_ind],
                  to   = indices[2, core_ind])
      sum(abs(two_samples[rows, 1] - two_samples[rows, 2]))
    }
  
  stopCluster(myCluster)
  
  return (bray.nonOverlap / (smp_sizes[1] + smp_sizes[2]))
}

# samples contains the data to work on
bray.fun.parallel <- function(column1, column2)
{
  if (column1 == column2)
  {
    return (0.0)
  }
  
  
  n_values <- nrow(samples)
  
  if(n_values > 1000000)
  {
    library(doParallel, quietly = T)
    library(parallel, quietly = T)
    library(foreach, quietly = T)
    n_cores <- detectCores()
    if (n_cores > 1)
    {
      n_cores <- n_cores - 1
    }
    myCluster <- makeCluster(n_cores, type = "FORK")
    registerDoParallel(myCluster)
  }else
  {
    n_cores = 1
  }
  
  n_values_per_chunk <- round(n_values / n_cores)
  
  raw_seq <- seq(from = 1,
                 to = n_values,
                 by = n_values_per_chunk)
  raw_seq[n_cores + 1] <- n_values + 1
  
  indices <- matrix(data = c(raw_seq[1:n_cores], (raw_seq[2:(n_cores+1)] - 1)),
                    byrow = TRUE, nrow = 2)
  
  smp_size_sum <- foreach(core_ind = 1:n_cores, .combine = "+") %dopar%
    {
      rows <- seq(from = indices[1, core_ind],
                  to   = indices[2, core_ind])
      sum(samples[rows, c(column1, column2)])
    }
  
  bray.nonOverlap <- foreach(core_ind = 1:n_cores, .combine = "+") %dopar%
    {
      rows <- seq(from = indices[1, core_ind],
                  to   = indices[2, core_ind])
      sum(abs(samples[rows, column1] - samples[rows, column2]))
    }
  
  if(n_values > 1000000)
  {
    stopCluster(myCluster)
  }
  
  return (bray.nonOverlap / smp_size_sum)
}


bcdissim <- function(samples)
{
  samples <- as.matrix(samples)
  N_smp <- ncol(samples)
  smp_names <- colnames(samples)
  smp_sizes <- apply(X = samples, MARGIN = 2, FUN = sum)
  
  dis_mat <- matrix(0, nrow = N_smp, ncol = N_smp,
                    dimnames = list(sample=smp_names, sample=smp_names))
  
  for (smp1 in 1:(N_smp-1))
  {
    name1 <- smp_names[smp1]
    for (smp2 in (smp1+1):N_smp)
    {
      name2 <- smp_names[smp2]
      
      dis_mat[name1, name2] = dis_mat[name2, name1] <-
        sum(abs(samples[,smp1] - samples[,smp2])) / (smp_sizes[smp1] + smp_sizes[smp2])
    }
  }
  return (dis_mat)
}

# rows = species, columns = samples
jaccdist <- function(samples)
{
  
  tryCatch(
    {
      library(ecodist)
      
      # transpose data.frame for dissimilarity computation
      tsamples <- as.data.frame(t(samples))
      
      # calculate distances and save them in matrices
      jac <- distance(x=tsamples, method="jaccard")
      jac <- as.matrix(jac)
      
      return(jac)
    },
    error = function(cond)
    {
      message(cond)
      message("Trying with function in utils.R")
    }
  )

  # If there wan an error, compute distance by hard code
  
  rm(tsamples)
  
  samples <- as.matrix(samples)
  smp_names <- colnames(samples)
  N_smp <- ncol(samples)
  smp_sizes <- apply(X = samples, MARGIN = 2, FUN = sum)
  
  dis_mat <- matrix(0, nrow = N_smp, ncol = N_smp,
                    dimnames = list(sample=smp_names, sample=smp_names))
  
  for (smp1 in 1:(N_smp-1))
  {
    name1 <- smp_names[smp1]
    for (smp2 in (smp1+1):N_smp)
    {
      name2 <- smp_names[smp2]
      # inters <- sum( as.numeric( apply(X = samples[, c(smp1, smp2)],
      #                                  MARGIN = 1,
      #                                  FUN = min) > 0 ) )
      # union <- sum( as.numeric( apply(X = samples[, c(smp1, smp2)],
      #                                 MARGIN = 1,
      #                                 FUN = max) > 0 ) )
      # dis_mat[name1, name2] = dis_mat[name2, name1] <- 1 - (inters / union)
      dis_mat[name1, names2] = dis_mat[name2, name1] <-
        jacc.fun.parallel(samples[,c(smp1,smp2)])
    }
  }
  return (dis_mat)
}

myEcoDist <- function(samples, method = "bray", use_ecodist=TRUE)
{
  if (use_ecodist)
  {
    tryCatch(
      {
        library(ecodist)
        
        # transpose data.frame for dissimilarity computation
        samples <- as.data.frame(t(samples))
        
        # calculate distances and save them in matrices
        if (method == "bray")
        {
          d <- bcdist(samples)
        }else{
          d <- distance(x=samples, method=method)
        }
          
        dis_mat <- as.matrix(d)
        
        return(dis_mat)
      },
      error = function(cond)
      {
        message(cond)
        message("Trying with function in utils.R")
      },
      finally = {samples <- as.matrix(t(samples))}
    )
  }
  
  
  # If there wan an error, compute distance by hard code
  
  
  samples <- as.matrix(samples)
  N_smp <- ncol(samples)
  smp_names <- colnames(samples)
  if (is.null(smp_names))
  {
    smp_names <- paste0("S", 1:N_smp)
  }
  smp_sizes <- apply(X = samples, MARGIN = 2, FUN = sum)
  
  dis_mat <- matrix(0, nrow = N_smp, ncol = N_smp,
                    dimnames = list(sample=smp_names, sample=smp_names))
  
  for (smp1 in 1:(N_smp-1))
  {
    name1 <- smp_names[smp1]
    for (smp2 in (smp1+1):N_smp)
    {
      name2 <- smp_names[smp2]
      if (method == "jaccard")
      {
        dis_mat[name1, name2] = dis_mat[name2, name1] <-
        jacc.fun.parallel(smp1,smp2)
      }else if(method == "bray")
      {
        dis_mat[name1, name2] = dis_mat[name2, name1] <-
          bray.fun.parallel(smp1,smp2)
      }
      
    }
  }
  return (dis_mat)
}


generate_dbg_smp <- function(size = 4000)
{
  p = c(0.3, rep(0.007, 100))
  smp <- sample(x = (0:100), size = size, replace = TRUE, prob = p)
  return (smp)
}
