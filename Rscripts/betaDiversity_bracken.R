#!/usr/bin/env Rscript

################################################################################
# This script is used to compute Jaccard & Bray-Curtis dissimilarities between 
# datasets on the basis of BRACKEN's estimates of species relative abundances.
#
# For help, run:
# ./betaDiversity_bracken.R --help
#
# ============================================================================ #
# Author: Giorgio Gallina
# Update: February 2023
################################################################################


# library("ecodist", quietly = TRUE)

library(tidyverse)

get_this_file <- function() {
  commandArgs() %>%
    tibble::enframe(name = NULL) %>%
    tidyr::separate(
      col = value, into = c("key", "value"), sep = "=", fill = "right"
    ) %>%
    dplyr::filter(key == "--file") %>%
    dplyr::pull(value)
}
this_file <- get_this_file()
rdir <- dirname(this_file)

# LOAD utils.R
utils <- paste0(rdir, "/utils.R")
source(utils)

library("argparse", quietly = TRUE)

make.relative <- function(estimates) #DEPRECATED: BC uses absolute abund
{
  scaling_factor <- 10000
  total <- sum(estimates)
  rel_estim <- estimates * scaling_factor / total
  return (estimates) # NO RELATIVISATION! DELETE THIS LINE IF NEEDED
  return (rel_estim)
}

thisFile <- function() {
  cmdArgs <- commandArgs(trailingOnly = FALSE)
  needle <- "--file="
  match <- grep(needle, cmdArgs)
  if (length(match) > 0) {
    # Rscript
    return(normalizePath(sub(needle, "", cmdArgs[match])))
  } else {
    # 'source'd via R console
    return(normalizePath(sys.frames()[[1]]$ofile))
  }
}

getProjectDir <- function() {
  return ( dirname( dirname( thisFile() ) ) )
}

# **************************************************************************** #

parser <- ArgumentParser()

parser$add_argument("collectionDescription",
                    type = "character",
                    help = "Path to the file of the collection description.")
parser$add_argument("brackenDir",
                    type = "character",
                    help = "Path to directory where bracken abundance estimates are stored.")
parser$add_argument("-o", "--outputBasename",
                    type = "character",
                    default = NULL,
                    help = "Basename of the output files. [Default: $projectDir/Results/$collection_name/Bracken/betadiv_tS]")
parser$add_argument("-s", "--separator",
                    type = "character",
                    default = "",
                    help = "The character used in the table to separate columns. [Default: %(default)s]")
parser$add_argument("-t", "--taxonomyLevel",
                    type = "character",
                    default = "S",
                    help = "Taxonomy lavel of intereste as a single upper case character (eg. S for species, G for genus, F for family). [Default: %(default)s]")
parser$add_argument("-f", "--minFrequence",
                    type = "double",
                    default = 0,
                    help = "Minimum frequency cut-off. [Default %(default)s]")
parser$add_argument("-x", "--suffix",
                    type = "character",
                    default = ".bracken",
                    help = "Suffix of bracken abundances estimates files, that is the part of the filename after the dataset name (eg, '_abun.bracken' for 'GOS011_abun.bracken' if 'GOS11' is the ds_name). [Default: %(default)s]")


args <- parser$parse_args()


COLLECTION_FNAME <- args$collectionDescription
BRACKEN_DIR      <- args$brackenDir
OUT_BASENAME     <- args$outputBasename
sep        <- args$separator
taxonLevel <- args$taxonomyLevel
minFreq    <- args$minFrequence

# INPUT CHECK
if (! file.exists(COLLECTION_FNAME))
{
  stop("File ", COLLECTION_FNAME, " does not exist.")
}
if (! dir.exists(BRACKEN_DIR))
{
  stop("Bracken directory ", BRACKEN_DIR, " does not exist.")
}
# Set default output basename
if(is.null(OUT_BASENAME))
{
  collection.name <- sub(pattern = "(.*?)\\..*$",
                         replacement = "\\1",
                         basename(COLLECTION_FNAME))
  project_dir <- getProjectDir()
  OUT_BASENAME <- paste0(project_dir, "/Results/", collection.name, "/Bracken/betadiv_t", taxonLevel)
}

# make output directory if does not exist
dir.create(path = dirname(OUT_BASENAME),
           recursive = TRUE)

# get label to brackenRes_path correspondences
collection.descr <- read.table(file = COLLECTION_FNAME,
                               header = TRUE,
                               row.names="label",
                               sep = sep)
labels <- sort(row.names(collection.descr))

# get paths/to/filenames of species' relative abundances estimates
# in_fnames <- list.files(path = BRACKEN_DIR,
#                         pattern = ".bracken",
#                         full.names = TRUE)
in_fnames <- paste0(BRACKEN_DIR, "/", collection.descr[labels,"ds_name"], ".bracken")
names(in_fnames) <- labels


# read percent relative abundances of first dataset
try({
  lbl <- labels[1]
  current <- read.delim(file=in_fnames[lbl], header=T, sep="\t", quote="\"")
  taxonomy_cond <- (current$taxonomy_lvl == taxonLevel)
  freq_cond <- (current$fraction_total_reads > minFreq)
  abundances <- current[ (freq_cond & taxonomy_cond) , c("taxonomy_id", "new_est_reads")]
  # tot.reads <- sum(abundances$new_est_reads)
  # abundances$new_est_reads <- abundances$new_est_reads / tot.reads * 100
#  abundances$new_est_reads <- make.relative(abundances$new_est_reads)
  names(abundances) <- c("taxonomy_id", lbl)
})

# get percent relative abundances of every other datasets
for (lbl in labels[-1])
{
  try({
    current <- read.delim(file=in_fnames[lbl], header=T, sep="\t", quote="\"")
    taxonomy_cond <- (current$taxonomy_lvl == taxonLevel)
    freq_cond <- (current$fraction_total_reads > minFreq)
    current <- current[ (freq_cond & taxonomy_cond) , c("taxonomy_id", "new_est_reads")]
    # tot.reads <- sum(current$new_est_reads)
    # current$new_est_reads <- current$new_est_reads / tot.reads * 100
    # current$new_est_reads <- make.relative(current$new_est_reads)
    names(current) <- c("taxonomy_id", lbl)
    abundances <- merge(abundances, current, by="taxonomy_id", all=T, sort=T)
    rm(current, taxonomy_cond, freq_cond)
  })
}
abundances[is.na(abundances)] <- 0.0
row.names(abundances) <- abundances$taxonomy_id
abundances <- abundances[-1]

# transpose data.frame for dissimilarity computation
# abundancest <- as.data.frame(t(abundances))

# calculate distances and save them in matrices
# bcd <- bcdist(abundancest)
# bcd <- as.matrix(bcd)
# jac <- distance(x=abundancest, method="jaccard")
# jac <- as.matrix(jac)
bcd <- myEcoDist(abundances, method="bray")
jac <- myEcoDist(abundances, method="jaccard")

# write results to files
# write.table(x=bcd, file=paste0(OUT_BASENAME, "_BC.txt"), quote=F, sep=";", row.names=T, col.names=T)
# write.table(x=jac, file=paste0(OUT_BASENAME, "_JAC.txt"), quote=F, sep=";", row.names=T, col.names=T)
# write in CSV format
writeChar(";", con=paste0(OUT_BASENAME, "_BC.csv"), eos=NULL)
write.table(x=bcd, file=paste0(OUT_BASENAME, "_BC.csv"), quote=F, sep=";", row.names=T, col.names=T, append=T)
writeChar(";", con=paste0(OUT_BASENAME, "_JAC.csv"), eos=NULL)
write.table(x=jac, file=paste0(OUT_BASENAME, "_JAC.csv"), quote=F, sep=";", row.names=T, col.names=T, append=T)
