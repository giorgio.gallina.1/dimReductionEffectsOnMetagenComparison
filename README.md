# Analysis of Dimensionality Reduction Effects on Metagenomic Comparison


This repository contains the scripts used by the owner of this repository for his master degree thesis (University of Padua, Computer Engineering, 21 April 2023).
His work consisted in empirically analysing how subsampling affects reference-free metagenomic comparison in relation to reference-based comparison, as far as **Bray-Curtis** and **Jaccard** dissimilarities are used.
He analysed impact of SimkaMin's sketching, and the use of both SPRISS' metagenome subsampling and SPRISS approximate frequent k-mers set on k-mer-based metagenomic comparison.
The reference-based method for benchmarking is based on the KRAKEN 2 classifier and BRACKEN for abundance estimation.

The scripts gathered here permit to:
 - download some datasets from the GOS, HMP, and CAMI II toy HMP;
 - build a reference database for KRAKEN 2 classification;
 - obtain reference-based dissimilarities (BRACKEN-based);
 - obtain true dissimilarities on a simulated CAMI dataset;
 - run SimkaMin with different sketching sizes (number of distinct k-mers) and desidered k-mer lengths;
 - compute dissimilarities on both SPRISS' subsamplings, on a user-defined grid of minimum frequencies thresholds (theta) and desired k-mer lengths;
 - draw heatmaps and othe plots of results.


## References

### SPRISS
Git repository: https://github.com/VandinLab/SPRISS.git

Presentation paper: https://doi.org/10.1093/bioinformatics/btac180

In addition, two scripts have been modified in order to suite the analysis. These are stored in folder `src`.


### SIMKA
Git repository: https://github.com/GATB/simka.git

Simka paper: https://doi.org/10.7717/peerj-cs.94

SimkaMin paper: https://doi.org/10.1093/bioinformatics/btz685


### BRACKEN + KRAKEN 2
Git Bracken repository: https://github.com/jenniferlu717/Bracken.git

Bracken's peer-reviewed paper (published Jan 2, 2017): "Bracken: estimating species abundance in metagenomics data" https://peerj.com/articles/cs-104/

Protocol paper for Kraken 2, Bracken, KrakenUniq, and KrakenTools (published Sept 28, 2022): "Metagenome analysis using the Kraken software suite" https://www.nature.com/articles/s41596-022-00738-y

Git Kraken 2 repository: https://github.com/DerrickWood/kraken2.git

Kraken 2 presentation: https://doi.org/10.1186/s13059-019-1891-0

Kraken 2 protocol: https://doi.org/10.1038/s41596-022-00738-y

Pre-compiled indices: https://benlangmead.github.io/aws-indexes/k2 

In addition, two KRAKEN 2 scripts have been modified in order to overcome some issues in library download (see https://github.com/DerrickWood/kraken2/issues/525#issuecomment-1087310681 though such a fix seems not to be enough). These are stored in folder `src` and are used in "hard" installation from git repository (see script `BashScripts/brachen_hard_install.bash`).
By default, the `setup.sh` script will try to install KRAKEN 2, BRACKEN and KRAKENTOOLS in a conda environment through Bioconda, however, this installation is likely to rise issues in reference database download and preparation; in such a case, the mentioned script will be automatically executed to install these programs in your project directory for subsequent use.


## System Requirements
 - Adequate computational resources (Disk space, RAM, cpu cores...)
 - Linux (Ubuntu) operating system
 - Anaconda with Bioconda
 - see `setup.sh` for other dependences.
A definition file for a Singularity container is provided. Its disk image should occupy about $2.6$ Gigabytes

## System preparation
run script `setup.sh`. You need administrator privileges. Otherwise, the script is easily convertible in its essential parts for building a Singularity container.


## Usage
The easiest way to run the analysis is by firsly run the script `parameters_preparation.bash` with the required arguments. Then run `run_bracken.bash` for the reference-based analysis, `run_spriss.bash` for the study of SPRISS' sampling schemes, `run_simkamin.bash` for the analysis of SimkaMin sketching. Most of the scripts have an help on the usage on the top their source code, while some of them also print the help when run with `-h` or `--help` option.
