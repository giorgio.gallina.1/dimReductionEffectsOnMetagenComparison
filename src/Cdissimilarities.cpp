#include <omp.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
using namespace std;

void par_bcdist(vector<long int> *abundances, long int nrows, int ncols, vector<string> &row_names, string fname)
{
  int nProcessors = omp_get_max_threads();  
  
  long int part_dif[nProcessors][ncols][ncols];
  long int part_sum[nProcessors][ncols];
  double bcd[ncols][ncols];
  long int tot_sums[ncols];
  
  /*
  cout << "nb_cores: " << nProcessors << endl;
  cout << "abundances[0][0]: " << abundances[0][0] << endl;
  cout << "((vector<long int>)abundances[0]).at(0): " << ((vector<long int>)abundances[0]).at(0) << endl;
  */
  
#pragma omp parallel for \
  shared(ncols, part_dif, part_sum, nProcessors) \
  schedule(dynamic) num_threads(nProcessors)
  for (int i = 0; i < nProcessors; i++)
  {
    for (int j = 0; j < ncols; j++)
    {
      part_sum[i][j] = 0;
      for (int k = 0; k < ncols; k++)
      {
        part_dif[i][j][k] = 0;
      }
    }
  }
#pragma omp barrier

  // cout << "1st cycle done." << endl;
  for (int j = 0; j < ncols; j++)
  {
    tot_sums[j] = 0;
    for (int k = 0; k < ncols; k++)
    {
	  bcd[j][k] = 0;
    }
  }
  // cout << "2nd cycle done." << endl;
 
#pragma omp parallel for                          \
  shared(nrows, ncols, part_dif, part_sum, abundances) \
  schedule(dynamic) num_threads(nProcessors)
  for (int i = 0; i < nrows; i++)
  {
    int tid = omp_get_thread_num();
    // cout << tid << " ";
    for (int l1 = 0; l1 < ncols; l1++)
    {
      part_sum[tid][l1] += abundances[i][l1];
      for (int l2 = l1+1; l2 < ncols; l2++)
      {
        double diff = abs(abundances[i][l1] - abundances[i][l2]);
        part_dif[tid][l1][l2] += diff;
      }
    }
    // NumericVector abd = abundances( i , _ );
    
    // std::cout << tid << std::endl << abd << std::endl << cmatrix << std::endl << csums << std::endl;
  }
#pragma omp barrier

  // cout << endl << "3rd cycle done." << endl;
    
    
  
#pragma omp parallel for \
  shared(nrows, ncols, part_dif, part_sum, abundances) \
  schedule(dynamic) num_threads(nProcessors)
  for(int j = 0; j < ncols; j++)
  {
    for (int i = 0; i < nProcessors; i++)
    {
      tot_sums[j] += part_sum[i][j];
      for(int k = j+1; k < ncols; k++)
      {
        bcd[j][k] += (double) part_dif[i][j][k];
      }
    }
  }
#pragma omp barrier

  // cout << "4th cycle done." << endl;
  
  for (int l1 = 0; l1 < ncols; l1++)
  {
    long int N1 = tot_sums[l1];
    for (int l2 = l1+1; l2 < ncols; l2++)
    {
      bcd[l1][l2] /= (double) (N1 + tot_sums[l2]);
      bcd[l2][l1] = bcd[l1][l2];
    }
  }
  
  // cout << "5th cycle done." << endl;
  
  fstream fout;
  fout.open(fname, ios::out);
  for(int k = 0; k < ncols; k++)
  {
    fout << ";" << row_names[k];
  }
  for(int j = 0; j < ncols; j++)
  {
    fout << "\n";
    fout << row_names[j];
    for(int k = 0; k < ncols; k++)
    {
      fout << ";" << bcd[j][k];
    }
  }
  
  
  // cout << "All done." << endl;
  
  fout.close();
}




void par_jacdist(vector<long int> *abundances, long int nrows, int ncols, vector<string> &row_names, string fname)
{
  int nProcessors = omp_get_max_threads();  
  
  long int part_inters[nProcessors][ncols][ncols];
  long int part_cardin[nProcessors][ncols];
  long int tot_inters[ncols][ncols];
  long int tot_cardin[ncols];
  double jaccard[ncols][ncols];
  
  /*
  cout << "nb_cores: " << nProcessors << endl;
  cout << "abundances[0][0]: " << abundances[0][0] << endl;
  cout << "((vector<long int>)abundances[0]).at(0): " << ((vector<long int>)abundances[0]).at(0) << endl;
  */
  
#pragma omp parallel for \
  shared(ncols, part_inters, part_cardin, nProcessors) \
  schedule(dynamic) num_threads(nProcessors)
  for (int i = 0; i < nProcessors; i++)
  {
    for (int j = 0; j < ncols; j++)
    {
      part_cardin[i][j] = 0;
      for (int k = 0; k < ncols; k++)
      {
        part_inters[i][j][k] = 0;
      }
    }
  }
#pragma omp barrier

  // cout << "1st cycle done." << endl;
  for (int j = 0; j < ncols; j++)
  {
    tot_cardin[j] = 0;
    for (int k = 0; k < ncols; k++)
    {
	  jaccard[j][k] = 0;
	  tot_inters[j][k] = 0;
    }
  }
  // cout << "2nd cycle done." << endl;
 
#pragma omp parallel for                          \
  shared(nrows, ncols, part_inters, part_cardin, abundances) \
  schedule(dynamic) num_threads(nProcessors)
  for (int i = 0; i < nrows; i++)
  {
    int tid = omp_get_thread_num();
    // cout << tid << " ";
    for (int l1 = 0; l1 < ncols; l1++)
    {
      bool ab1 = (bool)abundances[i][l1];
      part_cardin[tid][l1] += ab1;
      for (int l2 = l1+1; l2 < ncols; l2++)
      {
        short int intersection = (short int)(ab1 & ((bool)abundances[i][l2]));
        part_inters[tid][l1][l2] += (long int) intersection;
      }
    }
    // NumericVector abd = abundances( i , _ );
    
    // std::cout << tid << std::endl << abd << std::endl << cmatrix << std::endl << csums << std::endl;
  }
#pragma omp barrier

  // cout << endl << "3rd cycle done." << endl;
  
#pragma omp parallel for \
  shared(nrows, ncols, part_inters, part_cardin, abundances) \
  schedule(dynamic) num_threads(nProcessors)
  for(int j = 0; j < ncols; j++)
  {
    for (int i = 0; i < nProcessors; i++)
    {
      tot_cardin[j] += part_cardin[i][j];
      for(int k = j+1; k < ncols; k++)
      {
        tot_inters[j][k] += part_inters[i][j][k];
      }
    }
  }
#pragma omp barrier

  // cout << "4th cycle done." << endl;
  
  double setunion;
  for (int l1 = 0; l1 < ncols; l1++)
  {
    long int N1 = tot_cardin[l1];
    for (int l2 = l1+1; l2 < ncols; l2++)
    {
      setunion = (double) (N1 + tot_cardin[l2] - tot_inters[l1][l2]);
      jaccard[l1][l2] = (setunion - tot_inters[l1][l2]) / setunion;
      jaccard[l2][l1] = jaccard[l1][l2];
    }
  }
  
  // cout << "5th cycle done." << endl;
  
  fstream fout;
  fout.open(fname, ios::out);
  for(int k = 0; k < ncols; k++)
  {
    fout << ";" << row_names[k];
  }
  for(int j = 0; j < ncols; j++)
  {
    fout << "\n";
    fout << row_names[j];
    for(int k = 0; k < ncols; k++)
    {
      fout << ";" << jaccard[j][k];
    }
  }
  
  
  // cout << "All done." << endl;
  
  fout.close();
}



int main(int argc, char *argv[])
{
  string fname_in = string(argv[1]);
  string method = string(argv[2]);
  string fname_out = string(argv[3]);
  
  ifstream fin(fname_in);
  string line, word, temp;
  
  vector<string> labels;
  vector<vector<long int>> abundances;
  int ncols = 0;
  long int nrows = 0;
  
  // read header
  getline(fin, line);
  stringstream s(line);
  while(getline(s, word, ','))
  {
    labels.push_back(word);
    ncols++;
  }
  
  // read content
  while (getline(fin, line)) {
    stringstream s(line);
    
    vector<long int> abd_line;
    
    for (int i = 0; i < ncols; i++)
    {
      getline(s, word, ',');
      abd_line.push_back(stol(word));
    }
    
    abundances.push_back(abd_line);
    nrows++;
        
  }
  
  /*
  cout << "read done." << endl;
  cout << "dimension: " << nrows << "x" << ncols << endl;
  cout << "abundances.size(): " << abundances.size() << endl;
  cout << "abundances.at(1).at(2) " << abundances.at(1).at(2) << endl;
  */
  
  fin.close();
  
  if (method.compare("bc") == 0)
  {
    par_bcdist(abundances.data(), nrows, ncols, labels, fname_out);
    cerr << "Results stored as " << fname_out << endl;
    return 0;
  }else if (method.compare("jaccard") == 0)
  {
    par_jacdist(abundances.data(), nrows, ncols, labels, fname_out);
    cerr << "Results stored as " << fname_out << endl;
    return 0;
  }
  
  
  cerr << "Invalid method " << method << endl;
  cerr << "Please choose either 'bc' or 'jaccard'" << endl;
  return 1;
}

