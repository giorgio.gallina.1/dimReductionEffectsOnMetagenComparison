# What is gathered into this directory?

In this directory I collect the scripts of those software linked in the main README.md that I had to slightly modify in order for them to fix my analysis protocol.
These are the main changes I have made for them.
  - `create_sample.cpp`: form SPRISS project to sub-sample metagenomic datasets according to SPRISS protocol. I modified it so to also accept FASTA file format rather than allowing FASTQ format only.
  - `spriss_uf.py`: from SPRISS project to estimate frequent k-mers frequencies. Modified so to be more user-friendly, allowing more parameters management and running on FASTA-formatted datasets as well as on FASTQ-formatted ones.
  - `download_genomic_library.sh`: from KRAKEN2 project to download genomic references for database construction. Modified so to ftp download from `https://` server rather than `ftp://`.
  - `rsync_from_ncbi.pl`: from KRAKEN2 to rsync-download sequences. Modified in accordance with `download_genomic_library.sh` change.
  - `mask_low_complexity.sh`: from KRAKEN2 to mask low complexity sequences using `duskmasker` from NCBI-BLAST+. Modified so to find `dustmasker` by means of `command -v dustmasker` rather than `which dustmasker`, which rised issues in the server I used for my analysis.
  - `bracken-build`: from BRACKEN project to build reference database. Modified so to exit with exit status `1` on failure.