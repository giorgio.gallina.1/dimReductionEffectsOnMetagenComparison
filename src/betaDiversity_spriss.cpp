#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <omp.h>
#include <cstring>
#include <cstdlib>
#include <set>
#include <map>
#include <array>
#include <filesystem>
using namespace std;

/* 
 * Author: Giorgio Gallina
 * Last Update: March 2023
 */

static const vector<string> NULL_VECSTR;

long int kmer2long(string kmer)
{
	long int hash = 1;
	for (auto it = kmer.begin(); it != kmer.end(); ++it)
	{
		hash <<= 2;
		switch (*it)
		{
			case 'a':
			case 'A':
				break;
			case 'c':
			case 'C':
				hash += 1;
				break;
			case 'g':
			case 'G':
				hash += 2;
				break;
			case 't':
			case 'T':
				hash += 3;
				break;
			default:
				hash = 0; //ERROR
				break;
		}
	}
	
	return hash;
}

vector<vector<string>> read_table_header(string ifname, const char sep = ' ', const vector<string> &col_names = NULL_VECSTR)
{
	ifstream fin(ifname);
	vector<vector<string>> result;
	short unsigned int col_index[1000];
	short unsigned int col_count = 0; // n columns of input table
	short unsigned int inp_ncol = 0; // n columns of input table actually useful
	short unsigned int res_ncol; // n columns of output table
	
	string line, word;
	
	// manage header
	getline(fin, line);
	stringstream s(line);
	while(getline(s, word, sep))
	{
		if(col_names != NULL_VECSTR)
		{
			col_index[col_count] = -1;
			for (short unsigned int i = 0; i < col_names.size(); i++)
			{
				// cout << col_names[i] + ".compare(" + word + ") = " <<  (col_names[i].compare(word) == 0) << endl;
				if (col_names[i].compare(word) == 0)
				{
					col_index[col_count++] = i;
					inp_ncol = col_count;
					break;
				}
			}
		}else
		{
			col_index[col_count++] = col_count;
			inp_ncol = col_count;
		}
		// col_count++;
	}
	
	// cout << "col_count: " << col_count << endl;
	// cout << "inp_ncol: " << inp_ncol << endl;
	
	
	if (col_names != NULL_VECSTR)
	{
		res_ncol = col_names.size();
	}else
	{
		res_ncol = col_count;
	}
	
	// cout << "res_ncol: " << res_ncol << endl;
	// for (int i = 0; i < inp_ncol; i++)
		// cout << col_index[i] << ", ";
	// cout << endl;
	
	// read content
	while (getline(fin, line)) {
		stringstream s(line);
		
		vector<string> line_vec(res_ncol, "");
		
		for (short unsigned int i = 0; i < inp_ncol; i++)
		{
			getline(s, word, sep);
			if(word[0] == '\'')
			{
				string phrase = word.substr(1);
				while(phrase[phrase.length()-1] != '\'')
				{
					getline(s, word, sep);
					phrase += (sep + word);
				}
				word = phrase;
			}
			if(word[0] == '"')
			{
				string phrase = word.substr(1);
				while(phrase[phrase.length()-1] != '"')
				{
					getline(s, word, sep);
					phrase += (sep + word);
				}
				word = phrase.substr(0, phrase.length()-1);
			}
			// cout << word << " -- ";
			
			line_vec.at(col_index[i]) = word;
		}
		
		// cout << endl;
		// for( auto it = line_vec.begin(); it != line_vec.end(); ++it)
		// {
			// cout << *it << " -- ";
		// }
		// cout << endl;
		result.push_back(line_vec);
				
	}
	
	fin.close();
	
	return result;
}


array<unsigned long int, 2> read_spriss_abund(unordered_map<string, vector<unsigned long int>> &abd_collection, string in_fname, short int col_index, short int n_cols, char sep = ' ')
{
	ifstream fin(in_fname);
	array<unsigned long int, 2> result;
	result[0] = result[1] = 0;
	
	string line, key, count;
	
	while(getline(fin, line))
	{
		stringstream s(line);
		getline(s, key, sep);
		getline(s, count, sep);
		if (abd_collection[key].empty())
		{
			abd_collection[key].assign(n_cols, 0);
		}
		unsigned long int lcount = stoul(count);
		abd_collection[key].at(col_index) = lcount;
		result[0]++;
		result[1] += lcount;
	}
	
	fin.close();
	
	return result;
}



void braycurtis_parallel(unordered_map<string, vector<unsigned long int>> &abundances, set<string> col_names, unordered_map<string, array<unsigned long int, 2>> summaries, string fname)
{
	unsigned short int nProcessors = omp_get_max_threads(); 
	unsigned short int ncols = (unsigned short int) col_names.size();
	
	unsigned long int part_dif[nProcessors][ncols][ncols]; //actually, the minimum
	unsigned long int tot_dif[ncols][ncols];
	// long int part_sum[nProcessors][ncols];
	double bcd[ncols][ncols];
	// long int tot_sums[ncols];
	
	/*
	cout << "nb_cores: " << nProcessors << endl;
	cout << "abundances[0][0]: " << abundances[0][0] << endl;
	cout << "((vector<long int>)abundances[0]).at(0): " << ((vector<long int>)abundances[0]).at(0) << endl;
	*/
	
#pragma omp parallel for \
	shared(ncols, part_dif, nProcessors) \
	schedule(dynamic) num_threads(nProcessors)
	for (int i = 0; i < nProcessors; i++)
	{
		for (int j = 0; j < ncols; j++)
		{
			// part_sum[i][j] = 0;
			for (int k = 0; k < ncols; k++)
			{
				part_dif[i][j][k] = 0;
			}
		}
	}
#pragma omp barrier

	// cout << "1st cycle done." << endl;
	for (int j = 0; j < ncols; j++)
	{
		// tot_sums[j] = 0;
		for (int k = 0; k < ncols; k++)
		{
			bcd[j][k] = 0;
			tot_dif[j][k] = 0;
		}
	}
	// cout << "2nd cycle done." << endl;
 
 // unordered_map<string, vector<unsigned long int>>::iterator ito = abundances.begin();
 
 // #pragma omp parallel shared(ncols, part_dif, abundances) num_threads(nProcessors)
 
 size_t n_buckets = abundances.bucket_count();
 // *
#pragma omp parallel for shared(ncols, part_dif, abundances, n_buckets) \
schedule(dynamic) num_threads(nProcessors) // */
	for (size_t bki = 0; bki < n_buckets; bki++)
	{
		unsigned int tid = omp_get_thread_num();
		for( auto it = abundances.begin(bki); it != abundances.end(bki); ++it)
		{
			// cout << tid << " ";
			for (int l1 = 0; l1 < ncols; l1++)
			{
				// part_sum[tid][l1] += (*abundances[l1];
				unsigned long int abd1 = (it->second)[l1];
				if (abd1 != 0)
				{
					for (int l2 = l1+1; l2 < ncols; l2++)
					{
						// double diff = abs(abundances[i][l1] - abundances[i][l2]);
						unsigned long int cmin = (unsigned long int) min(abd1, (it->second)[l2]);
						part_dif[tid][l1][l2] += cmin;
					}
				}
			}
		}
	}
#pragma omp barrier

	// cout << endl << "3rd cycle done." << endl;
		
		
	
#pragma omp parallel for \
	shared(ncols, part_dif, abundances, tot_dif) \
	schedule(dynamic) num_threads(nProcessors)
	for(int j = 0; j < ncols; j++)
	{
		for (int i = 0; i < nProcessors; i++)
		{
			// tot_sums[j] += part_sum[i][j];
			for(int k = j+1; k < ncols; k++)
			{
				tot_dif[j][k] += part_dif[i][j][k];
			}
		}
	}
#pragma omp barrier

	// cout << "4th cycle done." << endl;
	string ord_keys[ncols];
	unsigned short int tmpi = 0;
	for (set<string>::iterator it = col_names.begin(); it != col_names.end(); ++it )
	{
		ord_keys[tmpi] = (*it);
		tmpi++;
	}
	
	
	for (int l1 = 0; l1 < ncols; l1++)
	{
		unsigned long int N1 = summaries[ord_keys[l1]][1];
		for (int l2 = l1+1; l2 < ncols; l2++)
		{
			bcd[l1][l2] = 1.0 - (double) (tot_dif[l1][l2] << 1) / (double) (N1 + summaries[ord_keys[l2]][1]);
			bcd[l2][l1] = bcd[l1][l2];
		}
		bcd[l1][l1] = 0;
	}
	
	// cout << "5th cycle done." << endl;
	
	fstream fout;
	fout.open(fname, ios::out);
	for(int k = 0; k < ncols; k++)
	{
		fout << ";" << ord_keys[k];
	}
	for(int j = 0; j < ncols; j++)
	{
		fout << "\n";
		fout << ord_keys[j];
		for(int k = 0; k < ncols; k++)
		{
			char buf[20];
			sprintf(buf, ";%.12f", bcd[j][k]);
			fout << string(buf);
		}
	}
	
	
	// cout << "All done." << endl;
	
	fout.close();
}



void jaccard_parallel(unordered_map<string, vector<unsigned long int>> &abundances, set<string> col_names, unordered_map<string, array<unsigned long int, 2>> summaries, string fname)
{
	unsigned short int nProcessors = omp_get_max_threads(); 
	unsigned short int ncols = (unsigned short int) col_names.size();
	
	unsigned long int part_inters[nProcessors][ncols][ncols]; //actually, the minimum
	unsigned long int tot_inters[ncols][ncols];
	// long int part_sum[nProcessors][ncols];
	double jac[ncols][ncols];
	// long int tot_sums[ncols];
	
	/*
	cout << "nb_cores: " << nProcessors << endl;
	cout << "abundances[0][0]: " << abundances[0][0] << endl;
	cout << "((vector<long int>)abundances[0]).at(0): " << ((vector<long int>)abundances[0]).at(0) << endl;
	*/
	
#pragma omp parallel for \
	shared(ncols, part_inters, nProcessors) \
	schedule(dynamic) num_threads(nProcessors)
	for (int i = 0; i < nProcessors; i++)
	{
		for (int j = 0; j < ncols; j++)
		{
			// part_sum[i][j] = 0;
			for (int k = 0; k < ncols; k++)
			{
				part_inters[i][j][k] = 0;
			}
		}
	}
#pragma omp barrier

	// cout << "1st cycle done." << endl;
	for (int j = 0; j < ncols; j++)
	{
		// tot_sums[j] = 0;
		for (int k = 0; k < ncols; k++)
		{
			jac[j][k] = 0;
			tot_inters[j][k] = 0;
		}
	}
	// cout << "2nd cycle done." << endl;
 
 // unordered_map<string, vector<unsigned long int>>::iterator ito = abundances.begin();
 
 // #pragma omp parallel shared(ncols, part_inters, abundances) num_threads(nProcessors)
 
 size_t n_buckets = abundances.bucket_count();
 // *
#pragma omp parallel for shared(ncols, part_inters, abundances, n_buckets) \
schedule(dynamic) num_threads(nProcessors) // */
	for (size_t bki = 0; bki < n_buckets; bki++)
	{
		unsigned int tid = omp_get_thread_num();
		for( auto it = abundances.begin(bki); it != abundances.end(bki); ++it)
		{
			// cout << tid << " ";
			for (int l1 = 0; l1 < ncols; l1++)
			{
				// part_sum[tid][l1] += (*abundances[l1];
				bool tally1 = (bool)(it->second)[l1];
				if (tally1 != 0)
				{
					for (int l2 = l1+1; l2 < ncols; l2++)
					{
						// double diff = abs(abundances[i][l1] - abundances[i][l2]);
						unsigned short int intersection = (unsigned short int)(tally1 & ((bool)((it->second)[l2])));
						part_inters[tid][l1][l2] += (unsigned long int) intersection;
					}
				}
			}
		}
	}
#pragma omp barrier

	// cout << endl << "3rd cycle done." << endl;
		
		
	
#pragma omp parallel for \
	shared(ncols, part_inters, abundances, tot_inters) \
	schedule(dynamic) num_threads(nProcessors)
	for(int j = 0; j < ncols; j++)
	{
		for (int i = 0; i < nProcessors; i++)
		{
			// tot_sums[j] += part_sum[i][j];
			for(int k = j+1; k < ncols; k++)
			{
				tot_inters[j][k] += part_inters[i][j][k];
			}
		}
	}
#pragma omp barrier

	// cout << "4th cycle done." << endl;
	string ord_keys[ncols];
	unsigned short int tmpi = 0;
	for (set<string>::iterator it = col_names.begin(); it != col_names.end(); ++it )
	{
		ord_keys[tmpi] = (*it);
		tmpi++;
	}
	
	
	double setunion;
	for (int l1 = 0; l1 < ncols; l1++)
	{
		unsigned long int N1 = summaries[ord_keys[l1]][0];
		for (int l2 = l1+1; l2 < ncols; l2++)
		{
			setunion = (double) (N1 + summaries[ord_keys[l2]][0] - tot_inters[l1][l2]);
			jac[l1][l2] = (setunion - tot_inters[l1][l2]) / setunion;
			jac[l2][l1] = jac[l1][l2];
		}
		jac[l1][l1] = 0;
	}
	
	// cout << "5th cycle done." << endl;
	
	fstream fout;
	fout.open(fname, ios::out);
	for(int k = 0; k < ncols; k++)
	{
		fout << ";" << ord_keys[k];
	}
	for(int j = 0; j < ncols; j++)
	{
		fout << "\n";
		fout << ord_keys[j];
		for(int k = 0; k < ncols; k++)
		{
			char buf[20];
			sprintf(buf, ";%.12f", jac[j][k]);
			fout << string(buf);
		}
	}
	
	
	// cout << "All done." << endl;
	
	fout.close();
}





int main(int argc, char *args[])
{
	int k = atoi(args[1]);
	string dslbl_list_file = string(args[2]);
	string freq_path = string(args[3]);
	string output_name = string(args[4]);
	// suffix of files containing SPRISS' frequent k-mers estimates
	string freq_suffix = "_frequent_" + to_string(k) + "-mers_estimates.txt";
	
	// get dataset_name-label correspondence
	vector<string> ds_lbl_header;
	ds_lbl_header.push_back("label");
	ds_lbl_header.push_back("ds_name");
	// cout << "init ok" << endl;
	vector<vector<string>> ds_lbl_list = read_table_header(dslbl_list_file, ' ', ds_lbl_header);
	// cout << "label - ds_name read" << endl;
	
	// get paths/to/filenames of frequent kmers' frequencies estimates
	set<string> labels;
	map<string, string> in_fnames;
	for (std::vector<vector<string>>::iterator it = ds_lbl_list.begin() ; it != ds_lbl_list.end(); ++it)
	{
		string clbl = it->at(0);
		string cur_fname = freq_path + "/" + it->at(1) + freq_suffix;
		ifstream fin_tmp(cur_fname);
		//if (filesystem::exists(cur_fname))
		if(fin_tmp.is_open())
		{
			string tmpline;
			if(getline(fin_tmp, tmpline))
			{
				labels.insert(clbl);
				in_fnames[clbl] = cur_fname;
			}else{
				cerr << "WARNING: empty file " + cur_fname + ". Ignoring label " + clbl << endl;
			}
			fin_tmp.close();
		}else
		{
			cerr << "WARNING: file " + cur_fname + " not found. Ignoring label " + clbl << endl;
		}
	}
	
	// cout << "labels and fnames inserted" << endl;
	
	// for (auto it = in_fnames.begin(); it != in_fnames.end(); ++it)
	// {
		// cout << it->first << ": " << it->second << endl;
	// }
	
	
	// read and store k-mer abundances
	short int lbl_ind = 0;
	unordered_map<string, vector<unsigned long int>> abund_collection; //key: kmer,	value: kmer-abundance vector
	unordered_map<string, array<unsigned long int, 2>> abund_totals;	 //key: label, value: [0]=#distinct_kmers, [1]=#all_kmers
	for (map<string, string>::iterator it = in_fnames.begin(); it != in_fnames.end(); ++it)
	{
		string clabel = it->first;
		abund_totals[clabel] = read_spriss_abund(abund_collection, it->second, lbl_ind, in_fnames.size());
		lbl_ind++;
	}
	
	// cout << "freqs. read" << endl;
	// for (auto it = abund_totals.begin(); it != abund_totals.end(); ++it)
		// cout << it->first + ": " << it->second[0] << ", " << it->second[1] << endl;
	// for(auto it = abund_collection.begin(); it != abund_collection.end(); ++it)
	// {
		// cout << it->first + ": ";
		// for (auto it2 = (it->second).begin(); it2 != (it->second).end(); ++it2)
			// cout << *it2 << ", ";
	// }
	// cout << endl;
	
	string bc_outname = output_name + "_BC.csv";
	braycurtis_parallel(abund_collection, labels, abund_totals, bc_outname);
	
	string jac_outname = output_name + "_JAC.csv";
	jaccard_parallel(abund_collection, labels, abund_totals, jac_outname);
	
	cout << "SPRISS freq. estimates' dissimilarity matrices stored." << endl;
	// cout << "All done." << endl;
}
