
################################################################################
##  ORIGINAL SPRISS SCRIPT MODIFIED BY GIORGIO GALLINA               ###########
##  DATE: 18 DECEMBER 2022                                           ###########
##  VERSION: 1p2: User-friendly interface + fasta management         ###########
################################################################################


import math
import os
import time
import subprocess
import sys
import numpy as np



# ####################### #  FUNCTIONS DECLARATION  # ######################## #

def printHelp(SCRIPT_NAME):
  print("Usage of SPRISS:")
  print("python3 " + SCRIPT_NAME + " [options] [-i <dataset.fq> -th <theta>]")
  print(" Compulsory arguments for execution:")
  print("   -i <dtset.fq>   The path to the fasta/fastq file to analyse")
  print("   -th <theta>     A number in (0,1) setting the threshold for frequent")
  print("                   k-mers as in the presentation paper")
  print(" Optional arguments:")
  print("   -d <delta>      A number in (0,1) for confidence (1-d) on results,")
  print("                   as defined in the presentation paper. (def. 0.1)")
  print("   -e <epsilon>    A number in (0,1) for the error parameter as in the ")
  print("                   presentation paper. (def. (theta - (2 / #kmers)))")
  print("   -k <kmer_len>   A small positive integer for k-mer length. (def. 31)")
  print("   -l <bags_len>   An integer for the size of each bag as in the")
  print("                   presentation paper. (def. see README)")
  print("   -m <RAM>        Max amount of RAM to allocate in GigaBytes. (def. 200)")
  print("   --filtermin <val>  Exclude k-mers occurring fewer than <val> times.")
  print("                      (def. 1)")
  print("   --memory <RAM>     Same as -m.")
  print("   --out-dir <path>   Specify a directory where to store output files.")
  print("   --threads <val>    Total number of threads. (def. 1)")
  print("   -h, --help      Print this message and exit.")
  print("   -v, --version   Print version of the software.")
  print("Please consult the presentation paper at https://doi.org/10.1093/bioinformatics/btac180 ")
  print("\nExample of usage:")
  print("python3 " + SCRIPT_NAME + " -i ./test/dataset.fq -th 0.00000001 -d 0.05 -m 64 --filtermin 2")
  

def computeDatasetStatistics(dataset_ext, k):
  # check fasta or fastq
  is_fastq = True
  if (dataset_ext.endswith("a")):
    is_fastq = False
  df = open(dataset_ext,'r')
  line = df.readline() #1
  datasets_size = 0
  tot_reads = 0.0
  avg_read_length = 0.0
  max_read_length = 0
  while(line):
  	line = df.readline() #2 (read)
  	tot_reads = tot_reads + 1
  	avg_read_length += (len(line)-1)
  	max_read_length = max(max_read_length,len(line)-1)
  	datasets_size = datasets_size + (len(line)-1 -k +1)
  	if (is_fastq):
  	  line = df.readline() #3
  	  line = df.readline() #4
  	line = df.readline() #1
  df.close()
  avg_read_length = float(avg_read_length)/float(tot_reads)
  return datasets_size, tot_reads, avg_read_length, max_read_length
  



# ########################## #  DATA PREPARATION  # ########################## #

# ====================  GET SCRIPTS DIRECTORY  ==================== #
SCRIPT_PATH = sys.argv[0]
SCRIPT_NAME = SCRIPT_PATH.split("/")
if (len(SCRIPT_NAME) == 1):
  SCRIPT_NAME = SCRIPT_NAME[0]
  SCRIPTS_DIR = "./"
else:
  SCRIPT_NAME = SCRIPT_NAME[len(SCRIPT_NAME)-1]
  SCRIPTS_DIR = SCRIPT_PATH[:-len(SCRIPT_NAME)] # SCRIPTS_DIR = SCRIPTS_DIR.removesuffix(SCRIPT_NAME)

# ====================  INITIALIZE PARAMETERS  ==================== #
# ----------  DEFAULTS  ---------- #
delta = 0.1
k = 31
epsilon = -1 # set later
l = -1 # set later
mem = MEM = 200
filtermin = 1
threads = THREADS = 1
VERSION ="1p2"
# 1p1: introduction of user friendly interface + fasta file management
# 1p2: '31' substituted by 'k' in a formula.
# 1p3: TODO: calculate actual sigma based on dataset instead of fixed value 4
is_fastq = True
kmc_format = " -fq "
out_dir = ""
sample_dir = "./" # dummy value

# ----------  INPUT ARGS  ---------- #
argn = len(sys.argv)
if(argn == 1):
  printHelp(SCRIPT_PATH)
  sys.exit()
argi = 1
control = 0 # to check mandatory arguments (-i, -th)
while (argi < argn):
  
  if (sys.argv[argi] == "-i"):
    dataset_ext = sys.argv[argi+1]
    dataset_split = dataset_ext.split("/")
    dataset_file = dataset_split[len(dataset_split)-1]
    dataset_name = dataset_file.split(".")[0]
    dataset_path = dataset_ext[:-len(dataset_file)]
    if(out_dir == ""): # output directory not set
      sample_dir = dataset_path
    print(dataset_file + ",  " + dataset_name + ",  " + dataset_path)
    if (dataset_ext.endswith("a")):
      is_fastq = False
      kmc_format = " -fa "
    control += 1
    argi += 2
    
  elif (sys.argv[argi] == "-th"):
    try:
      theta = float(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value of <theta> in \"-th <theta>\" must be a decimal number in (0,1).")
    else:
      if (theta <= 0 or theta >= 1):
        sys.exit("ERROR: Value of <theta> in \"-th <theta>\" must be a decimal number in (0,1).")
    control += 1
    argi += 2
  
  elif (sys.argv[argi] == "-d"):
    try:
      delta = float(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value of <delta> in \"-d <delta>\" must be a decimal number in (0,1).")
    else:
      if (delta <= 0 or delta >= 1):
        sys.exit("ERROR: Value of <delta> in \"-d <delta>\" must be a decimal number in (0,1).")
    argi += 2
    
  elif (sys.argv[argi] == "-e"):
    try:
      espilon = float(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value of <espilon> in \"-e <espilon>\" must be a decimal number in (0,1).")
    else:
      if (epsilon == -1):
        print("Using default value of epsilon")
      elif ((espilon <= 0) or (espilon >= 1)):
        sys.exit("ERROR: Value of <epsilon> in \"-e <espilon>\" must be a decimal number in (0,1).")
    argi += 2
    
  elif (sys.argv[argi] == "-k"):
    try:
      k = int(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value of <kmer_length> in \"-k <kmer_length>\" must be a positive integer.")
    else:
      if (k < 1):
        sys.exit("ERROR: Value of <kmer_length> in \"-k <kmer_length>\" must be a positive integer.")
    argi += 2
    
  elif (sys.argv[argi] == "-l"):
    try:
      l = int(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value of <bags_length> in \"-l <bags_length>\" must be a positive integer.")
    else:
      if (l == -1):
        print("Using default value of l")
      elif (l < 1):
        sys.exit("ERROR: Value of <bags_length> in \"-l <bags_length>\" must be a positive integer.")
    argi += 2
    
  elif ((sys.argv[argi] == "-m") or (sys.argv[argi] == "--memory")):
    try:
      mem = int(sys.argv[argi+1])
    except ValueError:
      print("ERROR: Value of <RAM> in \"-m <RAM>\" must be a positive integer (#GB). Using default (200GB).")
      mem = MEM
    else:
      if (mem < 1):
        print("ERROR: Value of <RAM> in \"-m <RAM>\" must be a positive integer (#GB). Using default (200GB).")
        mem = MEM
    argi += 2
    
  elif (sys.argv[argi] == "--filtermin"):
    try:
      filtermin = int(sys.argv[argi+1])
    except ValueError:
      sys.exit("ERROR: Value in \"--filtermin <value>\" must be a positive integer.")
    else:
      if (filtermin < 1):
        sys.exit("ERROR: Value in \"--filtermin <value>\" must be a positive integer.")
    argi += 2
    
  elif (sys.argv[argi] == "--out-dir"):
    out_dir = sys.argv[argi+1]
    if (not out_dir.endswith("/")):
      out_dir = out_dir + "/"
    sample_dir = out_dir
    try:
      mkdir_val = os.mkdir(out_dir)
    except FileExistsError:
      print("Warning: output directory " + out_dir + " already exists, previous data may be lost.")
    argi += 2
    
  elif (sys.argv[argi] == "--threads"):
    try:
      threads = int(sys.argv[argi+1])
    except ValueError:
      print("ERROR: Value in \"--threads <value>\" must be a positive integer. Using default (1 thread).")
      threads = THREADS
    else:
      if (threads < 1):
        print("ERROR: Value in \"--threads <value>\" must be a positive integer. Using default (1 thread).")
        threads = THREADS
    argi += 2
    
  elif ((sys.argv[argi] == "-h") or (sys.argv[argi] == "--help")):
    printHelp(SCRIPT_PATH)
    sys.exit()
      
  elif ((sys.argv[argi] == "-v") or (sys.argv[argi] == "--version")):
    print("SPRISS Version: " + VERSION)
    sys.exit()
    
  else:
    sys.exit("ERROR: unknown option " + sys.argv[argi] + ". Please run:\n  python3 " + SCRIPT_PATH + " --help \nfor getting help on usage.")
      
if (control < 2):
  sys.exit("Please specify both input dataset (-i) and cut-off frequency theta (-th).\n Use option -h or --help for help on usage.")

of = open((out_dir + "running_times.txt"), 'a')
      


# =================  COMPILE CPP PROGR IF NEEDED  ================= #
create_smp_exe_path = "\"" + SCRIPTS_DIR + "create_sample_v" + VERSION + "\""
create_smp_oldexe_path = "\"" + SCRIPTS_DIR + "create_sample\""
cmd = "test -f "+ create_smp_exe_path +" || g++ -o " + create_smp_exe_path + " \"" + SCRIPTS_DIR + "create_sample.cpp\""
print(cmd)
try:
  cmpl_res = os.system(cmd)
except (OSError, Exception):
  sys.exit("Error: Compilation failed. Cannot create sample(s).")
else:
  os.system("g++ -o " + create_smp_oldexe_path + " \"" + SCRIPTS_DIR + "create_sample.cpp\"")
  if ((cmpl_res != 0)): # or not (os.path.exists(create_smp_exe_path))):
    sys.exit("Error: Compilation failed. Cannot create sample(s).")


# ==================  STATISTICS OF THE DATASET  ================== #
print("> Computing statistics of the dataset ... ")
datasets_size, tot_reads, avg_read_length, max_read_length = computeDatasetStatistics(dataset_ext, k)
print(str(datasets_size) + " " + str(k)+"-mers")
print(str(tot_reads) + " reads")
print(str(avg_read_length) + " avg_read_length")
print(str(max_read_length) + " max_read_length")


# ====================  PARAMETERS ADJUSTMENT  ==================== #
if(epsilon < 0):
	epsilon = theta - 2.0/datasets_size
if(l < 0):
	l = math.floor((0.9/theta)/(avg_read_length-k+1))
print(dataset_ext + " k=" + str(k) + " delta=" + str(delta) + " theta=" + str(theta) + " epsilon=" + str(epsilon) + " l=" + str(l))



# ########################## #  CORE COMPUTATION  # ########################## #

#sampling and frequent k-mers estimates
print("")
print("> Computing sampling and frequent k-mers estimates ...")
print(dataset_path + dataset_name)
of.write(dataset_path + dataset_name + " \n")
print(str(theta))
of.write(str(theta) + " \n")

# ============================   SAMPLING   ======================= #
start_sample = time.time()
l = math.floor((0.9/theta)/(avg_read_length-k+1))
# Giorgio's modification: the following commented line was substitued by the next one (4**31 --> 4^^k)
# m = math.ceil((2/((epsilon*l*(avg_read_length-k+1))**2)) * ( math.floor(math.log2(min(2*l*(max_read_length-k+1) ,4**31))) + math.log(2.0/delta)  ) )
m = math.ceil((2/((epsilon*l*(avg_read_length-k+1))**2)) * ( math.floor(math.log2(min(2*l*(max_read_length-k+1) ,4**k))) + math.log(2.0/delta)  ) )
ml = int(m*l)
sample_rate = float(ml)/float(tot_reads)
print("sample_rate= " + str(sample_rate))
of.write("sample_rate= " + str(sample_rate) + " \n")
if(sample_rate >= 1.0):
	sys.exit("Sample size greater than the dataset size. The dataset could be too small (so the sampling strategy is not useful), or try to change values of k end/or theta and/or epsilon and/or l")

if (is_fastq):
  sample_path = sample_dir + dataset_name + "_sample.fastq"
else:
  sample_path = sample_dir + dataset_name + "_sample.fasta"
cmd = create_smp_exe_path + " " + dataset_ext + " " + sample_path + " " + str(int(tot_reads)) + " " + str(ml)
print(cmd)
os.system(cmd)
end_sample = time.time()
print("* Time_sample_creation= " + str(end_sample-start_sample))
of.write("Time_sample_creation= " + str(end_sample-start_sample) + " \n")


# ==================  FREQUENT K-MERS ESTIMATION ================== #

cmd = "test -d " + out_dir + "work_dir || mkdir " + out_dir + "work_dir"
# print(cmd)
os.system(cmd)

output_file = out_dir + dataset_name + "_kmc_" + str(k) + "-mers_db"
start_mining = time.time()
cmd = SCRIPTS_DIR+"../bin/kmc -k"+str(k)+" -cs"+str(datasets_size)+" -m"+str(mem)+" -ci"+str(filtermin)+" -t"+str(threads)+ kmc_format + sample_path + " " + output_file + " " + out_dir + "work_dir/"
print("\n" + cmd)
os.system(cmd)
end_mining = time.time()
counting_time = end_mining - start_mining

start_dump1 = time.time()
denominator = m*l*(avg_read_length-k+1)
kmer_counts_file = out_dir + dataset_name + "_frequent_" + str(k) + "-mers_estimates.txt"
cmd = SCRIPTS_DIR+"../bin/kmc_dump -ci"+str(filtermin)+" -theta" + str(theta) + " -epsilon" +str(epsilon) + " -n_bags" + str(int(m)) + " -denominator" +str(denominator) + " " + output_file + " " + kmer_counts_file
print("\n" + cmd)
os.system(cmd)
end_dump1 = time.time()
dump1_time = end_dump1 - start_dump1


print("\n* Total_time= " + str(counting_time+dump1_time+(end_sample-start_sample)))
of.write("Total_time= " + str(counting_time+dump1_time+(end_sample-start_sample)) + " \n")
of.close()
