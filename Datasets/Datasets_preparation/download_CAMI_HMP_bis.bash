#!/bin/bash

##############################################################################
# Simulated datasets download.                                               #
# See https://data.cami-challenge.org/participate                            #
#     2nd CAMI Toy Human Microbiome Project Dataset                          #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                    #
# Update: March 2023                                                         #
##############################################################################

# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $PROJECT_DIR/Datasets/Datasets_preparation/download_HMP_small.bash
DS_PREP_DIR=$( dirname -- "$file_link"; )	# $PROJECT_DIR/Datasets/Datasets_preparation
DATASET_DIR=$( dirname -- "$DS_PREP_DIR"; )	# $PROJECT_DIR/Datasets
PROJECT_DIR=$( dirname -- "$DATASET_DIR"; )	# $PROJECT_DIR
DSC_DIR="${DATASET_DIR}/CAMI_HMP"

source ${PROJECT_DIR}/BashScripts/utils.bash

exec_cmd "mkdir -p ${DSC_DIR}"
cd ${DSC_DIR}

TMP_DIR="${PROJECT_DIR}/.workdir/downl_cami_tmp"
if [[ $1 != "" ]]; then
	TMP_DIR="$1/downl_cami_tmp"
fi
mkdir -p $TMP_DIR


airways=(4 7 8 9 10 11 12 23 26 27)
skin=(1 13 14 15 16 17 18 19 20 28)
urogen=(0 2 3 5 6 21 22 24 25)
gastro=(0 1 2 3 4 5 9 10 11 12)
oral=(6 7 8 13 14 15 16 17 18 19)

asu="https://frl.publisso.de/data/frl:6425518/airskinurogenital"
go="https://frl.publisso.de/data/frl:6425518/gastrooral"
declare -A sites_d
sites_d=([airways]="${asu}" \
[skin]="${asu}" \
[urogen]="${asu}" \
[gastro]="${go}" \
[oral]="${go}")

N_ds_per_site=3
sites=(airways skin gastro oral)

setupn="setup.tar.gz"

if ! [ -s ${DSC_DIR}/CAMI_airways_metadata.tsv ] || ! [ -s ${DSC_DIR}/CAMI_skin_metadata.tsv ] ; then
	curdir="${TMP_DIR}/asu"
	mkdir -p $curdir 
	cd ${curdir}
	exec_cmd "wget -P ${curdir} -O ${curdir}/${setupn} ${asu}/${setupn}"
	exec_cmd "tar -xzf ${curdir}/${setupn}"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/CAMI_airways_metadata.tsv"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/CAMI_skin_metadata.tsv"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/CAMI_urogen_metadata.tsv"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/asu_metadata.tsv"
	cd ..
	exec_cmd "rm -rf ${curdir}"
else
	msg "INFO: metadata for airskinurogenital already exists: skipping download..."
fi


if ! [ -s ${DSC_DIR}/CAMI_gastro_metadata.tsv ] || ! [ -s ${DSC_DIR}/CAMI_oral_metadata.tsv ] ; then
	curdir="${TMP_DIR}/go"
	mkdir -p $curdir 
	cd ${curdir}
	exec_cmd "wget -P ${curdir} -O ${curdir}/${setupn} ${go}/${setupn}"
	exec_cmd "tar -xzf ${curdir}/${setupn}"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/CAMI_gastro_metadata.tsv"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/CAMI_oral_metadata.tsv"
	exec_cmd "cp ${curdir}/metadata.tsv ${DSC_DIR}/go_metadata.tsv"
	cd ..
	exec_cmd "rm -rf ${curdir}"
else
	msg "INFO: metadata for gastrooral already exists: skipping download..."
fi




for site in ${sites[@]};
do	
	declare -n site_smp=${site}
	for smp in ${site_smp[@]:0:${N_ds_per_site}}
	do
		fq_dest="${DSC_DIR}/CAMI_${site}${smp}.fastq"
		map_dest="${DSC_DIR}/CAMI_${site}${smp}_mapping.tsv"
		
		if [ -s $fq_dest ] & [ -s $map_dest ]; then
			msg "INFO: sample_${smp} of ${site} already exists: skipping download..."
			continue
		fi
		
		curdir="${TMP_DIR}/${site}${smp}"
		mkdir -p ${curdir}
		cd ${curdir}
		
		tardir="sample_${smp}"
		tarzname="${tardir}.tar.gz"
		# download archive with metagenome
		exec_cmd "wget -P ${curdir} -O ${curdir}/${tarzname} ${sites_d[$site]}/${tarzname}"
		exec_cmd "tar -xzf ${curdir}/${tarzname}"
		exec_cmd "gunzip ${curdir}/*/reads/*.gz"
		
		exec_cmd "mv ${curdir}/*/reads/anonymous_reads.f*q ${fq_dest}"
		status=$?
		
#		exec_cmd "gunzip ${curdir}/*/reads/reads_mapping.tsv.gz"
		exec_cmd "mv ${curdir}/*/reads/reads_mapping.tsv ${map_dest}"
		let status+=$?
		cd ..
		
		status=0 #delete anyway
		if [ $status -eq 0 ]; then
			exec_cmd "rm -rf ${curdir}"
		else
			msg "ERROR: download of ${site} sample_${smp} failed."
			msg "Please check directory ${curdir}"
		fi
		
	done
done
