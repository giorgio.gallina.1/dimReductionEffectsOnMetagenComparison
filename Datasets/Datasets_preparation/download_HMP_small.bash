#!/bin/bash

###############################################################################
# HMP datasets download from https://hmpdacc.org/hmp/HMASM/                   #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                     #
# Update: February 2023                                                       #
###############################################################################


# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $PROJECT_DIR/Datasets/Datasets_preparation/download_HMP_small.bash
DS_PREP_DIR=$( dirname -- "$file_link"; )	# $PROJECT_DIR/Datasets/Datasets_preparation
DATASET_DIR=$( dirname -- "$DS_PREP_DIR"; )	# $PROJECT_DIR/Datasets
PROJECT_DIR=$( dirname -- "$PROJECT_DIR"; )	# $PROJECT_DIR
HMP_DIR="${DATASET_DIR}/HMP"

source ${PROJECT_DIR}/BashScripts/utils.bash



function download_ds_group {
	group=$1
	declare -n datasets=$1
	rstatus=0
	
	for dataset in ${datasets[@]};
	do
		cd ${HMP_DIR}
		
		ctrl="${CTRL_DIR}/${dataset}"
		if [ -e $ctrl ] && [ $(cat $ctrl) -eq 0 ]; then
			continue
		fi
		
		ds_path="${HMP_DIR}/${dataset}"
		
		exec_cmd "wget -P ${HMP_DIR} -O ${ds_path}.tar.bz2 https://downloads.hmpdacc.org/data/Illumina/${group}/${dataset}.tar.bz2"
		status=$?
		if [ $status -ne 0 ]; then
			echo $status > $ctrl
			rm -r $ds_path
			rstatus=$status
			continue
		fi
		
		exec_cmd "tar xf ${ds_path}.tar.bz2"
		status=$?
		if [ $status -ne 0 ]; then
			echo $status > $ctrl
			rm -r $ds_path
			rstatus=$status
			continue
		fi
		
		rm ${ds_path}.tar.bz2
		rm ${ds_path}/${dataset}*single*
		
		exec_cmd "cat ${ds_path}/${dataset}*.fastq > ${ds_path}.fastq"
		status=$?
		if [ $status -ne 0 ]; then
			echo $status > $ctrl
			rm -r $ds_path
			rstatus=$status
			continue
		fi
		
#		rm ${ds_path}/${dataset}*novo*
		rm -r ${ds_path}/
		
		echo 0 > $ctrl
		
		
		# Download publicly available abundance table
		exec_cmd "wget -P ${ABUN_DIR} -O ${ABUN_DIR}/${dataset}_abundance_table.tsv.bz2 https://downloads.ihmpdcc.org/data/HMSCP/${dataset}/${dataset}_abundance_table.tsv.bz2"
		cd ${ABUN_DIR}
		exec_cmd "bzip2 -d ${ABUN_DIR}/${dataset}_abundance_table.tsv.bz2"
	done
	
	return $rstatus
}


supragingival_plaque=(SRS053917 SRS075410 SRS011126)
stool=(SRS012273 SRS045713 SRS016095)
tongue_dorsum=(SRS011306 SRS023617 SRS012279)

supragingival_plaque_unif=(SRS024355 SRS075410 SRS011126)
stool_unif=(SRS023526 SRS019397 SRS018351)
tongue_dorsum_unif=(SRS012279 SRS050244 SRS024081)

CTRL_DIR="${HMP_DIR}/.controls"
ABUN_DIR="${HMP_DIR}/abundances"
mkdir -p ${CTRL_DIR} ${ABUN_DIR}
cd ${HMP_DIR}

for group in "supragingival_plaque" "stool" "tongue_dorsum";
do
	download_ds_group "${group}"
done

exit
