#!/bin/bash

##############################################################################
# Simulated datasets download.                                               #
# See https://data.cami-challenge.org/participate                            #
#     2nd CAMI Toy Human Microbiome Project Dataset                          #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina                                                    #
# Update: March 2023                                                         #
##############################################################################

# absolute path to project directory
file_link=$( readlink -f -- "$0"; )			# $PROJECT_DIR/Datasets/Datasets_preparation/download_HMP_small.bash
DS_PREP_DIR=$( dirname -- "$file_link"; )	# $PROJECT_DIR/Datasets/Datasets_preparation
DATASET_DIR=$( dirname -- "$DS_PREP_DIR"; )	# $PROJECT_DIR/Datasets
PROJECT_DIR=$( dirname -- "$PROJECT_DIR"; )	# $PROJECT_DIR
DSC_DIR="${DATASET_DIR}/CAMI_HMP"

source ${PROJECT_DIR}/BashScripts/utils.bash

cd ${DSC_DIR}



downloader_d="https://data.cami-challenge.org/camiClient.jar"
downloader='java -jar ${DS_PREP_DIR}/camiClient.jar'

airways=(4 7 8 9 10 11 12 23 26 27)
skin=(1 13 14 15 16 17 18 19 20 28)
urogen=(0 2 3 5 6 21 22 24 25)
gastro=(0 1 2 3 4 5 9 10 11 12)
oral=(6 7 8 13 14 15 16 17 18 19)

declare -A sites_d
sites_d=([airways]="https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMI_Airways" \
[skin]="https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMI_Skin" \
[urogen]="https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMI_Urogenital_tract" \
[gastro]="https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMI_Gastrointestinal_tract" \
[oral]="https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/CAMI_Oral")

N_ds_per_site=3
sites=(airways skin gastro oral)

pattern_ds='short_read/.*sample_${smp}/reads/anonymous_reads.fq.gz'
pattern_reads_map='short_read/.*sample_${smp}/reads/reads_mapping.tsv.gz'
pattern_metadata='short_read/metadata.csv'


exec_cmd "wget -P ${DS_PREP_DIR} -O ${DS_PREP_DIR}/camiClient.jar ${downloader_d}"


for site in ${sites[@]};
do
#	cdir=${DS_PREP_DIR}/tmp
#	cfile=${DS_PREP_DIR}/tmp/${site}.txt
#	wget -P ${cdir} -O ${cfile} ${sites_d[$site]}

	exec_cmd "${downloader} -d ${sites_d[$site]} . -p ${pattern_metadata}"
		
	fname="${DSC_DIR}/CAMI_${site}_metadata.tsv"
	mv short_read/metadata.tsv ${fname}
	
	declare -n site_smp=${site}
	for smp in ${site_smp[@]:0:${N_ds_per_site}}
	do	
#		pattern_ds_c=$(grep "2017.*${pattern_ds}" ${cfile})
		
		# download metagenome
		exec_cmd "${downloader} -d ${sites_d[$site]} . -p ${pattern_ds}"
		
		fname="${DSC_DIR}/CAMI_${site}${smp}.fastq.gz"
		mv short_read/201*/reads/anonym*.fq.gz ${fname}
		gunzip ${fname}
		status=$?
		
		# download reads_mapping to taxonomy
		exec_cmd "${downloader} -d ${sites_d[$site]} . -p ${pattern_reads_map}"
		
		fname="${DSC_DIR}/CAMI_${site}${smp}_mapping.tsv.gz"
		mv short_read/201*/reads/reads_mapping.tsv.gz ${fname}
		gunzip ${fname}
		let status+=$?
		
		if [ $status -eq 0 ]; then
			rm -rf short_read/201*sample_${smp}
		fi
		
	done
done
