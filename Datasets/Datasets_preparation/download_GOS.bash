#!/bin/bash

#####################################################################
# Use this script to prepare download and prepare GOS datasets.
# Both fasta files containing metagenomic reads and taxonomy classif.
# from Centrifuge (2017) are downloaded
#
# Usage:
# ./download_GOS.bash [<dataset_directory>]
#    default <dataset_directory is ./GOS_datasets
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Author: Giorgio Gallina
# Update: January 2023
#####################################################################


# absolute path to project directory
file_link=$( readlink -f -- "$0"; )          # $project_dir/GOS_dataset_preparation/download_GOS.bash
project_dir=$( dirname -- "$file_link"; )    # $project_dir/GOS_dataset_preparation
project_dir=$( dirname -- "$project_dir"; )  # $project_dir

# directory where to save datasets (input or default)
ds_dir=$1
if [[ ds_dir == "" ]] ; then
	ds_dir="$project_dir/Datasets/GOS/"
fi

mkdir -p $ds_dir

# GOS numbers to download
xx=($(seq -w 2 23))
xx+=($(seq 25 37))
xx+=(47 51)


#mkdir -p GOS_datasets
for x in ${xx[@]}; do
	# get x value as decimal integer (leading 0 confusion with integers in base 8)
	let w="1$x"-100
	let y=$w+3000005
	let z=$w+584
	if [ $w -lt 24 ]; then
		let y+=1
		let z+=1
	fi
	echo "Downloading GOS0$x reads and taxonomy"
	wget -P ${ds_dir} -O ${ds_dir}/GOS0$x.centrifuge.tsv https://de.cyverse.org/anon-files//iplant/home/shared/imicrobe/projects/26/samples/$z/JCVI_SMPL_110328$y.centrifuge.tsv
	wget -P ${ds_dir} -O ${ds_dir}/GOS0$x.fasta https://de.cyverse.org/anon-files//iplant/home/shared/imicrobe/projects/26/samples/$z/JCVI_SMPL_110328$y.fa
done

echo "Download completed."
